import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutModule } from '../../@hubcore/layout/layout.module';
import { CustomLayoutComponent } from './custom-layout.component';
import { SidenavModule } from '../../@hubcore/layout/sidenav/sidenav.module';
import { ToolbarModule } from '../../@hubcore/layout/toolbar/toolbar.module';
import { ConfigPanelModule } from '../../@hubcore/components/config-panel/config-panel.module';
import { SidebarModule } from '../../@hubcore/components/sidebar/sidebar.module';


@NgModule({
  declarations: [CustomLayoutComponent],
  imports: [
    CommonModule,
    LayoutModule,
    SidenavModule,
    ToolbarModule,
    ConfigPanelModule,
    SidebarModule,
  ]
})
export class CustomLayoutModule {
}
