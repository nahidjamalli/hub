import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { HomeRoutingModule } from "./home-routing.module";
import { HomeComponent } from "./home.component";
import { NavigationBarModule } from "src/@hubcore/components/navigation/navigation-bar/navigation-bar.module";

@NgModule({
  declarations: [HomeComponent],
  imports: [CommonModule, HomeRoutingModule, NavigationBarModule],
})
export class HomeModule {}
