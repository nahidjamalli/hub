import { Component } from '@angular/core';

@Component({
  selector: 'vex-home',
  templateUrl: './home.component.html'
})

export class HomeComponent {

  constructor() { }
  
  navigationBarItems = [
    {
      id: 1,
      link: '/sales-orders',
      label: 'Sales Orders'
    },
    {
      id: 2,
      link: '/items',
      label: 'Items'
    },
    {
      id: 3,
      link: '/customers',
      label: 'Customers'
    },
    {
      id: 4,
      link: '/item-journals',
      label: 'Item Journals'
    },
    {
      id: 5,
      link: '/sales-journals',
      label: 'Sales Journals'
    }, {
      id: 6,
      link: '/cash-receipt-journals',
      label: 'Cash Receipt Journals'
    },
    {
      id: 6,
      link: '/transfer-orders',
      label: 'Transfer Orders'
    }
  ]
}
