import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { VexRoutes } from 'src/@hubcore/interfaces/vex-route.interface';
import { ResponsibilityCenterComponent } from './responsibility-center.component';

const routes: VexRoutes = [
  {
    path: '',
    component: ResponsibilityCenterComponent,
    data: {
      toolbarShadowEnabled: true
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]

})
export class ResponsibilityCenterRoutingModule { }
