import { Component, OnInit } from "@angular/core";
import { GeneralService } from "src/app/shared/services/global.service";

@Component({
    selector: "business-units",
    templateUrl: './business-units.component.html'
})

export class BusinessUnitsComponent implements OnInit {
    dataSource: any = {};
    dsCompany: any = {}
    dsCurrency: any = [];
    constructor(private generalService: GeneralService) { }

    ngOnInit() {
        this.getCurrencies();
        this.getCompanies();
    }

    async getCompanies() {
        if (!this.dsCompany.length)
            this.dsCompany = await this.generalService.sendRequest({}, '/config/get-companies');
    }

    async getCurrencies() {
        if (!this.dsCurrency.length)
            this.dsCurrency = await this.generalService.sendRequest({}, '/finance/get-currencies');
    }
}
