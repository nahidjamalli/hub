import { Component, OnInit } from "@angular/core";
import { GeneralService } from "src/app/shared/services/global.service";

@Component({
  selector: "locations",
  templateUrl: "./locations.component.html",
})
export class LocationsComponent implements OnInit {
  dataSource: any = {};

  constructor(private generalService: GeneralService) {}

  async ngOnInit() {
    this.dataSource = await this.generalService.sendRequest(
      {},
      "/warehouse/get-locations"
    );
  }

  onRowInserting(e) {
    let x = this.generalService.sendRequest(e.data, "/warehouse/save-location");
    x.then((x) => {
      this.dataSource[this.dataSource.length - 1].Id = x.ROWID;
      this.dataSource[this.dataSource.length - 1].Code = x.CODE;
    });
  }

  onRowUpdating(e) {
    let updatedRow = Object.assign(e.oldData, e.newData);
    this.generalService.sendRequest(updatedRow, "/warehouse/save-location");
  }

  onRowRemoving(e) {
    this.generalService.sendRequest(
      { Id: e.data.Id },
      "/warehouse/delete-location"
    );
  }
}
