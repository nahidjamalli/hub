import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FlexLayoutModule } from "@angular/flex-layout";
import { ReactiveFormsModule } from "@angular/forms";
import { MatButtonToggleModule } from "@angular/material/button-toggle";
import { TranslateModule } from "@ngx-translate/core";
import { DxButtonModule, DxDataGridModule, DxFormModule } from "devextreme-angular";
import { BreadcrumbsModule } from "src/@hubcore/components/breadcrumbs/breadcrumbs.module";
import { PageLayoutModule } from "src/@hubcore/components/page-layout/page-layout.module";
import { GeneralService } from "src/app/shared/services/global.service";
import { CompaniesRoutingModule } from "./companies-routing.module";
import { CompaniesComponent } from "./companies.component";

@NgModule({
    declarations: [CompaniesComponent],
    imports: [
        CommonModule,
        CompaniesRoutingModule,
        PageLayoutModule,
        FlexLayoutModule,
        BreadcrumbsModule,
        ReactiveFormsModule,
        MatButtonToggleModule,
        DxDataGridModule,
        TranslateModule,
        DxFormModule,
        DxButtonModule
    ],
    providers: []
})

export class CompaniesModule { }