import { Component, OnInit } from "@angular/core";
import { GeneralService } from "src/app/shared/services/global.service";


@Component({
    selector: "companies",
    templateUrl: './companies.component.html'
})

export class CompaniesComponent implements OnInit {
    dataSource: any = {};
    profilePictureURL: any;

    constructor(private generalService: GeneralService) { }

    async ngOnInit() {
        this.dataSource = await this.generalService.sendRequest({}, '/config/get-companies');
    }

    onRowInserting(e) {
        let x = this.generalService.sendRequest(e.data, '/config/save-company');
        x.then(x => {
            this.dataSource[this.dataSource.length - 1].Id = x.ROWID;
            this.dataSource[this.dataSource.length - 1].Code = x.CODE;
        });
        e.cancel = false;
    }

    onRowUpdating(e) {
        let updatedRow = Object.assign(e.oldData, e.newData);
        this.generalService.sendRequest(updatedRow, '/config/save-company');
        e.cancel = false;
    }

    onRowRemoving(e) {
        this.generalService.sendRequest(e.data, '/config/delete-company');
        e.cancel = false;
    }
}