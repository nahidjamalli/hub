import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageLayoutModule } from 'src/@hubcore/components/page-layout/page-layout.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BreadcrumbsModule } from 'src/@hubcore/components/breadcrumbs/breadcrumbs.module';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { ItemTypesComponent } from './item-types.component';
import { ItemTypesRoutingModule } from './item-types-routing.module';

import { TranslateModule } from '@ngx-translate/core';
import { DxDataGridModule, DxFormModule } from 'devextreme-angular';

@NgModule({
  declarations: [ItemTypesComponent],
  imports: [
    CommonModule,
    ItemTypesRoutingModule,
    PageLayoutModule,
    FlexLayoutModule,
    BreadcrumbsModule,
    ReactiveFormsModule,
    MatButtonToggleModule,
    TranslateModule,
    DxDataGridModule,
    DxFormModule
  ],
  exports: [ItemTypesComponent],
  providers: []
})
export class ItemTypesModule { }
