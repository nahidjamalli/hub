import { Component } from '@angular/core';
import { GeneralService } from 'src/app/shared/services/global.service';


@Component({
  selector: 'vex-item-types',
  templateUrl: './item-types.component.html'
})
export class ItemTypesComponent {
  dataSource: any = {};

  constructor(private generalService: GeneralService) { }

  async ngOnInit() {
    this.dataSource = await this.generalService.sendRequest({}, '/inventory/get-item-types');
  }

  onRowInserting(e) {
    let x = this.generalService.sendRequest(e.data, '/inventory/save-item-type');
    x.then(x => {
      this.dataSource[this.dataSource.length - 1].Id = x.ROWID;
      this.dataSource[this.dataSource.length - 1].Code = x.CODE;
    });
  }

  onRowUpdating(e) {
    let updatedRow = Object.assign(e.oldData, e.newData);
    this.generalService.sendRequest(updatedRow, '/inventory/save-item-type');
  }

  onRowRemoving(e) {
    this.generalService.sendRequest({ Id: e.data.Id }, '/inventory/delete-item-type');
  }
}
