import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageLayoutModule } from 'src/@hubcore/components/page-layout/page-layout.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BreadcrumbsModule } from 'src/@hubcore/components/breadcrumbs/breadcrumbs.module';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { ItemCategoriesComponent } from './item-categories.component';
import { ItemCategoriesRoutingModule } from './item-categories-routing.module';

import { TranslateModule } from '@ngx-translate/core';
import { DxTreeListModule } from 'devextreme-angular';

@NgModule({
  declarations: [ItemCategoriesComponent],
  imports: [
    CommonModule,
    ItemCategoriesRoutingModule,
    PageLayoutModule,
    FlexLayoutModule,
    BreadcrumbsModule,
    ReactiveFormsModule,
    MatButtonToggleModule,
    TranslateModule,
    DxTreeListModule
  ],
  exports: [ItemCategoriesComponent],
  providers: []
})
export class ItemCategoriesModule { }
