import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { VexRoutes } from 'src/@hubcore/interfaces/vex-route.interface';
import { UnitOfMeasuresComponent } from './unit-of-measures.component';

const routes: VexRoutes = [
  {
    path: '',
    component: UnitOfMeasuresComponent,
    data: {
      toolbarShadowEnabled: true
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class UnitOfMeasuresRoutingModule { }
