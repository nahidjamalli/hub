import { Component, OnInit } from '@angular/core';
import { GeneralService } from 'src/app/shared/services/global.service';


@Component({
  selector: 'vex-unit-of-measures',
  templateUrl: './unit-of-measures.component.html'
})
export class UnitOfMeasuresComponent implements OnInit {
  dataSource: any = {};
  constructor(private generalService: GeneralService) { }

  async ngOnInit() {
    this.dataSource = await this.generalService.sendRequest({}, '/inventory/get-unit-of-measures');
  }

  onRowInserting(e) {
    let x = this.generalService.sendRequest(e.data, '/inventory/save-unit-of-measure');
    x.then(x => {
      this.dataSource[this.dataSource.length - 1].Id = x.ROWID;
    });
  }

  onRowUpdating(e) {
    let updatedRow = Object.assign(e.oldData, e.newData);
    this.generalService.sendRequest(updatedRow, '/inventory/save-unit-of-measure');
  }

  onRowRemoving(e) {
    this.generalService.sendRequest({ Id: e.data.Id }, '/inventory/delete-unit-of-measure');
  }
}
