import { Component, OnInit } from "@angular/core";
import { MatTableDataSource } from "@angular/material/table";
import { Items } from "./items.interfacce";
import { MatDialog, MatDialogConfig } from "@angular/material/dialog";
import { ItemCardComponent } from "../item-card/item-card.component";
import { GeneralService } from "src/app/shared/services/global.service";

@Component({
  selector: "items",
  templateUrl: "./items.component.html",
  styleUrls: ["./items.component.scss"],
})
export class ItemsComponent implements OnInit {
  dataSource: MatTableDataSource<Items> | null;
  pageSize = 25;
  pageSizeOptions: number[] = [25, 50, 100];
  columns = [
    {
      dataField: "Code",
      label: "code",
    },
    {
      dataField: "Description",
      label: "description",
    },
    {
      dataField: "Type",
      label: "type",
    },
    {
      dataField: "BaseUnitOfMeasure",
      label: "base-unit-of-measure",
    },
    {
      dataField: "UnitCost",
      label: "unit-cost",
    },
    {
      dataField: "UnitPrice",
      label: "unit-price",
    },
  ];
  constructor(
    private generalService: GeneralService,
    public dialog: MatDialog
  ) {}

  ngOnInit() {
    this.getItems();
  }
  async getItems() {
    this.dataSource = new MatTableDataSource();
    this.dataSource.data = await this.generalService.sendRequest(
      {},
      "/inventory/get-items"
    );
  }

  openDialog(id?: any): void {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.width = "90vw";
    dialogConfig.height = "100vh";
    if (id) dialogConfig.data = { id };
    this.dialog
      .open(ItemCardComponent, dialogConfig)
      .afterClosed()
      .subscribe((result) => {
        if (result) this.getItems();
      });
  }

  dblClick({ data, event }) {}

  selectRow({ data, event }) {}

  insertItem() {
    this.openDialog();
  }

  updateItem({ data, e }) {
    this.openDialog(data.Id);
  }
  async deleteItem({ data, event }) {
    await this.generalService.sendRequest(
      { Id: data.Id },
      "/inventory/delete-item"
    );
    this.getItems();
  }
}
