import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageLayoutModule } from 'src/@hubcore/components/page-layout/page-layout.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ItemsComponent } from './items.component';
import { ItemsRoutingModule } from './items-routing.module';
import { BreadcrumbsModule } from '../../../../@hubcore/components/breadcrumbs/breadcrumbs.module';
import { CardHeaderModule } from 'src/@hubcore/components/card-header/card-header.module';
import { TranslateModule } from '@ngx-translate/core';
import { MatTableModule } from '@angular/material/table';
import { MatIconModule } from '@angular/material/icon';
import { IconModule } from '@visurel/iconify-angular';
import { MatSortModule } from '@angular/material/sort';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatButtonModule } from '@angular/material/button';
import { MatDialogModule } from '@angular/material/dialog';
import { MatMultiSortModule } from 'ngx-mat-multi-sort';
import {MatMenuModule} from '@angular/material/menu';
import { HubDataGridModule } from 'src/@hubcore/components/hubital/hub-data-grid/hub-data-grid.module';

@NgModule({
  declarations: [ItemsComponent],
  imports: [
    CommonModule,
    ItemsRoutingModule,
    PageLayoutModule,
    CardHeaderModule,
    BreadcrumbsModule,
    FlexLayoutModule,
    ReactiveFormsModule,
    TranslateModule,
    MatPaginatorModule,
    MatTableModule,
    MatSortModule,
    MatIconModule,
    IconModule,
    FormsModule,
    MatButtonModule,
    MatDialogModule,
    MatMultiSortModule,
    MatMenuModule,
    MatButtonModule,
    HubDataGridModule
  ],
  providers: []
})
export class ItemsModule { }
