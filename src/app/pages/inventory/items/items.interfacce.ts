export class Items {
    Code:any;
    Description: string;
    Type: string;
    BaseUnitOfMeasure: string;
    ItemCategory: string;
    UnitCost: number;
    UnitPrice: number;
}