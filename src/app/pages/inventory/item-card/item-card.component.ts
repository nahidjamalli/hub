import { Component, Inject } from "@angular/core";
import { FormGroup, Validators } from "@angular/forms";
import { FormBuilder } from "@angular/forms";
import { GeneralService } from "src/app/shared/services/global.service";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { PopupService } from "@progress/kendo-angular-popup";

@Component({
  selector: "item-card",
  templateUrl: "./item-card.component.html",
  styleUrls: ["./item-card.component.scss"],
  providers: [PopupService],
})
export class ItemCardComponent {
  Id = 0;
  dsItem: any;
  dsType: any[];
  dsItemCategory: any[];
  dsBaseUnitOfMeasure: any[];

  itemAttributesColumn: string[] = ["Attribute", "Value"];
  dsItemAttributes = [
    { Attribute: "Hydrogen", Value: 1.0079 },
    { Attribute: "Helium", Value: 4.0026 },
  ];

  itemForm: FormGroup = this.fb.group({
    Id: this.Id,
    Code: [""],
    Description: ["", Validators.required],
    PurchasingBlocked: [false],
    BaseUnitOfMeasureId: ["", Validators.required],
    ItemCategoryId: ["", Validators.required],
    ItemTypeId: ["", Validators.required],
    LastModifiedDateTime: [""],
    GTIN: [""],
    AutomaticExtText: [false],
    CommonItemNumber: [""],
    UnitPrice: ["", Validators.required],
    UnitCost: ["", Validators.required],
  });

  constructor(
    private generalService: GeneralService,
    private fb: FormBuilder,
    @Inject(MAT_DIALOG_DATA) public data: any,
    private dialogRef: MatDialogRef<ItemCardComponent>
  ) {
    if (this.data) {
      this.Id = this.data.id;
    }
  }

  ngOnInit() {
    this.getItem();
    this.getType();
    this.getBaseUnitOfMeasure();
    this.getItemCategory();
  }

  async getItem() {
    if (this.Id) {
      this.dsItem = await this.generalService
        .sendRequest({ Id: this.Id }, "/inventory/get-item")
        .then((result) => this.updateForm(result));
    }
  }

  async getType() {
    this.dsType = await this.generalService.sendRequest(
      {},
      "/inventory/get-item-types"
    );
  }

  async getBaseUnitOfMeasure() {
    this.dsBaseUnitOfMeasure = await this.generalService.sendRequest(
      {},
      "/inventory/get-unit-of-measures"
    );
  }

  async getItemCategory() {
    this.dsItemCategory = await this.generalService.sendRequest(
      {},
      "/inventory/get-item-categories"
    );
  }

  updateForm(data) {
    this.itemForm.patchValue({
      Id: data.Id,
      Code: data.Code,
      Description: data.Description,
      PurchasingBlocked: data.PurchasingBlocked,
      BaseUnitOfMeasureId: data.BaseUnitOfMeasureId,
      ItemCategoryId: data.ItemCategoryId,
      ItemTypeId: data.ItemTypeId,
      LastModifiedDateTime: data.LastModifiedDateTime,
      GTIN: data.GTIN,
      AutomaticExtText: data.AutomaticExtText,
      CommonItemNumber: data.CommonItemNumber,
      UnitPrice: data.UnitPrice,
      UnitCost: data.UnitCost,
    });
  }

  async approve() {
    if (this.Id == 0) {
      await this.generalService.sendRequest(
        this.itemForm.value,
        "/inventory/save-item"
      );
    } else {
      await this.generalService.sendRequest(
        this.itemForm.value,
        "/inventory/save-item"
      );
    }
    this.dialogRef.close("OK");
  }
}
