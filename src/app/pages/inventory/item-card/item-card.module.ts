import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageLayoutModule } from 'src/@hubcore/components/page-layout/page-layout.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BreadcrumbsModule } from 'src/@hubcore/components/breadcrumbs/breadcrumbs.module';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { ItemCardComponent } from './item-card.component';
import { TranslateModule } from '@ngx-translate/core';
import { MatButtonModule } from '@angular/material/button';
import { IconModule } from '@visurel/iconify-angular';
import { MatIconModule } from '@angular/material/icon';
import { MatDialogModule } from '@angular/material/dialog';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatMenuModule } from '@angular/material/menu';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatTableModule } from '@angular/material/table';
import { HubSelectBoxModule } from 'src/@hubcore/components/hubital/hub-form-elements/hub-select-box/hub-select-box.module';
import { HubSlideToggleModule } from 'src/@hubcore/components/hubital/hub-form-elements/hub-slide-toggle/hub-slide-toggle.module';
import { HubInputModule } from 'src/@hubcore/components/hubital/hub-form-elements/hub-input/hub-input.module';
import { HubFormFieldModule } from 'src/@hubcore/components/hubital/hub-form-elements/hub-form-field/hub-form-field.module';
import { DropDownsModule } from "@progress/kendo-angular-dropdowns";

@NgModule({
  declarations: [ItemCardComponent],
  imports: [
    CommonModule,
    PageLayoutModule,
    FlexLayoutModule,
    BreadcrumbsModule,
    ReactiveFormsModule,
    MatButtonToggleModule,
    MatGridListModule,
    TranslateModule,
    MatButtonModule,
    MatIconModule,
    IconModule,
    MatDialogModule,
    MatExpansionModule,
    MatMenuModule,
    MatTableModule,
    HubFormFieldModule,
    HubInputModule,
    HubSlideToggleModule,
    HubSelectBoxModule,
    DropDownsModule,
  ],
  providers: []
})
export class ItemCardModule { }
