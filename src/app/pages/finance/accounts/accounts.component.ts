import { Component, OnInit, ViewChild } from '@angular/core';

import { Router } from '@angular/router';
import { DxTreeListComponent } from 'devextreme-angular';
import { GeneralService } from 'src/app/shared/services/global.service';

@Component({
  selector: 'accounts',
  templateUrl: './accounts.component.html'
})
export class AccountsComponent implements OnInit {
  @ViewChild(DxTreeListComponent, { static: false }) treeList: DxTreeListComponent;
  dataSource: any = {};
  dsParents: any;
  expanded: boolean = false;

  constructor(private generalService: GeneralService, private router: Router) { }

  ngOnInit() {
    this.getChartOfAccounts();
    this.getParents();
  }

  async getParents() {
    this.dsParents = await this.generalService.sendRequest({}, '/finance/get-account-parents');
  }

  onRowRemoving(e) {
    this.generalService.sendRequest(e.data, '/finance/delete-account');
    e.cancel = false;
  }

  onInitNewRow(e) {
    this.router.navigate(['/finance/crud-account-card/-' + e.data.ParentId]);
    e.cancel = true;
  }

  onEditingStart(e) {
    this.router.navigate(['/finance/crud-account-card/' + e.data.Id]);
    e.cancel = true;
  }

  async getChartOfAccounts() {
    this.dataSource = await this.generalService.sendRequest({}, '/finance/get-accounts');
  }

  onToolbarPreparing(e) {
    e.toolbarOptions.items.unshift(
      {
        location: 'before',
        widget: 'dxButton',
        options: {
          width: 130,
          text: 'Expand All',
          onClick: this.toggleAll.bind(this)
        }
      });
  }

  toggleAll(e) {
    this.expanded = !this.expanded;
    e.component.option({
      text: this.expanded ? 'Collapse All' : 'Expand All'
    });

    if (!this.expanded) this.collapseAll();
  }

  collapseAll() {
    this.dsParents.forEach(element => {
      this.treeList.instance.collapseRow(element.Id);
    });
  }
}
