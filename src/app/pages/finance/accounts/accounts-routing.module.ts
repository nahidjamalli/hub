import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { VexRoutes } from '../../../../@hubcore/interfaces/vex-route.interface';
import { AccountsComponent } from './accounts.component';

const routes: VexRoutes = [
  {
    path: '',
    component: AccountsComponent,
    data: {
      toolbarShadowEnabled: true
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccountsRoutingModule { }
