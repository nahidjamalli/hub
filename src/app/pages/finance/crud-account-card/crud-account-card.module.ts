import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageLayoutModule } from '../../../../@hubcore/components/page-layout/page-layout.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BreadcrumbsModule } from '../../../../@hubcore/components/breadcrumbs/breadcrumbs.module';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { CrudAccountCardRoutingModule } from './crud-account-card-routing.module';
import { DxFormModule, DxDataGridModule, DxDropDownBoxModule, DxTreeListModule } from 'devextreme-angular';

import { MatSnackBarModule } from '@angular/material/snack-bar';
import { CrudAccountCardComponent } from './crud-account-card.component';
import { DxiItemModule } from 'devextreme-angular/ui/nested';
import { DxButtonModule } from 'devextreme-angular';
import { MatTabsModule } from '@angular/material/tabs';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [CrudAccountCardComponent],
  imports: [
    MatSnackBarModule,
    CommonModule,
    CrudAccountCardRoutingModule,
    PageLayoutModule,
    FlexLayoutModule,
    BreadcrumbsModule,
    ReactiveFormsModule,
    MatButtonToggleModule,
    DxFormModule,
    DxiItemModule,
    DxButtonModule,
    MatTabsModule,
    DxDropDownBoxModule,
    DxTreeListModule,
    DxDataGridModule,
    TranslateModule
  ],
  providers: []
})
export class CrudAccountCardModule { }
