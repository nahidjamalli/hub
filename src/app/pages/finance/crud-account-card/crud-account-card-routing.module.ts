import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { VexRoutes } from '../../../../@hubcore/interfaces/vex-route.interface';
import { CrudAccountCardComponent } from './crud-account-card.component';

const routes: VexRoutes = [
  {
    path: '',
    component: CrudAccountCardComponent,
    data: {
      toolbarShadowEnabled: true
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CrudAccountCardRoutingModule { }
