import { Component, OnInit, ViewChild } from '@angular/core';

import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import CustomStore from 'devextreme/data/custom_store';
import { DxDropDownBoxComponent } from 'devextreme-angular';
import { GeneralService } from 'src/app/shared/services/global.service';

@Component({
  selector: 'crud-account-card',
  templateUrl: './crud-account-card.component.html'
})
export class CrudAccountCardComponent implements OnInit {
  @ViewChild('category') categoryPopup: DxDropDownBoxComponent;
  @ViewChild('dimensionGroup') dimensionGroupPopup: DxDropDownBoxComponent;
  @ViewChild('analysisDimension') analysisDimensionPopup: DxDropDownBoxComponent;

  Id = 0;
  user = null;
  dataSource: any = {};

  dsStatuses: any;
  statusEditorOptions: any = {};

  dsAccountTypes: any;
  accountTypeEditorOptions: any = {};

  dsAccountGroups: any;
  accountGroupEditorOptions: any = {};

  dsBalanceTypes: any;
  balanceTypeEditorOptions: any = {};

  dsCategories: any = {};
  selectedCategory: any = [];

  dsSelectedDimensionGroups: any = [];
  dsDimensionGroups: any = {};
  selectedDimensionGroup: any = [];
  dimensionPostTypeDataSource: any = [];

  dsTransactionTypes: any;
  transactionTypeEditorOptions = {};

  analysisDimensionsMap: any = [];
  selectedAnalysisDimensionMap: any[] = [];
  dimensionGroupsIsOpened: any = [];
  dimensionGroupDisabled: any = [];

  constructor(private generalService: GeneralService, private route: ActivatedRoute, private snackbar: MatSnackBar, private router: Router) {
    this.Id = Number(this.route.snapshot.params.id);
  }

  ngOnInit() {
    try {
      this.user = JSON.parse(localStorage.getItem('user'));
    }
    catch {
      this.router.navigate(['/signin']);
      return;
    }

    if (this.Id > 0) {
      this.getAccount();
      this.getAccountJoinDimensionGroups();
    }
    else {
      this.dataSource.AccountGroupCode = 10;
      this.dataSource.AccountTypeCode = 10;
      this.dataSource.BalanceTypeCode = 100;
      this.dataSource.TransactionTypeCode = 10;
      this.dataSource.StatusCode = 100;
    }

    this.getStatuses();
    this.getAccountCategories();
    this.getBalanceTypes();
    this.getTransactionTypes();
    this.getDimensionGroups();
    this.getDimensionPostTypes();
    this.getAccountTypes();
    this.getAccountGroups();
  }

  async approve() {
    if (this.selectedCategory[0]) this.dataSource.AccountCategoryId = this.selectedCategory[0].Id;

    this.dataSource.DimensionGroups = this.dsSelectedDimensionGroups;

    if (this.Id < 0) this.dataSource.ParentId = -this.Id;

    let controls = ['Code', 'Name', 'StatusCode', 'BalanceTypeCode', 'AccountTypeCode', 'TransactionTypeCode'];

    let messages = {
      // AccountCategoryId: 'Choose any account category.',
      AccountTypeCode: 'Choose any account type.',
      Code: 'Fill in the code.',
      Name: 'Fill in the name.',
      StatusCode: 'Choose any status.',
      BalanceTypeCode: 'Choose any balance type.',
      TransactionTypeCode: 'Choose any transaction type.'
    };

    let resp = this.generalService.validateDataSource(this.dataSource, controls);
    if (resp === true) {
      await this.generalService.sendRequest(this.dataSource, '/finance/save-account');

      this.selectedAnalysisDimensionMap.forEach((element, index) => {
        this.generalService.sendRequest({ AccountId: this.Id, DimensionGroupId: index, AnalysisDimensionId: element == null || element[0].PostType == 300 ? 0 : element[0].Id }, '/finance/save-account-join-analysis-dimension');
      });

      this.snackbar.open('Operation succeeded.', 'Ok', { duration: 3000 });
      this.router.navigate(['/finance/accounts']);
    }
    else {
      this.snackbar.open('Operation failed.\n' + messages[resp], 'Ok', { duration: 3000 });
    }
  }

  async getDimensionPostTypes() {
    this.dimensionPostTypeDataSource = await this.generalService.sendRequest({}, '/finance/get-dimension-post-types');
  }

  categories_displayValueFormatter(item) {
    item = item[0];
    return item && item.Name;
  }

  gvCategories_rowDblClick(e) {
    this.selectedCategory = [e.data];

    this.categoryPopup.instance.close();
  }

  async getAccountCategory() {
    this.selectedCategory = [await this.generalService.sendRequest({ Id: this.dataSource.AccountCategoryId }, '/finance/get-account-category')];
  }

  async getAccount() {
    this.dataSource = await this.generalService.sendRequest({ AccountId: this.Id }, '/finance/get-account');

    if (this.Id != 0 && this.dataSource == null) {
      this.router.navigate(['/*']);
      return;
    }

    this.getAccountCategory();
  }

  async getAccountJoinDimensionGroups() {
    this.dsSelectedDimensionGroups = await this.generalService.sendRequest({ AccountId: this.Id }, '/finance/get-account-join-dimension-groups');

    this.dsSelectedDimensionGroups.forEach(async element => {
      this.analysisDimensionsMap[element.Id] = await this.getAnalysisDimensions(element.Id);

      if (element.AnalysisDimensionId)
        this.selectedAnalysisDimensionMap[element.Id] = [await this.generalService.sendRequest({ AnalysisDimensionId: element.AnalysisDimensionId }, '/finance/get-analysis-dimension')];
    });
  }

  async getBalanceTypes() {
    this.dsBalanceTypes = await this.generalService.sendRequest({ AccountId: this.Id }, '/finance/get-balance-types');
    this.balanceTypeEditorOptions = { dataSource: this.dsBalanceTypes, valueExpr: "Code", displayExpr: "Name" };
  }

  async getAccountTypes() {
    this.dsAccountTypes = await this.generalService.sendRequest({}, '/finance/get-account-types');
    this.accountTypeEditorOptions = { dataSource: this.dsAccountTypes, valueExpr: "Code", displayExpr: "Name" };
  }

  async getAccountGroups() {
    this.dsAccountGroups = await this.generalService.sendRequest({}, '/finance/get-account-groups');
    this.accountGroupEditorOptions = { dataSource: this.dsAccountGroups, valueExpr: "Code", displayExpr: "Name" };
  }

  async getStatuses() {
    this.dsStatuses = await this.generalService.sendRequest({ StatusTypeId: 80, UserId: -4 }, '/config/get-statuses');
    this.statusEditorOptions = { dataSource: this.dsStatuses, valueExpr: "Id", displayExpr: "Name" };
  }

  async getTransactionTypes() {
    this.dsTransactionTypes = await this.generalService.sendRequest({}, '/finance/get-transaction-types');
    this.transactionTypeEditorOptions = { dataSource: this.dsTransactionTypes, valueExpr: "Code", displayExpr: "Name" };
  }

  async getAccountCategories() {
    const _this = this;

    this.dsCategories.store = new CustomStore({
      load: async function (loadOptions: any) {
        let request = {};
        if (loadOptions.filter) {
          for (var i = 0; i < loadOptions.filter.length; i += 2) {
            if (typeof loadOptions.filter[i] == "string") {
              request[loadOptions.filter[i]] = loadOptions.filter[2];
              break;
            }
            else {
              request[loadOptions.filter[i][0]] = loadOptions.filter[i][2];
            }
          }
        }
        return _this.generalService.sendRequest(request, '/finance/get-account-categories').then(r => {
          return {
            data: r
          };
        });
      },
      byKey: function () { return _this.dsCategories; }
    });
  }

  async getDimensionGroups() {
    this.dsDimensionGroups = await this.generalService.sendRequest({ AnalysisSectionCode: 100 }, '/finance/get-analysis-section-join-dimension-groups');
  }

  async gvDimensionGroups_rowDblClick(e) {
    this.selectedDimensionGroup = [e.data];
    let result = this.dsSelectedDimensionGroups.find((r) => r.Id == e.data.Id);

    if (!result) {
      this.dsSelectedDimensionGroups.push(Object.assign(e.data, { PostType: 100 }));
    }

    this.dimensionGroupPopup.instance.close();

    if (!this.analysisDimensionsMap[e.key.Id])
      this.analysisDimensionsMap[e.key.Id] = await this.getAnalysisDimensions(e.key.Id);

    if (result.AnalysisDimensionId)
      this.selectedAnalysisDimensionMap[e.key.Id] = [await this.generalService.sendRequest({ AnalysisDimensionId: result.AnalysisDimensionId }, '/finance/get-analysis-dimension')];
  }

  dimensionGroups_displayValueFormatter(item) {
    item = item[0];
    return item && '[' + item.Code + '] :: [' + item.Name + ']';
  }

  analysisDimensions_displayValueFormatter(item) {
    item = item[0];
    return item && '[' + item.Code + '] :: [' + item.Name + ']';
  }

  async getAnalysisDimensions(dimensionGroupId) {
    return await this.generalService.sendRequest({ DimensionGroupId: dimensionGroupId }, '/finance/get-analysis-dimensions');
  }

  checkDimensionGroupsRowExpanded(rowId) {
    const result = this.dsSelectedDimensionGroups.find((r) => r.Id == rowId);
    return result ? result.PostType != 400 : false;
  }

  getAnalysisDimensions_rowDblClick(e) {
    this.selectedAnalysisDimensionMap[e.data.DimensionGroupId] = [e.data];
    this.dimensionGroupsIsOpened[e.data.DimensionGroupId] = false;
  }
}
