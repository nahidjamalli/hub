import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { VexRoutes } from '../../../../@hubcore/interfaces/vex-route.interface';
import { AnalysisSectionsComponent } from './analysis-sections.component';

const routes: VexRoutes = [
  {
    path: '',
    component: AnalysisSectionsComponent,
    data: {
      toolbarShadowEnabled: true
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AnalysisSectionsRoutingModule { }
