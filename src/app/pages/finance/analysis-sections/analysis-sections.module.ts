import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageLayoutModule } from '../../../../@hubcore/components/page-layout/page-layout.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BreadcrumbsModule } from '../../../../@hubcore/components/breadcrumbs/breadcrumbs.module';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { AnalysisSectionsRoutingModule } from './analysis-sections-routing.module';
import { DxDataGridModule } from 'devextreme-angular';

import { AnalysisSectionsComponent } from './analysis-sections.component';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [AnalysisSectionsComponent],
  imports: [
    CommonModule,
    AnalysisSectionsRoutingModule,
    PageLayoutModule,
    FlexLayoutModule,
    BreadcrumbsModule,
    ReactiveFormsModule,
    MatButtonToggleModule,
    DxDataGridModule,
    TranslateModule
  ],
  providers: []
})
export class AnalysisSectionsModule { }
