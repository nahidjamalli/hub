import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';
import notify from 'devextreme/ui/notify';
import { GeneralService } from 'src/app/shared/services/global.service';

@Component({
  selector: 'analysis-sections',
  templateUrl: './analysis-sections.component.html'
})
export class AnalysisSectionsComponent implements OnInit {

  dataSource: any;
  analysisSectionJoinDimensionGroups: any = [];
  dsDimensionGroups: any;

  constructor(private generalService: GeneralService, private router: Router) { }

  async ngOnInit() {
    this.dataSource = await this.generalService.sendRequest({}, '/finance/get-analysis-sections');
    this.dsDimensionGroups = await this.generalService.sendRequest({}, '/finance/get-dimension-groups');
  }

  onRowRemoving(e, analysisSectionCode) {
    this.generalService.sendRequest({ AnalysisSectionCode: analysisSectionCode, DimensionGroupId: e.data.Id }, '/finance/delete-analysis-section-join-dimension-group');
  }

  onRowInserting(e, analysisSectionCode) {
    if (this.analysisSectionJoinDimensionGroups[analysisSectionCode].find(element => element.Id == e.data.Id)) {
      e.cancel = true;
      notify({ message: 'You can select the same group once.', width: 350 }, "warning", 1500);
    }
    else if (e.data.Id) {
      this.generalService.sendRequest({ AnalysisSectionCode: analysisSectionCode, DimensionGroupId: e.data.Id }, '/finance/save-analysis-section-join-dimension-group');
    }
    else {
      e.cancel = true;
      notify({ message: 'Select any dimension group.', width: 280 }, "warning", 1000);
    }
  }

  onRowUpdating(e, analysisSectionCode) {
    if (this.analysisSectionJoinDimensionGroups[analysisSectionCode].find(element => element.Id == e.newData.Id) && e.newData.Id != e.oldData.Id) {
      e.cancel = true;
      notify({ message: 'You can select the same group once.', width: 350 }, "warning", 1500);
    }
    else {
      this.generalService.sendRequest({ AnalysisSectionCode: analysisSectionCode, DimensionGroupId: e.newData.Id }, '/finance/save-analysis-section-join-dimension-group');
    }
  }

  async onRowExpanded(e) {
    if (!this.analysisSectionJoinDimensionGroups[e.key])
      this.analysisSectionJoinDimensionGroups[e.key] = await this.generalService.sendRequest({ AnalysisSectionCode: e.key }, '/finance/get-analysis-section-join-dimension-groups');
  }
}
