import { Component, OnInit } from '@angular/core';
import { GeneralService } from 'src/app/shared/services/global.service';


@Component({
  selector: 'journal-types',
  templateUrl: './journal-types.component.html'
})
export class JournalTypesComponent implements OnInit {
  dataSource: any;

  constructor(private generalService: GeneralService) { }

  async ngOnInit() {
    this.dataSource = await this.generalService.sendRequest({}, '/finance/get-journal-types');
  }

  onRowInserting(e) {
    let x = this.generalService.sendRequest(e.data, '/finance/save-journal-type');
    x.then(x => {
      this.dataSource[this.dataSource.length - 1].Id = x.ROWID;
      this.dataSource[this.dataSource.length - 1].Code = x.CODE;
    });
    e.cancel = false;
  }

  onRowUpdating(e) {
    let updatedRow = Object.assign(e.oldData, e.newData);
    this.generalService.sendRequest(updatedRow, '/finance/save-journal-type');
    e.cancel = false;
  }

  onRowRemoving(e) {
    this.generalService.sendRequest(e.data, '/finance/delete-journal-type');
    e.cancel = false;
  }
}
