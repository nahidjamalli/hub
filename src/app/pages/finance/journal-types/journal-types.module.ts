import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageLayoutModule } from '../../../../@hubcore/components/page-layout/page-layout.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BreadcrumbsModule } from '../../../../@hubcore/components/breadcrumbs/breadcrumbs.module';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { JournalTypesRoutingModule } from './journal-types-routing.module';
import { DxDataGridModule } from 'devextreme-angular';

import { JournalTypesComponent } from './journal-types.component';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [JournalTypesComponent],
  imports: [
    CommonModule,
    JournalTypesRoutingModule,
    PageLayoutModule,
    FlexLayoutModule,
    BreadcrumbsModule,
    ReactiveFormsModule,
    MatButtonToggleModule,
    DxDataGridModule,
    TranslateModule
  ],
  providers: []
})
export class JournalTypesModule { }
