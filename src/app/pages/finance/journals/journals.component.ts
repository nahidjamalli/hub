import { Component, OnInit } from '@angular/core';
import icAdd from '@iconify/icons-ic/twotone-add';
import { Router } from '@angular/router';
import { GeneralService } from 'src/app/shared/services/global.service';

@Component({
  selector: 'journals',
  templateUrl: './journals.component.html',
  styleUrls: ['./journals.component.scss']
})
export class JournalsComponent implements OnInit {
  dataSource: any;
  icAdd = icAdd;
  constructor(private generalService: GeneralService, private router: Router) { }

  async ngOnInit() {
    this.dataSource = await this.generalService.sendRequest({}, '/finance/get-journals');
  }

  onRowRemoving(e) {
    this.generalService.sendRequest(e.data, '/finance/delete-journal');
    e.cancel = false;
  }

  onInitNewRow(e) {
    this.router.navigate(['/finance/crud-journal/0']);
    e.cancel = true;
  }

  onEditingStart(e) {
    this.router.navigate(['/finance/crud-journal/' + e.data.Id]);
    e.cancel = true;
  }

  contentReady = (e) => {
    if (!e.component.isRowExpanded(["Posted"])) {
      e.component.expandRow(["Posted"]);
    }
  }
}
