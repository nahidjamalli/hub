import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageLayoutModule } from '../../../../@hubcore/components/page-layout/page-layout.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BreadcrumbsModule } from '../../../../@hubcore/components/breadcrumbs/breadcrumbs.module';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { JournalsRoutingModule } from './journals-routing.module'
import { DxDataGridModule } from 'devextreme-angular';
import { JournalsComponent } from './journals.component'
import { TranslateModule } from '@ngx-translate/core';
import { MatIconModule } from '@angular/material/icon';
import { IconModule } from '@visurel/iconify-angular'
import { MatButtonModule } from '@angular/material/button';

@NgModule({
  declarations: [JournalsComponent],
  imports: [
    CommonModule,
    JournalsRoutingModule,
    PageLayoutModule,
    FlexLayoutModule,
    BreadcrumbsModule,
    ReactiveFormsModule,
    MatButtonToggleModule,
    DxDataGridModule,
    TranslateModule,
    MatIconModule,
    IconModule,
    MatButtonModule,
  ],
  providers: []
})
export class JournalsModule { }
