export class Journal {
    Id: number;
    StatusCode: number;
    JournalModeId: number;
    Code: string;
    Name: string;
    Date: string;
    JournalTypeId: number;
    Note: string;
    Lines: JournalLine[];
}

export class JournalLine {
    Id: number;
    JournalId: number;
    ParentId: number;
    Code: string;
    LineDate: string;
    DocumentNumber: string;
    EntryExpirationDate: string;
    EntryTypeCode: number;
    DebitAccountId: number;
    DebitAccountGroupCode: number;
    DebitAccountCode: string;
    DebitAccountName: string;
    CreditAccountId: number;
    CreditAccountGroupCode: number;
    CreditAccountCode: string;
    CreditAccountName: string;
    Amount: number;
    PaidAmount: number;
    OutstandingAmount: number;
    CurrencyCode: string;
    Note: string;
    DebitAccountDimensionGroups: any;
    CreditAccountDimensionGroups: any;
    DebitAccountSelectedAnalysisDimension: any;
    CreditAccountSelectedAnalysisDimension: any;
}
