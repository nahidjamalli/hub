import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageLayoutModule } from '../../../../@hubcore/components/page-layout/page-layout.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BreadcrumbsModule } from '../../../../@hubcore/components/breadcrumbs/breadcrumbs.module';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { CrudJournalCardRoutingModule } from './crud-journal-routing.module';
import { DxFormModule, DxDataGridModule, DxDropDownBoxModule, DxTreeListModule, DxPopupModule, DxDropDownButtonModule, DxTextAreaModule, DxScrollViewModule, DxTagBoxModule, DxSelectBoxModule, DxTextBoxModule, DxNumberBoxModule } from 'devextreme-angular';

import { MatSnackBarModule } from '@angular/material/snack-bar';
import { CrudJournalCardComponent } from './crud-journal.component';
import { DxiItemModule } from 'devextreme-angular/ui/nested';
import { DxButtonModule } from 'devextreme-angular';
import { MatTabsModule } from '@angular/material/tabs';
import { MatExpansionModule } from '@angular/material/expansion';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [CrudJournalCardComponent],
  imports: [
    MatSnackBarModule,
    CommonModule,
    CrudJournalCardRoutingModule,
    PageLayoutModule,
    FlexLayoutModule,
    BreadcrumbsModule,
    ReactiveFormsModule,
    MatButtonToggleModule,
    DxFormModule,
    DxiItemModule,
    DxButtonModule,
    MatTabsModule,
    DxDropDownBoxModule,
    DxTreeListModule,
    DxDataGridModule,
    DxPopupModule,
    DxDropDownButtonModule,
    DxTextAreaModule,
    MatExpansionModule,
    DxScrollViewModule,
    TranslateModule,
    DxSelectBoxModule,
    DxTextBoxModule,
    DxNumberBoxModule
  ],
  providers: []
})
export class CrudJournalCardModule { }
