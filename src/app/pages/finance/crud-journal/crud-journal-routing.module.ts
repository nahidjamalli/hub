import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { VexRoutes } from '../../../../@hubcore/interfaces/vex-route.interface';
import { CrudJournalCardComponent } from './crud-journal.component';

const routes: VexRoutes = [
  {
    path: '',
    component: CrudJournalCardComponent,
    data: {
      toolbarShadowEnabled: true
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CrudJournalCardRoutingModule { }
