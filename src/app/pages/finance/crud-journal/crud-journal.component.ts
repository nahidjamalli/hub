import { Component, OnInit, ViewChild } from '@angular/core';

import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DxDataGridComponent } from 'devextreme-angular';
import { JournalLine, Journal } from './crud-journal.model';
import { confirm } from 'devextreme/ui/dialog';
import notify from 'devextreme/ui/notify';
import { GeneralService } from 'src/app/shared/services/global.service';

@Component({
  selector: 'crud-journal',
  templateUrl: './crud-journal.component.html'
})
export class CrudJournalCardComponent implements OnInit {
  @ViewChild('lines') dxDataGrid: DxDataGridComponent;

  // Settings
  Id = 0;
  user = null;

  // Journal
  journalLinePopupVisible = false;
  journal: Journal = new Journal();

  dsJournalTypes: any = [];
  journalTypeEditorOptions = {};

  journalModesDataSource: any = [];
  journalModesEditorOptions = {};

  journalButtonSettings = [
    { value: 2, name: "Save as a new journal draft", icon: "tags" }
  ];

  debitTotalAmount = 0.0;
  creditTotalAmount = 0.0;

  // Journal Line
  newRow = false;
  selectedJournalLineId = 0;
  deletedJournalLineIds = [];
  selectedJournalLine: JournalLine = new JournalLine();
  refSelectedJournalLine: JournalLine = new JournalLine();
  journalLineButtonSettings = ["Save as new line & Add offset line"];

  // Dimension Groups - Post Type
  dimensionPostTypeDataSource: any = [];
  analysisDimensionsMap: any = [];
  debitAccount_dimensionGroupsIsOpened: any = [];
  creditAccount_dimensionGroupsIsOpened: any = [];
  debitAccountDimensionGroupDisabled: any = [];
  creditAccountDimensionGroupDisabled: any = [];

  // Accounts
  dsAccounts: any = [];
  _selectedDebitAccount: number;
  selectedDebitAccount: any = {};

  _selectedCreditAccount: number;
  selectedCreditAccount: any = {};

  // Currencies
  dsCurrencies: any = [];
  currencyEditorOptions = {};

  // Entries
  dsEntryTypes: any = [];
  entryTypeEditorOptions = {};

  entryDateEditorOptions = {
    type: "date",
    value: new Date().toLocaleDateString(),
    openOnFieldClick: true,
    displayFormat: "dd-MM-yyyy",
    dateSerializationFormat: 'yyyy-MM-dd'
  };

  expirationDateEditorOptions = {
    openOnFieldClick: true,
    displayFormat: "dd-MM-yyyy",
    showClearButton: true,
    dateSerializationFormat: 'yyyy-MM-dd'
  };

  // Amount - DC
  amountEditorOptions = {
    format: '###,##0.000 AZN',
    value: 0.0,
    showClearButton: true
  };;

  debitAccountDimensionGroupsPopup = false;
  creditAccountDimensionGroupsPopup = false;

  constructor(private generalService: GeneralService, private route: ActivatedRoute, private snackbar: MatSnackBar, private router: Router) {
    this.Id = Number(this.route.snapshot.params.id);
    if (localStorage.getItem('lang') == 'az') {
      this.journalButtonSettings[0] = { value: 2, name: "Yeni qaralama", icon: "tags" };
      this.journalLineButtonSettings[0] = "Yeni sətir & Ofset sətri yarat";
    }
  }

  async ngOnInit() {
    try {
      this.user = JSON.parse(localStorage.getItem('user'));
    }
    catch {
      this.router.navigate(['/signin']);
      return;
    }

    if (this.Id) {
      this.getJournal();
    }
    else {
      this.journal = new Journal();
      this.journal.JournalModeId = 100;
      this.journal.Lines = [];
    }

    await this.getAccounts();
    this.getJournalTypes();
    this.getJournalModes();
    this.getEntryTypes();
  }

  // #region Journal
  async getJournal() {
    this.journal = await this.generalService.sendRequest({ Id: this.Id }, '/finance/get-journal');
    this.getJournalLines();
  }

  async getJournalLines() {
    this.journal.Lines = await this.generalService.sendRequest({ JournalModeId: this.journal.JournalModeId, JournalId: this.Id }, '/finance/get-journal-lines');
    this.calculateDebitCreditSummary();
  }

  async getJournalModes() {
    this.journalModesDataSource = await this.generalService.sendRequest({}, '/finance/get-journal-modes');
    const _that = this;

    this.journalModesEditorOptions = {
      dataSource: this.journalModesDataSource,
      valueExpr: "Code",
      displayExpr: "Name",
      readOnly: this.Id,
      onValueChanged: function (e) {
        if (e.value == 100) {
          for (let index = 0; index < _that.journal.Lines.length; index++) {
            if (!_that.journal.Lines[index].DebitAccountId || !_that.journal.Lines[index].CreditAccountId) {
              _that.snackbar.open('Both debit and credit accounts must be selected for the single-line mode.', 'Ok', { duration: 5000 });
              _that.journal.JournalModeId = 200;
              e.component.option("value", 200);
              break;
            }
            if (_that.journal.Lines[index].Amount < 0) {
              _that.snackbar.open('Amount must be positive for the single-line mode.', 'Ok', { duration: 5000 });
              _that.journal.JournalModeId = 200;
              e.component.option("value", 200);
              break;
            }
          }
        }
      }
    };
  }

  async getJournalTypes() {
    this.dsJournalTypes = await this.generalService.sendRequest({}, '/finance/get-journal-types');
    const _that = this;

    this.journalTypeEditorOptions = {
      dataSource: this.dsJournalTypes, valueExpr: "Id", displayExpr: "Name",
      searchEnabled: true,
      onValueChanged: function () {
        if (!_that.journal.Name)
          _that.journal.Name = 'New Journal-' + new Date().getMilliseconds() + '-' + new Date().toLocaleDateString();
      }
    };
  }

  onInitNewRow(e) {
    this.getCurrencies();
    this.getDimensionPostTypes();

    this._selectedDebitAccount = 0;
    this._selectedCreditAccount = 0;
    this.selectedDebitAccount = null;
    this.selectedCreditAccount = null;
    this.newRow = true;

    this.refSelectedJournalLine = new JournalLine();
    this.refSelectedJournalLine.Id = -Number(new Date().getMilliseconds() + '' + this.journal.Lines.length);
    this.refSelectedJournalLine.JournalId = this.Id;
    this.refSelectedJournalLine.LineDate = new Date().toLocaleDateString();
    this.refSelectedJournalLine.Amount = 0.0;
    this.refSelectedJournalLine.PaidAmount = 0.0;
    this.refSelectedJournalLine.OutstandingAmount = 0.0;
    this.refSelectedJournalLine.EntryTypeCode = 100;
    this.refSelectedJournalLine.CurrencyCode = 'AZN';
    this.journal.Lines.push(this.refSelectedJournalLine);

    this.selectedJournalLine = Object.assign({}, this.refSelectedJournalLine);

    this.selectedJournalLine.DebitAccountDimensionGroups = [];
    this.selectedJournalLine.DebitAccountSelectedAnalysisDimension = [];

    this.selectedJournalLine.CreditAccountDimensionGroups = [];
    this.selectedJournalLine.CreditAccountSelectedAnalysisDimension = [];

    this.journalLinePopupVisible = true;
    window.setTimeout(function () { e.component.cancelEditData(); }, 0);
  }

  async onEditingStart(e) {
    // to cancel event for editing row mode
    e.cancel = true;
    if (e.data.Amount < 0.0) return;

    this.dxDataGrid.instance.selectRows([e.key], true);
    this.newRow = false;

    // assign selected journal line to global variable
    this.refSelectedJournalLine = e.data;

    // shallow copy for selected journal line
    this.selectedJournalLine = Object.assign({}, e.data);

    if (this.refSelectedJournalLine.DebitAccountDimensionGroups)
      this.selectedJournalLine.DebitAccountDimensionGroups = this.refSelectedJournalLine.DebitAccountDimensionGroups;
    else this.selectedJournalLine.DebitAccountDimensionGroups = [];

    if (this.refSelectedJournalLine.CreditAccountDimensionGroups)
      this.selectedJournalLine.CreditAccountDimensionGroups = this.refSelectedJournalLine.CreditAccountDimensionGroups;
    else this.selectedJournalLine.CreditAccountDimensionGroups = [];

    if (!this.selectedJournalLine.DebitAccountSelectedAnalysisDimension)
      this.selectedJournalLine.DebitAccountSelectedAnalysisDimension = [];

    if (!this.selectedJournalLine.CreditAccountSelectedAnalysisDimension)
      this.selectedJournalLine.CreditAccountSelectedAnalysisDimension = [];

    // show popup for journal line
    this.journalLinePopupVisible = true;

    this.getCurrencies();
    this.getDimensionPostTypes();

    // get dimension groups by sections - Account, Credit Account
    this.getDebitAccountDimensionGroups(this.selectedJournalLine.DebitAccountId);
    this.getCreditAccountDimensionGroups(this.selectedJournalLine.CreditAccountId);

    // find and setting selected account
    this.selectedDebitAccount = this.dsAccounts.filter(acc => acc.Id == this.selectedJournalLine.DebitAccountId);
    if (this.selectedDebitAccount) this.selectedDebitAccount = this.selectedDebitAccount[0];

    // find and setting selected credit account
    this.selectedCreditAccount = this.dsAccounts.filter(acc => acc.Id == this.selectedJournalLine.CreditAccountId);
    if (this.selectedCreditAccount) this.selectedCreditAccount = this.selectedCreditAccount[0];
  }

  onRowRemoved(e) {
    if (e.data.Id) {
      this.deletedJournalLineIds.push(e.data.Id);

      this.journal.Lines.splice(this.journal.Lines.findIndex(p => p.ParentId == e.data.Id), 1);
    }

    this.calculateDebitCreditSummary();
  }

  async deleteSelectedRows() {
    let dialogResponse = false;
    var result = confirm("Are you sure you want to delete selected record(s)?", "Question");
    await result.then(function (dialogResult) { dialogResponse = dialogResult; });

    if (dialogResponse) {
      let selectedRows = this.dxDataGrid.instance.getSelectedRowKeys().sort();
      let selectedRowsData = this.dxDataGrid.instance.getSelectedRowsData().sort();
      let count = selectedRows.length;

      while (count--) {
        let rowIndex = this.dxDataGrid.instance.getRowIndexByKey(selectedRows[count]);
        if (selectedRowsData[count].Id) this.deletedJournalLineIds.push(selectedRowsData[count].Id);
        this.journal.Lines.splice(rowIndex, 1);

        // Remove offset row
        this.journal.Lines.splice(this.journal.Lines.findIndex(p => p.ParentId == selectedRowsData[count].Id), 1);
      }

      this.dxDataGrid.instance.refresh();
      this.calculateDebitCreditSummary();
    }
  }

  calculateDebitCreditSummary() {
    this.debitTotalAmount = this.creditTotalAmount = 0.0;

    for (let index = 0; index < this.journal.Lines.length; index++) {
      const element = this.journal.Lines[index];

      // Single line mode
      if (this.journal.JournalModeId == 100) {
        this.debitTotalAmount += this.floatToFixed3(element.Amount);
        this.creditTotalAmount += this.floatToFixed3(element.Amount);
      }
      // Double line mode
      else if (this.journal.JournalModeId == 200) {
        if (this.floatToFixed3(element.Amount) > 0) this.debitTotalAmount += this.floatToFixed3(element.Amount);
        else this.creditTotalAmount += this.floatToFixed3(element.Amount);
      }
    }
  }

  async journalSave(e) {
    if (e.itemData.name == "Remove") {
      let result = await confirm("Are you sure you want to remove this journal?", "Question");

      if (result) {
        if (this.journal.Id) await this.generalService.sendRequest({ Id: this.journal.Id }, '/finance/delete-journal');
        notify({ message: 'The journal has been removed successfully.', width: 500 }, "success", 3000);
        this.router.navigate(['/finance/journals']);
      }

      return;
    }

    if (!this.journal.Lines.length) {
      notify({ message: 'Operation failed. Please add any lines.', width: 500 }, "error", 3000);
      return;
    }

    if (e.itemData.name == "Post Now") {
      this.journal.StatusCode = 100;
    }
    else if (e.itemData.name == "Save as a draft") {
      this.journal.StatusCode = 200;
    }
    else if (e.itemData.name == "Post as a new journal") {
      this.journal.Id = 0;
      this.journal.StatusCode = 100;
    }
    else if (e.itemData.name == this.journalButtonSettings[0].name) {
      this.journal.Id = 0;
      this.journal.StatusCode = 200;
    }

    let controls = ['Name', 'JournalTypeId'];

    let messages = {
      JournalTypeId: 'Choose any journal type.',
      Name: 'Fill in the name.',
      OOB: 'The journal is out of balance.'
    };

    let validationResponse = this.generalService.validateDataSource(this.journal, controls);

    // Draft jurnallar isoutOfBalance yoxlanmır.
    if (validationResponse == true && this.journal.StatusCode == 100 && this.journal.JournalModeId == 200)
      validationResponse = this.isOutOfBalance();

    if (validationResponse === true) {
      let journalResponse = await this.generalService.sendRequest(this.journal, '/finance/save-journal');

      for (let journalLineIndex = 0; journalLineIndex < this.journal.Lines.length; journalLineIndex++) {
        // Skip offset lines
        if (this.journal.Lines[journalLineIndex].Amount < 0) continue;

        const journalLineRowId = this.journal.Lines[journalLineIndex].Id;
        this.journal.Lines[journalLineIndex].JournalId = journalResponse.ROWID;

        // for save as a new journal. when journal id = 0
        if (this.journal.Id == 0) this.journal.Lines[journalLineIndex].Id = 0;

        let journalLineResponse = await this.generalService.sendRequest(this.journal.Lines[journalLineIndex], '/finance/save-journal-line');

        if (this.journal.Lines[journalLineIndex].DebitAccountSelectedAnalysisDimension && this.journal.Lines[journalLineIndex].DebitAccountDimensionGroups.length)
          this.journal.Lines[journalLineIndex].DebitAccountSelectedAnalysisDimension.forEach((element, i) => {
            this.generalService.sendRequest({
              JournalLineId: journalLineResponse.ROWID,
              AccountId: this.journal.Lines[journalLineIndex].DebitAccountId,
              DimensionGroupId: i,
              AnalysisDimensionId: element[0] == null ? 0 : element[0].Id
            }, '/finance/save-journal-line-join-account-analysis-dimension');
          });

        if (this.journal.Lines[journalLineIndex].CreditAccountSelectedAnalysisDimension && this.journal.Lines[journalLineIndex].CreditAccountDimensionGroups.length)
          this.journal.Lines[journalLineIndex].CreditAccountSelectedAnalysisDimension.forEach((element, i) => {
            this.generalService.sendRequest({
              JournalLineId: journalLineResponse.ROWID,
              AccountId: this.journal.Lines[journalLineIndex].CreditAccountId,
              DimensionGroupId: i,
              AnalysisDimensionId: element[0] == null ? 0 : element[0].Id
            }, '/finance/save-journal-line-join-account-analysis-dimension');
          });
      }

      for (let index = 0; index < this.deletedJournalLineIds.length; index++) {
        this.generalService.sendRequest({ JournalLineId: this.deletedJournalLineIds[index] }, '/finance/delete-journal-line');
      }

      this.snackbar.open('The journal has been successfully saved as: ' + journalResponse.CODE, 'Ok', { duration: 5000 });
      this.router.navigate(['/finance/journals']);
    }
    else {
      this.snackbar.open('Operation failed.\n' + messages[validationResponse], 'Ok', { duration: 3000 });
    }
  }

  isOutOfBalance() {
    let sum = 0.0;

    for (let index = 0; index < this.journal.Lines.length; index++) {
      sum += this.journal.Lines[index].Amount;
      sum = Number(sum.toFixed(3));
    }

    return sum ? 'OOB' : true;
  }
  // #endregion

  // #region Popup
  // #region Journal Line
  async getAccounts() {
    if (!this.dsAccounts.length)
      this.dsAccounts = await this.generalService.sendRequest({}, '/finance/get-accounts');
  }

  async getCurrencies() {
    if (!this.dsCurrencies.length)
      this.dsCurrencies = await this.generalService.sendRequest({}, '/finance/get-currencies');
    else return;

    const _that = this;

    this.currencyEditorOptions = {
      dataSource: this.dsCurrencies,
      valueExpr: "Code",
      displayExpr: "Name",
      onValueChanged: function (e) {
        _that.amountEditorOptions = { format: '###,##0.000 ' + e.value, value: _that.selectedJournalLine.Amount, showClearButton: true };
      }
    };
  }

  async getEntryTypes() {
    if (!this.dsEntryTypes.length)
      this.dsEntryTypes = await this.generalService.sendRequest({}, '/finance/get-entry-types');
    else return;

    this.entryTypeEditorOptions = { dataSource: this.dsEntryTypes, valueExpr: "Code", displayExpr: "Name", showClearButton: true };
  }

  async debitAccount_onSelectionChanged(e) {
    this.selectedDebitAccount = e.selectedItem;
    if (e.selectedItem) this.getDebitAccountDimensionGroups(e.selectedItem.Id);
  }

  async creditAccount_onSelectionChanged(e) {
    this.selectedCreditAccount = e.selectedItem;
    if (e.selectedItem) this.getCreditAccountDimensionGroups(e.selectedItem.Id);
  }

  // #endregion
  // #region Dimension Groups

  async debitAccount_onRowExpanded(e) {
    if (!this.analysisDimensionsMap[e.key])
      this.analysisDimensionsMap[e.key] = [];

    this.debitAccountDimensionGroupDisabled[e.key] = false;

    if (!this.selectedJournalLine.DebitAccountSelectedAnalysisDimension) {
      this.selectedJournalLine.DebitAccountSelectedAnalysisDimension = [];
    }
    if (!this.selectedJournalLine.DebitAccountSelectedAnalysisDimension[e.key]) {
      this.selectedJournalLine.DebitAccountSelectedAnalysisDimension[e.key] = [];
    }

    const result = this.selectedJournalLine.DebitAccountDimensionGroups.find((r) => r.Id == e.key);

    if (result.PostType != 400) {
      this.getAnalysisDimensions(e.key);
      this.selectedJournalLine.DebitAccountSelectedAnalysisDimension[e.key] = [await this.generalService.sendRequest({
        JournalLineId: this.selectedJournalLine.Id,
        AccountId: this.selectedDebitAccount.Id,
        DimensionGroupId: e.key
      }, '/finance/get-journal-line-join-account-analysis-dimension')];

      this.debitAccountDimensionGroupDisabled[e.key] = (result.PostType == 300);
    }
  }

  async creditAccount_onRowExpanded(e) {
    if (!this.analysisDimensionsMap[e.key])
      this.analysisDimensionsMap[e.key] = [];
    this.creditAccountDimensionGroupDisabled[e.key] = false;

    if (!this.selectedJournalLine.CreditAccountSelectedAnalysisDimension) {
      this.selectedJournalLine.CreditAccountSelectedAnalysisDimension = [];
    }
    if (!this.selectedJournalLine.CreditAccountSelectedAnalysisDimension[e.key]) {
      this.selectedJournalLine.CreditAccountSelectedAnalysisDimension[e.key] = [];
    }

    const result = this.selectedJournalLine.CreditAccountDimensionGroups.find((r) => r.Id == e.key);

    if (result.PostType != 400) {
      this.getAnalysisDimensions(e.key);
      this.selectedJournalLine.CreditAccountSelectedAnalysisDimension[e.key] = [await this.generalService.sendRequest({
        JournalLineId: this.selectedJournalLine.Id,
        AccountId: this.selectedCreditAccount.Id,
        DimensionGroupId: e.key
      }, '/finance/get-journal-line-join-account-analysis-dimension')];

      this.creditAccountDimensionGroupDisabled[e.key] = (result.PostType == 300);
    }
  }

  async getAnalysisDimensions(dimensionGroupId) {
    if (!this.analysisDimensionsMap[dimensionGroupId].length)
      this.analysisDimensionsMap[dimensionGroupId] = await this.generalService.sendRequest({ DimensionGroupId: dimensionGroupId }, '/finance/get-analysis-dimensions');
  }

  analysisDimensions_displayValueFormatter(item) {
    item = item[0];
    return item && '[' + item.Code + '] :: [' + item.Name + ']';
  }

  gvDebitAccountAnalysisDimensions_rowDblClick(e) {
    this.selectedJournalLine.DebitAccountSelectedAnalysisDimension[e.data.DimensionGroupId] = [e.data];
    this.debitAccount_dimensionGroupsIsOpened[e.data.DimensionGroupId] = false;
  }

  gvCreditAccountAnalysisDimensions_rowDblClick(e) {
    this.selectedJournalLine.CreditAccountSelectedAnalysisDimension[e.data.DimensionGroupId] = [e.data];
    this.creditAccount_dimensionGroupsIsOpened[e.data.DimensionGroupId] = false;
  }

  async getDimensionPostTypes() {
    if (!this.dimensionPostTypeDataSource.length)
      this.dimensionPostTypeDataSource = await this.generalService.sendRequest({}, '/finance/get-dimension-post-types');
  }

  async getDebitAccountDimensionGroups(debitAccountId: number) {
    this.selectedJournalLine.DebitAccountDimensionGroups = await this.generalService.sendRequest({ AccountId: debitAccountId }, '/finance/get-account-join-dimension-groups');

    this.selectedJournalLine.DebitAccountDimensionGroups.forEach(async element => {
      this.debitAccount_onRowExpanded({ key: element.Id });
    });
  }

  async getCreditAccountDimensionGroups(creditAccountId: number) {
    this.selectedJournalLine.CreditAccountDimensionGroups = await this.generalService.sendRequest({ AccountId: creditAccountId }, '/finance/get-account-join-dimension-groups');

    this.selectedJournalLine.CreditAccountDimensionGroups.forEach(async element => {
      this.creditAccount_onRowExpanded({ key: element.Id });
    });
  }

  checkDebitAccountDimensionGroupsRowExpanded(rowId) {
    const result = this.selectedJournalLine.DebitAccountDimensionGroups.find((r) => r.Id == rowId);
    return result ? result.PostType != 400 : false;
  }

  checkCreditAccountDimensionGroupsRowExpanded(rowId) {
    const result = this.selectedJournalLine.CreditAccountDimensionGroups.find((r) => r.Id == rowId);
    return result ? result.PostType != 400 : false;
  }

  // #endregion
  async journalLineSave(e) {
    if (e.itemData == 'Remove') {
      if (this.newRow) {
        this.journalLinePopupClosing();
      }
      else {
        this.newRow = false;
        await this.deleteSelectedRows();
        this.journalLinePopupClosing();
      }
      return;
    }

    if (this.selectedDebitAccount == null && this.selectedCreditAccount == null) {
      this.snackbar.open('Choose any debit or credit account.', 'Ok', { duration: 3000 });
      return;
    }

    if (this.selectedDebitAccount) {
      this.selectedJournalLine.DebitAccountId = this.selectedDebitAccount.Id;
      this.selectedJournalLine.DebitAccountCode = this.selectedDebitAccount.Code;
      this.selectedJournalLine.DebitAccountName = this.selectedDebitAccount.Name;
      this.selectedJournalLine.DebitAccountGroupCode = this.selectedDebitAccount.AccountGroupCode;
    }
    else {
      this.selectedJournalLine.DebitAccountId = 0;
      this.selectedJournalLine.DebitAccountCode = '';
      this.selectedJournalLine.DebitAccountName = '';
      this.selectedJournalLine.DebitAccountGroupCode = 0;
    }

    if (this.selectedCreditAccount) {
      this.selectedJournalLine.CreditAccountId = this.selectedCreditAccount.Id;
      this.selectedJournalLine.CreditAccountCode = this.selectedCreditAccount.Code;
      this.selectedJournalLine.CreditAccountName = this.selectedCreditAccount.Name;
      this.selectedJournalLine.CreditAccountGroupCode = this.selectedCreditAccount.AccountGroupCode;
    }
    else {
      this.selectedJournalLine.CreditAccountId = 0;
      this.selectedJournalLine.CreditAccountCode = '';
      this.selectedJournalLine.CreditAccountName = '';
      this.selectedJournalLine.CreditAccountGroupCode = 0;
    }

    if (this.selectedDebitAccount && this.selectedCreditAccount && this.selectedJournalLine.Amount < 0) {
      this.snackbar.open('Amount should not be negative.', 'Ok', { duration: 3000 });
      return;
    }

    if ((!this.selectedDebitAccount || !this.selectedCreditAccount) && e.itemData.includes("offset")) {
      this.snackbar.open('Credit and debit accounts must be selected to add an offset line.', 'Ok', { duration: 5000 });
      return;
    }

    if ((!this.selectedDebitAccount || !this.selectedCreditAccount) && this.journal.JournalModeId == 100) {
      this.snackbar.open('Both debit and credit accounts must be selected for the single-line mode.', 'Ok', { duration: 5000 });
      return;
    }

    if (e.itemData.includes("offset") && this.journal.JournalModeId == 100) {
      this.snackbar.open('Offset lines are not allowed in single-line mode.', 'Ok', { duration: 5000 });
      return;
    }

    let controls = ['LineDate', 'Amount', 'CurrencyCode', 'EntryTypeCode'];

    let messages = {
      LineDate: 'Choose any line date.',
      Amount: 'Fill in the amount.',
      CurrencyCode: 'Choose any currency.',
      EntryTypeCode: 'Choose any type.'
    };

    let resp = this.generalService.validateDataSource(this.selectedJournalLine, controls);

    if (resp) {
      for (let index = 0; index < this.selectedJournalLine.DebitAccountDimensionGroups.length; index++) {
        if (this.selectedJournalLine.DebitAccountDimensionGroups[index].PostType == 200) {
          const dGroupId = this.selectedJournalLine.DebitAccountDimensionGroups[index].Id;
          if (this.selectedJournalLine.DebitAccountSelectedAnalysisDimension[dGroupId] == null || this.selectedJournalLine.DebitAccountSelectedAnalysisDimension[dGroupId][0] == null) {
            this.snackbar.open('Operation failed.\n Fill in the dimension group: '
              + '[' + this.selectedJournalLine.DebitAccountDimensionGroups[index].Code + '] :: [' + this.selectedJournalLine.DebitAccountDimensionGroups[index].Name + ']', 'Ok', { duration: 3000 });
            this.debitAccountDimensionGroupsPopup = true;
            return;
          }
        }
      }

      for (let index = 0; index < this.selectedJournalLine.CreditAccountDimensionGroups.length; index++) {
        if (this.selectedJournalLine.CreditAccountDimensionGroups[index].PostType == 200) {
          const dGroupId = this.selectedJournalLine.CreditAccountDimensionGroups[index].Id;
          if (this.selectedJournalLine.CreditAccountSelectedAnalysisDimension[dGroupId] == null || this.selectedJournalLine.CreditAccountSelectedAnalysisDimension[dGroupId][0] == null) {
            this.snackbar.open('Operation failed.\n Fill in the dimension group: '
              + '[' + this.selectedJournalLine.CreditAccountDimensionGroups[index].Code + '] :: [' + this.selectedJournalLine.CreditAccountDimensionGroups[index].Name + ']', 'Ok', { duration: 3000 });
            this.creditAccountDimensionGroupsPopup = true;
            return;
          }
        }
      }
    }

    if (resp === true) {
      this.snackbar.open('Operation succeeded.', 'Ok', { duration: 3000 });
    }
    else {
      this.snackbar.open('Operation failed.\n' + messages[resp], 'Ok', { duration: 3000 });
      return;
    }

    switch (e.itemData) {
      // Save Line & Close
      case "Save Line & Close":
        {
          this.refSelectedJournalLine.LineDate = this.selectedJournalLine.LineDate;
          this.refSelectedJournalLine.CurrencyCode = this.selectedJournalLine.CurrencyCode;
          this.refSelectedJournalLine.Amount = this.selectedJournalLine.Amount;
          this.refSelectedJournalLine.OutstandingAmount = this.selectedJournalLine.Amount - this.selectedJournalLine.PaidAmount;
          this.refSelectedJournalLine.EntryTypeCode = this.selectedJournalLine.EntryTypeCode;
          this.refSelectedJournalLine.EntryExpirationDate = this.selectedJournalLine.EntryExpirationDate;

          this.refSelectedJournalLine.DebitAccountId = this.selectedJournalLine.DebitAccountId;
          this.refSelectedJournalLine.DebitAccountCode = this.selectedJournalLine.DebitAccountCode;
          this.refSelectedJournalLine.DebitAccountName = this.selectedJournalLine.DebitAccountName;
          this.refSelectedJournalLine.DebitAccountGroupCode = this.selectedJournalLine.DebitAccountGroupCode;

          this.refSelectedJournalLine.CreditAccountId = this.selectedJournalLine.CreditAccountId;
          this.refSelectedJournalLine.CreditAccountCode = this.selectedJournalLine.CreditAccountCode;
          this.refSelectedJournalLine.CreditAccountName = this.selectedJournalLine.CreditAccountName;
          this.refSelectedJournalLine.CreditAccountGroupCode = this.selectedJournalLine.CreditAccountGroupCode;

          this.refSelectedJournalLine.DocumentNumber = this.selectedJournalLine.DocumentNumber;
          this.refSelectedJournalLine.Note = this.selectedJournalLine.Note;

          // slice() == shallow copy
          this.refSelectedJournalLine.DebitAccountDimensionGroups = this.selectedJournalLine.DebitAccountDimensionGroups.slice();
          this.refSelectedJournalLine.DebitAccountSelectedAnalysisDimension = this.selectedJournalLine.DebitAccountSelectedAnalysisDimension.slice();

          this.refSelectedJournalLine.CreditAccountDimensionGroups = this.selectedJournalLine.CreditAccountDimensionGroups.slice();
          this.refSelectedJournalLine.CreditAccountSelectedAnalysisDimension = this.selectedJournalLine.CreditAccountSelectedAnalysisDimension.slice();

          {
            let jline = this.journal.Lines.filter(c => c.ParentId == this.refSelectedJournalLine.Id);
            if (jline.length) {
              jline[0].LineDate = this.selectedJournalLine.LineDate;
              jline[0].CurrencyCode = this.selectedJournalLine.CurrencyCode;
              jline[0].Amount = -this.selectedJournalLine.Amount;
              jline[0].EntryTypeCode = this.selectedJournalLine.EntryTypeCode;
              jline[0].EntryExpirationDate = this.selectedJournalLine.EntryExpirationDate;

              jline[0].CreditAccountId = this.selectedJournalLine.DebitAccountId;
              jline[0].CreditAccountCode = this.selectedJournalLine.DebitAccountCode;
              jline[0].CreditAccountName = this.selectedJournalLine.DebitAccountName;
              jline[0].CreditAccountGroupCode = this.selectedJournalLine.DebitAccountGroupCode;

              jline[0].DebitAccountId = this.selectedJournalLine.CreditAccountId;
              jline[0].DebitAccountCode = this.selectedJournalLine.CreditAccountCode;
              jline[0].DebitAccountName = this.selectedJournalLine.CreditAccountName;
              jline[0].DebitAccountGroupCode = this.selectedJournalLine.CreditAccountGroupCode;

              jline[0].Note = this.selectedJournalLine.Note;

              jline[0].DocumentNumber = this.selectedJournalLine.DocumentNumber;
            }
          }

          this.newRow = false;
          this.journalLinePopupClosing();
        }
        break;

      // Save as a new line
      case "Save as a new line":
      case "Save as a new line & Close":
        if (this.newRow) {
          this.refSelectedJournalLine.Id = -Number(new Date().getMilliseconds() + '' + this.journal.Lines.length);
          this.refSelectedJournalLine.JournalId = this.Id;
          this.refSelectedJournalLine.LineDate = this.selectedJournalLine.LineDate;
          this.refSelectedJournalLine.CurrencyCode = this.selectedJournalLine.CurrencyCode;
          this.refSelectedJournalLine.Amount = this.selectedJournalLine.Amount;
          this.refSelectedJournalLine.OutstandingAmount = this.selectedJournalLine.Amount - this.selectedJournalLine.PaidAmount;
          this.refSelectedJournalLine.EntryTypeCode = this.selectedJournalLine.EntryTypeCode;
          this.refSelectedJournalLine.EntryExpirationDate = this.selectedJournalLine.EntryExpirationDate;

          this.refSelectedJournalLine.DebitAccountId = this.selectedJournalLine.DebitAccountId;
          this.refSelectedJournalLine.DebitAccountCode = this.selectedJournalLine.DebitAccountCode;
          this.refSelectedJournalLine.DebitAccountName = this.selectedJournalLine.DebitAccountName;
          this.refSelectedJournalLine.DebitAccountGroupCode = this.selectedJournalLine.DebitAccountGroupCode;

          this.refSelectedJournalLine.CreditAccountId = this.selectedJournalLine.CreditAccountId;
          this.refSelectedJournalLine.CreditAccountCode = this.selectedJournalLine.CreditAccountCode;
          this.refSelectedJournalLine.CreditAccountName = this.selectedJournalLine.CreditAccountName;
          this.refSelectedJournalLine.CreditAccountGroupCode = this.selectedJournalLine.CreditAccountGroupCode;

          this.refSelectedJournalLine.DocumentNumber = this.selectedJournalLine.DocumentNumber;
          this.refSelectedJournalLine.Note = this.selectedJournalLine.Note;

          // slice() == shallow copy
          this.refSelectedJournalLine.DebitAccountDimensionGroups = this.selectedJournalLine.DebitAccountDimensionGroups.slice();
          this.refSelectedJournalLine.DebitAccountSelectedAnalysisDimension = this.selectedJournalLine.DebitAccountSelectedAnalysisDimension.slice();

          this.refSelectedJournalLine.CreditAccountDimensionGroups = this.selectedJournalLine.CreditAccountDimensionGroups.slice();
          this.refSelectedJournalLine.CreditAccountSelectedAnalysisDimension = this.selectedJournalLine.CreditAccountSelectedAnalysisDimension.slice();
        }
        else {
          let jCreditline = new JournalLine();
          jCreditline.Id = -Number(new Date().getMilliseconds() + '' + this.journal.Lines.length);
          jCreditline.JournalId = this.Id;
          jCreditline.LineDate = this.selectedJournalLine.LineDate;
          jCreditline.CurrencyCode = this.selectedJournalLine.CurrencyCode;
          jCreditline.Amount = this.selectedJournalLine.Amount;
          jCreditline.OutstandingAmount = this.selectedJournalLine.Amount - this.selectedJournalLine.PaidAmount;
          jCreditline.EntryTypeCode = this.selectedJournalLine.EntryTypeCode;
          jCreditline.EntryExpirationDate = this.selectedJournalLine.EntryExpirationDate;

          jCreditline.DebitAccountId = this.selectedJournalLine.DebitAccountId;
          jCreditline.DebitAccountCode = this.selectedJournalLine.DebitAccountCode;
          jCreditline.DebitAccountName = this.selectedJournalLine.DebitAccountName;
          jCreditline.DebitAccountGroupCode = this.selectedJournalLine.DebitAccountGroupCode;

          jCreditline.CreditAccountId = this.selectedJournalLine.CreditAccountId;
          jCreditline.CreditAccountCode = this.selectedJournalLine.CreditAccountCode;
          jCreditline.CreditAccountName = this.selectedJournalLine.CreditAccountName;
          jCreditline.CreditAccountGroupCode = this.selectedJournalLine.CreditAccountGroupCode;

          jCreditline.DocumentNumber = this.selectedJournalLine.DocumentNumber;
          jCreditline.Note = this.selectedJournalLine.Note;

          // slice() == shallow copy
          jCreditline.DebitAccountDimensionGroups = this.selectedJournalLine.DebitAccountDimensionGroups.slice();
          jCreditline.DebitAccountSelectedAnalysisDimension = this.selectedJournalLine.DebitAccountSelectedAnalysisDimension.slice();

          jCreditline.CreditAccountDimensionGroups = this.selectedJournalLine.CreditAccountDimensionGroups.slice();
          jCreditline.CreditAccountSelectedAnalysisDimension = this.selectedJournalLine.CreditAccountSelectedAnalysisDimension.slice();

          this.journal.Lines.push(jCreditline);
          this.selectedJournalLine.Amount = 0.0;
        }

        this.newRow = false;
        // Save as a new line & Close
        if (e.itemData == "Save as a new line & Close") {
          this.journalLinePopupClosing();
        }
        break;

      // Save Line & Add Offset Line
      case "Save line & Add offset line":
        this.refSelectedJournalLine.LineDate = this.selectedJournalLine.LineDate;
        this.refSelectedJournalLine.CurrencyCode = this.selectedJournalLine.CurrencyCode;
        this.refSelectedJournalLine.Amount = this.selectedJournalLine.Amount;
        this.refSelectedJournalLine.OutstandingAmount = this.selectedJournalLine.Amount - this.selectedJournalLine.PaidAmount;
        this.refSelectedJournalLine.EntryTypeCode = this.selectedJournalLine.EntryTypeCode;
        this.refSelectedJournalLine.EntryExpirationDate = this.selectedJournalLine.EntryExpirationDate;

        this.refSelectedJournalLine.DebitAccountId = this.selectedJournalLine.DebitAccountId;
        this.refSelectedJournalLine.DebitAccountCode = this.selectedJournalLine.DebitAccountCode;
        this.refSelectedJournalLine.DebitAccountName = this.selectedJournalLine.DebitAccountName;
        this.refSelectedJournalLine.DebitAccountGroupCode = this.selectedJournalLine.DebitAccountGroupCode;

        this.refSelectedJournalLine.CreditAccountId = this.selectedJournalLine.CreditAccountId;
        this.refSelectedJournalLine.CreditAccountCode = this.selectedJournalLine.CreditAccountCode;
        this.refSelectedJournalLine.CreditAccountName = this.selectedJournalLine.CreditAccountName;
        this.refSelectedJournalLine.CreditAccountGroupCode = this.selectedJournalLine.CreditAccountGroupCode;

        this.refSelectedJournalLine.DocumentNumber = this.selectedJournalLine.DocumentNumber;
        this.refSelectedJournalLine.Note = this.selectedJournalLine.Note;

        // slice() == shallow copy
        this.refSelectedJournalLine.DebitAccountDimensionGroups = this.selectedJournalLine.DebitAccountDimensionGroups.slice();
        this.refSelectedJournalLine.DebitAccountSelectedAnalysisDimension = this.selectedJournalLine.DebitAccountSelectedAnalysisDimension.slice();

        this.refSelectedJournalLine.CreditAccountDimensionGroups = this.selectedJournalLine.CreditAccountDimensionGroups.slice();
        this.refSelectedJournalLine.CreditAccountSelectedAnalysisDimension = this.selectedJournalLine.CreditAccountSelectedAnalysisDimension.slice();

        {
          let jline = this.journal.Lines.filter(c => c.ParentId == this.refSelectedJournalLine.Id);
          if (jline.length) {
            jline[0].LineDate = this.selectedJournalLine.LineDate;
            jline[0].CurrencyCode = this.selectedJournalLine.CurrencyCode;
            jline[0].Amount = -this.selectedJournalLine.Amount;
            jline[0].EntryTypeCode = this.selectedJournalLine.EntryTypeCode;
            jline[0].EntryExpirationDate = this.selectedJournalLine.EntryExpirationDate;

            jline[0].CreditAccountId = this.selectedJournalLine.DebitAccountId;
            jline[0].CreditAccountCode = this.selectedJournalLine.DebitAccountCode;
            jline[0].CreditAccountName = this.selectedJournalLine.DebitAccountName;
            jline[0].CreditAccountGroupCode = this.selectedJournalLine.DebitAccountGroupCode;

            jline[0].DebitAccountId = this.selectedJournalLine.CreditAccountId;
            jline[0].DebitAccountCode = this.selectedJournalLine.CreditAccountCode;
            jline[0].DebitAccountName = this.selectedJournalLine.CreditAccountName;
            jline[0].DebitAccountGroupCode = this.selectedJournalLine.CreditAccountGroupCode;

            jline[0].DocumentNumber = this.selectedJournalLine.DocumentNumber;
            jline[0].Note = this.selectedJournalLine.Note;
          }
        }

        // Add offset Line
        {
          let jline = new JournalLine();
          jline.Id = -Number(new Date().getMilliseconds() + '' + this.journal.Lines.length);
          jline.ParentId = this.refSelectedJournalLine.Id;
          jline.JournalId = this.Id;
          jline.LineDate = this.selectedJournalLine.LineDate;
          jline.CurrencyCode = this.selectedJournalLine.CurrencyCode;
          jline.Amount = -this.selectedJournalLine.Amount;
          jline.EntryTypeCode = this.selectedJournalLine.EntryTypeCode;
          jline.EntryExpirationDate = this.selectedJournalLine.EntryExpirationDate;

          jline.CreditAccountId = this.selectedJournalLine.DebitAccountId;
          jline.CreditAccountCode = this.selectedJournalLine.DebitAccountCode;
          jline.CreditAccountName = this.selectedJournalLine.DebitAccountName;
          jline.CreditAccountGroupCode = this.selectedJournalLine.DebitAccountGroupCode;

          jline.DebitAccountId = this.selectedJournalLine.CreditAccountId;
          jline.DebitAccountCode = this.selectedJournalLine.CreditAccountCode;
          jline.DebitAccountName = this.selectedJournalLine.CreditAccountName;
          jline.DebitAccountGroupCode = this.selectedJournalLine.CreditAccountGroupCode;

          jline.DocumentNumber = this.selectedJournalLine.DocumentNumber;
          jline.Note = this.selectedJournalLine.Note;

          this.journal.Lines.push(jline);
        }

        this.newRow = false;
        this.journalLinePopupClosing();
        break;

      // Save as a new line & Add offset line
      case this.journalLineButtonSettings[0]:
        {
          const lineId = -Number(new Date().getMilliseconds() + '' + this.journal.Lines.length);
          if (this.newRow) {
            this.refSelectedJournalLine.Id = lineId;
            this.refSelectedJournalLine.JournalId = this.Id;
            this.refSelectedJournalLine.LineDate = this.selectedJournalLine.LineDate;
            this.refSelectedJournalLine.CurrencyCode = this.selectedJournalLine.CurrencyCode;
            this.refSelectedJournalLine.Amount = this.selectedJournalLine.Amount;
            this.refSelectedJournalLine.OutstandingAmount = this.selectedJournalLine.Amount - this.selectedJournalLine.PaidAmount;
            this.refSelectedJournalLine.EntryTypeCode = this.selectedJournalLine.EntryTypeCode;
            this.refSelectedJournalLine.EntryExpirationDate = this.selectedJournalLine.EntryExpirationDate;

            this.refSelectedJournalLine.DebitAccountId = this.selectedJournalLine.DebitAccountId;
            this.refSelectedJournalLine.DebitAccountCode = this.selectedJournalLine.DebitAccountCode;
            this.refSelectedJournalLine.DebitAccountName = this.selectedJournalLine.DebitAccountName;
            this.refSelectedJournalLine.DebitAccountGroupCode = this.selectedJournalLine.DebitAccountGroupCode;

            this.refSelectedJournalLine.CreditAccountId = this.selectedJournalLine.CreditAccountId;
            this.refSelectedJournalLine.CreditAccountCode = this.selectedJournalLine.CreditAccountCode;
            this.refSelectedJournalLine.CreditAccountName = this.selectedJournalLine.CreditAccountName;
            this.refSelectedJournalLine.CreditAccountGroupCode = this.selectedJournalLine.CreditAccountGroupCode;

            this.refSelectedJournalLine.DocumentNumber = this.selectedJournalLine.DocumentNumber;
            this.refSelectedJournalLine.Note = this.selectedJournalLine.Note;

            // slice() == shallow copy
            this.refSelectedJournalLine.DebitAccountDimensionGroups = this.selectedJournalLine.DebitAccountDimensionGroups.slice();
            this.refSelectedJournalLine.DebitAccountSelectedAnalysisDimension = this.selectedJournalLine.DebitAccountSelectedAnalysisDimension.slice();

            this.refSelectedJournalLine.CreditAccountDimensionGroups = this.selectedJournalLine.CreditAccountDimensionGroups.slice();
            this.refSelectedJournalLine.CreditAccountSelectedAnalysisDimension = this.selectedJournalLine.CreditAccountSelectedAnalysisDimension.slice();
          }
          else {
            let jCreditline = new JournalLine();
            jCreditline.Id = lineId;
            jCreditline.JournalId = this.Id;
            jCreditline.LineDate = this.selectedJournalLine.LineDate;
            jCreditline.CurrencyCode = this.selectedJournalLine.CurrencyCode;

            jCreditline.Amount = this.selectedJournalLine.Amount;
            jCreditline.OutstandingAmount = this.selectedJournalLine.Amount - this.selectedJournalLine.PaidAmount;

            jCreditline.EntryTypeCode = this.selectedJournalLine.EntryTypeCode;
            jCreditline.EntryExpirationDate = this.selectedJournalLine.EntryExpirationDate;

            jCreditline.DebitAccountId = this.selectedJournalLine.DebitAccountId;
            jCreditline.DebitAccountCode = this.selectedJournalLine.DebitAccountCode;
            jCreditline.DebitAccountName = this.selectedJournalLine.DebitAccountName;
            jCreditline.DebitAccountGroupCode = this.selectedJournalLine.DebitAccountGroupCode;

            jCreditline.CreditAccountId = this.selectedJournalLine.CreditAccountId;
            jCreditline.CreditAccountCode = this.selectedJournalLine.CreditAccountCode;
            jCreditline.CreditAccountName = this.selectedJournalLine.CreditAccountName;
            jCreditline.CreditAccountGroupCode = this.selectedJournalLine.CreditAccountGroupCode;

            jCreditline.DocumentNumber = this.selectedJournalLine.DocumentNumber;
            jCreditline.Note = this.selectedJournalLine.Note;

            // slice() == shallow copy
            jCreditline.DebitAccountDimensionGroups = this.selectedJournalLine.DebitAccountDimensionGroups.slice();
            jCreditline.DebitAccountSelectedAnalysisDimension = this.selectedJournalLine.DebitAccountSelectedAnalysisDimension.slice();

            jCreditline.CreditAccountDimensionGroups = this.selectedJournalLine.CreditAccountDimensionGroups.slice();
            jCreditline.CreditAccountSelectedAnalysisDimension = this.selectedJournalLine.CreditAccountSelectedAnalysisDimension.slice();

            this.journal.Lines.push(jCreditline);
          }

          // Add offset line
          {
            let jCreditline = new JournalLine();
            jCreditline.Id = -Number(new Date().getMilliseconds() + '' + this.journal.Lines.length);
            jCreditline.JournalId = this.Id;
            jCreditline.ParentId = lineId;
            jCreditline.LineDate = this.selectedJournalLine.LineDate;
            jCreditline.CurrencyCode = this.selectedJournalLine.CurrencyCode;
            jCreditline.Amount = -this.selectedJournalLine.Amount;
            jCreditline.EntryTypeCode = this.selectedJournalLine.EntryTypeCode;
            jCreditline.EntryExpirationDate = this.selectedJournalLine.EntryExpirationDate;

            jCreditline.CreditAccountId = this.selectedJournalLine.DebitAccountId;
            jCreditline.CreditAccountCode = this.selectedJournalLine.DebitAccountCode;
            jCreditline.CreditAccountName = this.selectedJournalLine.DebitAccountName;
            jCreditline.CreditAccountGroupCode = this.selectedJournalLine.DebitAccountGroupCode;

            jCreditline.DebitAccountId = this.selectedJournalLine.CreditAccountId;
            jCreditline.DebitAccountCode = this.selectedJournalLine.CreditAccountCode;
            jCreditline.DebitAccountName = this.selectedJournalLine.CreditAccountName;
            jCreditline.DebitAccountGroupCode = this.selectedJournalLine.CreditAccountGroupCode;

            jCreditline.DocumentNumber = this.selectedJournalLine.DocumentNumber;
            jCreditline.Note = this.selectedJournalLine.Note;

            this.journal.Lines.push(jCreditline);
          }

          this.newRow = false;
          this.journalLinePopupClosing();
        }
        break;
    }

    this.dxDataGrid.instance.refresh();
    this.calculateDebitCreditSummary();
  }

  journalLinePopupClosing() {
    if (this.newRow) this.journal.Lines.pop();
    this.newRow = false;
    this.debitAccountDimensionGroupsPopup = this.creditAccountDimensionGroupsPopup = this.journalLinePopupVisible = false;
    this.dxDataGrid.instance.deselectAll();
    this.dxDataGrid.instance.refresh();
  }
  // #endregion

  debitAccountDimensionGroupsPopupClick() {
    this.debitAccountDimensionGroupsPopup = !this.debitAccountDimensionGroupsPopup;
  }

  creditAccountDimensionGroupsPopupClick() {
    this.creditAccountDimensionGroupsPopup = !this.creditAccountDimensionGroupsPopup;
  }

  floatToFixed3(num) {
    return Number(Number.parseFloat(num).toFixed(3));
  }
}
