import { Component, OnInit } from '@angular/core';

import CustomStore from 'devextreme/data/custom_store';
import { GeneralService } from 'src/app/shared/services/global.service';

@Component({
  selector: 'electronic-invoice-codes',
  templateUrl: './electronic-invoice-codes.component.html',
  styleUrls: ['./electronic-invoice-codes.component.scss']
})
export class ElectronicInvoiceCodesComponent implements OnInit {
  dataSource: any = {};
  NAME: string;
  CODE: string;

  constructor(private generalService: GeneralService) { }

  async ngOnInit() {
    this.getElectronicInvoiceCodes();
  }

  onFocusedRowChanged(e) {
    const rowData = e.row && e.row.data;

    if (rowData) {
      this.NAME = rowData.Name;
      this.CODE = rowData.Code;
    }
  }

  async getElectronicInvoiceCodes() {
    const _this = this;

    this.dataSource.store = new CustomStore({
      key: "Id",
      load: async function (loadOptions: any) {
        let request = { PageNumber: 0, RowCount: 10 };

        if (loadOptions.filter) {
          for (var i = 0; i < loadOptions.filter.length; i += 2) {
            if (typeof loadOptions.filter[i] == "string") {
              request[loadOptions.filter[i]] = loadOptions.filter[2];
              break;
            }
            else {
              request[loadOptions.filter[i][0]] = loadOptions.filter[i][2];
            }
          }
        }

        if (loadOptions.skip) request.PageNumber = loadOptions.skip;
        if (loadOptions.take) request.RowCount = loadOptions.take;

        return _this.generalService.sendRequest(request, '/finance/get-electronic-invoice-codes').then(r => {
          return {
            data: r,
            totalCount: r.length ? r[0].Total : 0
          };
        });
      }
    });
  }
}
