import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageLayoutModule } from '../../../../@hubcore/components/page-layout/page-layout.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BreadcrumbsModule } from '../../../../@hubcore/components/breadcrumbs/breadcrumbs.module';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { ElectronicInvoiceCodesRoutingModule } from './electronic-invoice-codes-routing.module';
import { DxDataGridModule } from 'devextreme-angular';

import { ElectronicInvoiceCodesComponent } from './electronic-invoice-codes.component';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [ElectronicInvoiceCodesComponent],
  imports: [
    CommonModule,
    ElectronicInvoiceCodesRoutingModule,
    PageLayoutModule,
    FlexLayoutModule,
    BreadcrumbsModule,
    ReactiveFormsModule,
    MatButtonToggleModule,
    DxDataGridModule,
    TranslateModule
  ],
  providers: []
})
export class ElectronicInvoiceCodesModule { }
