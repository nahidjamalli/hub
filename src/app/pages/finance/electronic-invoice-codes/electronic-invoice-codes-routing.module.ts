import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { VexRoutes } from '../../../../@hubcore/interfaces/vex-route.interface';
import { ElectronicInvoiceCodesComponent } from './electronic-invoice-codes.component';

const routes: VexRoutes = [
  {
    path: '',
    component: ElectronicInvoiceCodesComponent,
    data: {
      toolbarShadowEnabled: true
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ElectronicInvoiceCodesRoutingModule { }
