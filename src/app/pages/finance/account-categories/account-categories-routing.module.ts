import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { VexRoutes } from '../../../../@hubcore/interfaces/vex-route.interface';
import { AccountCategoriesComponent } from './account-categories.component';

const routes: VexRoutes = [
  {
    path: '',
    component: AccountCategoriesComponent,
    data: {
      toolbarShadowEnabled: true
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccountCategoriesRoutingModule { }
