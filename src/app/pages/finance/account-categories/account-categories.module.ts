import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageLayoutModule } from '../../../../@hubcore/components/page-layout/page-layout.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BreadcrumbsModule } from '../../../../@hubcore/components/breadcrumbs/breadcrumbs.module';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { AccountCategoriesComponent } from './account-categories.component';
import { AccountCategoriesRoutingModule } from './account-categories-routing.module';
import { DxTreeListModule } from 'devextreme-angular';

import { DxoExportModule } from 'devextreme-angular/ui/nested';
import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [AccountCategoriesComponent],
  imports: [
    CommonModule,
    AccountCategoriesRoutingModule,
    PageLayoutModule,
    FlexLayoutModule,
    BreadcrumbsModule,
    ReactiveFormsModule,
    MatButtonToggleModule,
    DxTreeListModule,
    DxoExportModule,
    TranslateModule
  ],
  providers: []
})
export class AccountCategoriesModule { }
