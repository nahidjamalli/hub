import { Component, OnInit, ViewChild } from '@angular/core';
import { DxTreeListComponent } from 'devextreme-angular';
import CustomStore from 'devextreme/data/custom_store';
import { GeneralService } from 'src/app/shared/services/global.service';

@Component({
  selector: 'account-categories',
  templateUrl: './account-categories.component.html'
})
export class AccountCategoriesComponent implements OnInit {
  @ViewChild(DxTreeListComponent, { static: false }) treeList: DxTreeListComponent;

  dataSource: any = {};
  dsParents: any;
  expanded: boolean = false;

  constructor(private generalService: GeneralService) { }

  ngOnInit() {
    this.getAccountCategories();
    this.getParents();
  }

  async getParents() {
    this.dsParents = await this.generalService.sendRequest({}, '/finance/get-account-category-parents');
  }

  onToolbarPreparing(e) {
    e.toolbarOptions.items.unshift(
      {
        location: 'before',
        widget: 'dxButton',
        options: {
          width: 130,
          text: 'Expand All',
          onClick: this.toggleAll.bind(this)
        }
      }, {
      location: 'before',
      widget: 'dxButton',
      options: {
        icon: 'refresh',
        onClick: this.refreshDataGrid.bind(this)
      }
    });
  }

  toggleAll(e) {
    this.expanded = !this.expanded;
    e.component.option({
      text: this.expanded ? 'Collapse All' : 'Expand All'
    });

    if (!this.expanded) this.collapseAll();
  }

  refreshDataGrid() {
    this.treeList.instance.refresh();
    this.getAccountCategories();
  }

  collapseAll() {
    this.dsParents.forEach(element => {
      this.treeList.instance.collapseRow(element.Id);
    });
  }

  async getAccountCategories() {
    const _this = this;

    this.dataSource.store = new CustomStore({
      key: "Id",
      load: async function (loadOptions: any) {
        let request = {};
        if (loadOptions.filter) {
          for (var i = 0; i < loadOptions.filter.length; i += 2) {
            if (typeof loadOptions.filter[i] == "string") {
              request[loadOptions.filter[i]] = loadOptions.filter[2];
              break;
            }
            else {
              request[loadOptions.filter[i][0]] = loadOptions.filter[i][2];
            }
          }
        }
        return _this.generalService.sendRequest(request, '/finance/get-account-categories').then(r => {
          return {
            data: r,
            totalCount: r.length ? r[0].Total : 0
          };
        });
      },
      insert: async (values): Promise<any> => {
        await _this.generalService.sendRequest(values, '/finance/save-account-category');
      },
      update: async function (updateOptions: any, updatedValues: any): Promise<any> {
        updateOptions = Object.assign({ Id: updateOptions }, updatedValues);
        await _this.generalService.sendRequest(updateOptions, '/finance/save-account-category');
      },
      remove: async function (deletedRow: any): Promise<any> {
        await _this.generalService.sendRequest({ Id: deletedRow }, '/finance/delete-account-category');
      }
    });
  }
}
