import { Component, OnInit, ViewChild } from '@angular/core';

import { DxTreeListComponent } from 'devextreme-angular';
import CustomStore from 'devextreme/data/custom_store';
import { GeneralService } from 'src/app/shared/services/global.service';

@Component({
  selector: 'analysis-dimensions',
  templateUrl: './analysis-dimensions.component.html'
})
export class AnalysisDimensionsComponent implements OnInit {
  @ViewChild('treeList') treeList: DxTreeListComponent;

  dimensionGroupsDataSource: any;
  expanded: boolean = false;
  analysisDimensionDataSource: any = [];

  constructor(private generalService: GeneralService) { }

  ngOnInit() {
    this.getDimensionGroups();
  }

  async getDimensionGroups() {
    this.dimensionGroupsDataSource = await this.generalService.sendRequest({}, '/finance/get-dimension-groups');
  }

  onToolbarPreparing(e) {
    e.toolbarOptions.items.unshift(
      {
        location: 'before',
        widget: 'dxButton',
        options: {
          width: 120,
          text: 'Expand All',
          onClick: this.toggleAll.bind(this)
        }
      }, {
      location: 'before',
      widget: 'dxButton',
      options: {
        icon: 'refresh',
        onClick: this.refreshDataGrid.bind(this)
      }
    });
  }

  toggleAll(e) {
    this.expanded = !this.expanded;
    e.component.option({
      text: this.expanded ? 'Collapse All' : 'Expand All'
    });

    if (!this.expanded) this.collapseAll();
  }

  refreshDataGrid() {
    this.treeList.instance.refresh();
    this.getDimensionGroups();
  }

  collapseAll() {
    let rows = this.treeList.instance.getVisibleRows();

    rows.forEach(element => {
      this.treeList.instance.collapseRow(element.key);
    });
  }

  async getAnalysisDimensions(dimensionGroupId) {
    const _this = this;

    this.analysisDimensionDataSource[dimensionGroupId] = {};
    this.analysisDimensionDataSource[dimensionGroupId].store = new CustomStore({
      key: "Id",
      load: async function (loadOptions: any) {

        let request = { DimensionGroupId: dimensionGroupId };
        return _this.generalService.sendRequest(request, '/finance/get-analysis-dimensions').then(r => {
          return {
            data: r
          };
        });
      },
      insert: async (values): Promise<any> => {
        values.DimensionGroupId = dimensionGroupId;
        await _this.generalService.sendRequest(values, '/finance/save-analysis-dimension');
      },
      update: async function (updateOptions: any, updatedValues: any): Promise<any> {
        updateOptions = Object.assign({ Id: updateOptions }, updatedValues);
        updateOptions.DimensionGroupId = dimensionGroupId;
        await _this.generalService.sendRequest(updateOptions, '/finance/save-analysis-dimension');
      },
      remove: async function (deletedRow: any): Promise<any> {
        await _this.generalService.sendRequest({ Id: deletedRow }, '/finance/delete-analysis-dimension');
      }
    });
  }

  onRowExpanded(e) {
    if (!this.analysisDimensionDataSource[e.key])
      this.getAnalysisDimensions(e.key);
  }

  dimensionGroup_onRowInserting(e) {
    let x = this.generalService.sendRequest(e.data, '/finance/save-dimension-group');
    x.then(x => this.dimensionGroupsDataSource[this.dimensionGroupsDataSource.length - 1].Id = x.ROWID);
    e.cancel = false;
  }

  dimensionGroup_onRowUpdating(e) {
    let updatedRow = Object.assign(e.oldData, e.newData);
    this.generalService.sendRequest(updatedRow, '/finance/save-dimension-group');
    e.cancel = false;
  }

  dimensionGroup_onRowRemoving(e) {
    this.generalService.sendRequest(e.data, '/finance/delete-dimension-group');
    e.cancel = false;
  }
}
