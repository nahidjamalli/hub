import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { VexRoutes } from '../../../../@hubcore/interfaces/vex-route.interface';
import { AnalysisDimensionsComponent } from './analysis-dimensions.component';

const routes: VexRoutes = [
  {
    path: '',
    component: AnalysisDimensionsComponent,
    data: {
      toolbarShadowEnabled: true
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AnalysisDimensionsRoutingModule { }
