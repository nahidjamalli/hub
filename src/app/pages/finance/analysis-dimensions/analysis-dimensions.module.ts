import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageLayoutModule } from '../../../../@hubcore/components/page-layout/page-layout.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BreadcrumbsModule } from '../../../../@hubcore/components/breadcrumbs/breadcrumbs.module';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { AnalysisDimensionsComponent } from './analysis-dimensions.component';
import { AnalysisDimensionsRoutingModule } from './analysis-dimensions-routing.module';
import { DxDataGridModule, DxTreeListModule } from 'devextreme-angular';

import { TranslateModule } from '@ngx-translate/core';

@NgModule({
  declarations: [AnalysisDimensionsComponent],
  imports: [
    CommonModule,
    AnalysisDimensionsRoutingModule,
    PageLayoutModule,
    FlexLayoutModule,
    BreadcrumbsModule,
    ReactiveFormsModule,
    MatButtonToggleModule,
    DxDataGridModule,
    DxTreeListModule,
    TranslateModule
  ],
  providers: []
})
export class AnalysisDimensionsModule { }
