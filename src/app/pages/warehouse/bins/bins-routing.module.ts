import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { VexRoutes } from 'src/@hubcore/interfaces/vex-route.interface';
import { BinsComponent } from './bins.component';

const routes: VexRoutes = [
  {
    path: '',
    component: BinsComponent,
    data: {
      toolbarShadowEnabled: true
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]

})
export class BinsRoutingModule { }
