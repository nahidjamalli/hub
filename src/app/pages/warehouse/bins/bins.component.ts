import { Component, OnInit } from "@angular/core";
import { GeneralService } from "src/app/shared/services/global.service";

@Component({
    selector: "bins",
    templateUrl: './bins.component.html'
})

export class BinsComponent implements OnInit {
    dataSource: any = {};
    dsBinType: any = {};
    dsZones: any = {};
    constructor(private generalService: GeneralService) { }

    async ngOnInit() {
        this.dataSource = await this.generalService.sendRequest({}, '/warehouse/get-bins');
        this.dsBinType = await this.generalService.sendRequest({}, '/warehouse/get-bin-types');
        this.dsZones = await this.generalService.sendRequest({}, '/warehouse/get-zones');
    }

    onRowInserting(e) {
        let x = this.generalService.sendRequest(e.data, '/warehouse/save-bin');
        x.then(x => {
            this.dataSource[this.dataSource.length - 1].Id = x.ROWID;
        });
    }

    onRowUpdating(e) {
        let updatedRow = Object.assign(e.oldData, e.newData);
        this.generalService.sendRequest(updatedRow, '/warehouse/save-bin');
    }

    onRowRemoving(e) {
        this.generalService.sendRequest({ Id: e.data.Id }, '/warehouse/delete-bin');
    }
}