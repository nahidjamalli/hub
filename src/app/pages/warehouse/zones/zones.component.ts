import { Component, OnInit } from "@angular/core";
import { GeneralService } from "src/app/shared/services/global.service";

@Component({
    selector: "zones",
    templateUrl: './zones.component.html'
})

export class ZonesComponent implements OnInit {

    dataSource: any = {};
    dsBinType: any = {};

    constructor(private generalService: GeneralService) { }

    async ngOnInit() {
        this.dataSource = await this.generalService.sendRequest({}, '/warehouse/get-zones');
        this.dsBinType = await this.generalService.sendRequest({}, '/warehouse/get-bin-types');

    }

    onRowInserting(e) {
        let x = this.generalService.sendRequest(e.data, '/warehouse/save-zone');
        x.then(x => {
            this.dataSource[this.dataSource.length - 1].Id = x.ROWID;
        });
    }

    onRowUpdating(e) {
        let updatedRow = Object.assign(e.oldData, e.newData);
        this.generalService.sendRequest(updatedRow, '/warehouse/save-zone');
    }

    onRowRemoving(e) {
        this.generalService.sendRequest({ Id: e.data.Id }, '/warehouse/delete-zone');
    }
}