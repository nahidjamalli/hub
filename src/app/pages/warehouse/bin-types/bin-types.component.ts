import { Component, OnInit } from "@angular/core";
import { GeneralService } from "src/app/shared/services/global.service";

@Component({
    selector: "bin-types",
    templateUrl: './bin-types.component.html'
})

export class BinTypesComponent implements OnInit {

    dataSource: any = {};

    constructor(private generalService: GeneralService) { }

    async ngOnInit() {
        this.dataSource = await this.generalService.sendRequest({}, '/warehouse/get-bin-types');
    }

    onRowInserting(e) {
        let x = this.generalService.sendRequest(e.data, '/warehouse/save-bin-type');
        x.then(x => {
            this.dataSource[this.dataSource.length - 1].Id = x.ROWID;
        });
    }

    onRowUpdating(e) {
        let updatedRow = Object.assign(e.oldData, e.newData);
        this.generalService.sendRequest(updatedRow, '/warehouse/save-bin-type');
    }

    onRowRemoving(e) {
        this.generalService.sendRequest({ Id: e.data.Id }, '/warehouse/delete-bin-type');
    }
}