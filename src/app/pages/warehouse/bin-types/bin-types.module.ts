import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FlexLayoutModule } from "@angular/flex-layout";
import { ReactiveFormsModule } from "@angular/forms";
import { MatButtonToggleModule } from "@angular/material/button-toggle";
import { TranslateModule } from "@ngx-translate/core";
import { DxButtonModule, DxDataGridModule, DxFormModule } from "devextreme-angular";
import { BreadcrumbsModule } from "src/@hubcore/components/breadcrumbs/breadcrumbs.module";
import { PageLayoutModule } from "src/@hubcore/components/page-layout/page-layout.module";
import { GeneralService } from "src/app/shared/services/global.service";
import { BinTypesRoutingModule } from "./bin-types-routing.module";
import { BinTypesComponent } from "./bin-types.component";

@NgModule({
    declarations: [BinTypesComponent],
    imports: [
        CommonModule,
        BinTypesRoutingModule,
        PageLayoutModule,
        FlexLayoutModule,
        BreadcrumbsModule,
        ReactiveFormsModule,
        MatButtonToggleModule,
        TranslateModule,
        DxDataGridModule,
        DxFormModule,
        DxButtonModule
    ],
    providers: []
})

export class BinTypesModule { }