import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import CustomStore from 'devextreme/data/custom_store';
import { DxDropDownBoxComponent } from 'devextreme-angular';
import { MatSnackBar } from '@angular/material/snack-bar';
import { GeneralService } from 'src/app/shared/services/global.service';

@Component({
  selector: 'crud-role-demand',
  templateUrl: './crud-role.component.html',
  styleUrls: ['./crud-role.component.scss']
})
export class CrudRoleComponent implements OnInit {
  @ViewChild('permission') permissionPopup: DxDropDownBoxComponent;

  Id: number;
  user: any;

  dsPermissions: any = {};
  dsSelectedPermissions: any = {};
  selectedPermission: any = [];

  // USERS
  dsRole: any = {};

  constructor(private generalService: GeneralService, private route: ActivatedRoute, private snackbar: MatSnackBar, private router: Router) {
    this.Id = Number(this.route.snapshot.params.id);
  }

  async ngOnInit() {
    try {
      this.user = JSON.parse(localStorage.getItem('user'));
    } catch {
      this.router.navigate(['/signin']);
      return;
    }

    this.createStore();
    if (this.Id == 0) {
      this.dsRole = {};
    }
    else {
      this.getRoleJoinPermissions();
      this.getRole();
    }
  }

  async getRole() {
    this.dsRole = await this.generalService.sendRequest({ Id: this.Id }, '/admin/get-role');
  }

  async getRoleJoinPermissions() {
    this.dsSelectedPermissions = await this.generalService.sendRequest({ Id: this.Id }, '/admin/get-role-join-permissions');
  }

  async approve() {
    let messages = {
      Name: 'Fill in the name.'
    };

    const controls = ['Name'];

    let resp = this.generalService.validateDataSource(this.dsRole, controls);
    if (resp === true) {
      let resp = await this.generalService.sendRequest(this.dsRole, '/admin/save-role');

      if (resp && resp.ROWID) {
        if (!this.Id) {
          this.Id = resp.ROWID;
          this.dsRole['Code'] = resp.Code;
        }

        this.snackbar.open('Operation succeeded.', 'Ok', { duration: 3000 });
      }
      else {
        this.snackbar.open('Operation succeeded.', 'Ok', { duration: 3000 });
        await this.router.navigate(['/administration/roles']);
      }
    } else {
      this.snackbar.open('Operation failed. ' + messages[resp], 'Ok', { duration: 3000 });
    }
  }

  async onRowRemoved(e) {
    await this.generalService.sendRequest(e.data, '/admin/delete-role-join-permission');
  }

  permissions_displayValueFormatter(item) {
    item = item[0];
    return item && item.Code + ' - ' + item.Name;
  }

  async gvPermissions_rowDblClick(e) {
    let result = this.dsSelectedPermissions.find((r) => {
      return r.PermissionId == e.data.Id;
    });

    if (!result) {
      const row_id = await this.generalService.sendRequest({ RoleId: this.Id, PermissionId: e.data.Id }, '/admin/save-role-join-permission');
      this.dsSelectedPermissions.push({
        Id: row_id.ROWID,
        PermissionId: e.data.Id,
        Code: e.data.Code,
        Name: e.data.Name
      });
    }

    this.permissionPopup.instance.close();
  }

  createStore() {
    const _this = this;

    this.dsPermissions.store = new CustomStore({
      load: async function (loadOptions: any) {
        let request = {};
        if (loadOptions.filter) {
          for (var i = 0; i < loadOptions.filter.length; i += 2) {
            if (typeof loadOptions.filter[i] == "string") {
              request[loadOptions.filter[i]] = loadOptions.filter[2];
              break;
            }
            else {
              request[loadOptions.filter[i][0]] = loadOptions.filter[i][2];
            }
          }
        }

        if (!Object.entries(request).length) return null;
        else return _this.generalService.sendRequest(request, '/admin/get-permissions').then(r => {
          return {
            data: r
          };
        });
      },
      byKey: function () { return _this.dsPermissions; }
    });
  }
}
