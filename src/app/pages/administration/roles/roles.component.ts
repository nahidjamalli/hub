import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';
import { GeneralService } from 'src/app/shared/services/global.service';

@Component({
  selector: 'roles',
  templateUrl: './roles.component.html'
})
export class RolesComponent implements OnInit {
  dataSource: any;

  constructor(private generalService: GeneralService, private router: Router) { }

  async ngOnInit() {
    this.dataSource = await this.generalService.sendRequest({}, '/admin/get-roles');
  }

  onRowRemoving(e) {
    this.generalService.sendRequest(e.data, '/admin/delete-role');
    e.cancel = false;
  }

  onInitNewRow(e) {
    this.router.navigate(['/administration/crud-role/0']);
    e.cancel = true;
  }

  onEditingStart(e) {
    this.router.navigate(['/administration/crud-role/' + e.data.Id]);
    e.cancel = true;
  }
}
