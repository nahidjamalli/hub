import { Component, OnInit, ViewChild } from '@angular/core';

import { ActivatedRoute, Router } from '@angular/router';
import CustomStore from 'devextreme/data/custom_store';
import { DxDropDownBoxComponent } from 'devextreme-angular';
import { MatSnackBar } from '@angular/material/snack-bar';
import { GeneralService } from 'src/app/shared/services/global.service';

@Component({
  selector: 'crud-user-demand',
  templateUrl: './crud-user.component.html',
  styleUrls: ['./crud-user.component.scss']
})
export class CrudUserComponent implements OnInit {
  @ViewChild('employee') employeePopup: DxDropDownBoxComponent;
  @ViewChild('permission') permissionPopup: DxDropDownBoxComponent;
  @ViewChild('role') rolePopup: DxDropDownBoxComponent;

  Id: number;
  user: any;
  selectedEmployee: any = [];
  password: string;
  passwordMode: string;
  passwordButton: any;

  // USERS
  dsUser: any = {};

  // EMPLOYEES
  dsEmployees: any = {};

  // GROUPS
  dsGroups: any;
  groupEditorOptions = {};

  // STATUSES
  dsStatuses: any = {};
  statusEditorOptions = {};

  // ROLES
  dsRoles: any = {};
  dsSelectedRoles: any = [];
  selectedRole: any[] = [];

  // PERMISSIONS
  dsPermissions: any = {};
  dsSelectedPermissions: any = [];
  selectedPermission: any[] = [];

  constructor(private generalService: GeneralService, private route: ActivatedRoute, private snackbar: MatSnackBar, private router: Router) {
    this.Id = Number(this.route.snapshot.params.id);

    this.passwordMode = 'password';
    this.passwordButton = {
      icon: "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAACQAAAAkCAYAAADhAJiYAAAABmJLR0QA/wD/AP+gvaeTAAAACXBIWXMAAAsTAAALEwEAmpwYAAAB7klEQVRYw+2YP0tcQRTFz65xFVJZpBBS2O2qVSrRUkwqYfUDpBbWQu3ELt/HLRQ/Q8RCGxVJrRDEwj9sTATxZ/Hugo4zL/NmV1xhD9xi59177pl9986fVwLUSyi/tYC+oL6gbuNDYtyUpLqkaUmfJY3a+G9JZ5J2JW1J2ivMDBSxeWCfeBxYTHSOWMcRYLOAEBebxtEVQWPASQdi2jgxro4E1YDTQIJjYM18hszGbew4EHNq/kmCvgDnHtI7YBko58SWgSXg1hN/btyFBM0AlwExczG1YDZrMS4uLUeUoDmgFfjLGwXEtG05wNXyTc4NXgzMCOAIGHD8q0ATuDZrempkwGJ9+AfUQ4K+A/eEseqZ/UbgdUw4fqs5vPeW+5mgBvBAPkLd8cPju+341P7D/WAaJGCdOFQI14kr6o/zvBKZYz11L5Okv5KGA89Kzu9K0b0s5ZXt5PjuOL6TRV5ZalFP4F+rrnhZ1Cs5vN6ijmn7Q162/ThZq9+YNW3MbfvDAOed5cxdGL+RFaUPKQtjI8DVAr66/u9i6+jJzTXm+HFEVqxVYBD4SNZNKzk109HxoycPaG0bIeugVDTp4hH2qdXJDu6xOAAWiuQoQdLHhvY1aEZSVdInG7+Q9EvSz9RrUKqgV0PP3Vz7gvqCOsUj+CxC9LB1Dc8AAAASdEVYdEVYSUY6T3JpZW50YXRpb24AMYRY7O8AAAAASUVORK5CYII=",
      type: "default",
      onClick: () => {
        this.passwordMode = this.passwordMode === "text" ? "password" : "text";
      }
    }
  }

  ngOnInit() {
    try {
      this.user = JSON.parse(localStorage.getItem('user'));
    }
    catch {
      this.router.navigate(['/signin']);
      return;
    }

    if (this.Id != 0) {
      this.getUser();
      this.getSelectedPermissions();
      this.getSelectedRoles();
    }
    else {
      this.dsUser = {};
    }

    this.createStore();
    this.getGroups();
    this.getStatuses();
    this.rolesCreateStore();
    this.permissionsCreateStore();
  }

  async getSelectedPermissions() {
    this.dsSelectedPermissions = await this.generalService.sendRequest({ UserId: this.Id }, '/admin/get-user-join-permissions');
  }

  async getSelectedRoles() {
    this.dsSelectedRoles = await this.generalService.sendRequest({ UserId: this.Id }, '/admin/get-user-join-roles');
  }

  async getUser() {
    this.dsUser = await this.generalService.sendRequest({ Id: this.Id }, '/admin/get-user');
    this.password = this.dsUser['Password'];

    if (this.Id != 0 && this.dsUser == null) {
      await this.router.navigate(['/*']);
      return;
    }

    this.selectedEmployee = [await this.generalService.sendRequest({ Id: this.dsUser.EmployeeId }, '/human/get-employee')];
  }

  async approve() {
    let messages = {
      EmployeeId: 'Choose any employee.',
      GroupId: 'Choose any group.',
      StatusId: 'Choose any status.',
      Username: 'Fill in the username.',
      Password: 'Fill in the password.'
    };

    if (this.selectedEmployee[0]) this.dsUser['EmployeeId'] = this.selectedEmployee[0].Id;
    this.dsUser['Password'] = this.password;

    const controls = ['EmployeeId', 'GroupId', 'StatusId', 'Username', 'Password'];

    let resp = this.generalService.validateDataSource(this.dsUser, controls);
    if (resp === true) {
      let resp = await this.generalService.sendRequest(this.dsUser, '/admin/save-user');

      if (resp && resp.ROWID) {
        if (!this.Id) {
          this.Id = resp.ROWID;
        }

        this.snackbar.open('Operation succeeded.', 'Ok', { duration: 3000 });
      }
      else {
        this.snackbar.open('Operation succeeded.', 'Ok', { duration: 3000 });
        await this.router.navigate(['/administration/users']);
      }
    } else {
      this.snackbar.open('Operation failed. ' + messages[resp], 'Ok', { duration: 3000 });
    }
  }

  async getGroups() {
    this.dsGroups = await this.generalService.sendRequest({}, '/admin/get-groups');
    this.groupEditorOptions = { placeholder: 'Select a group...', dataSource: this.dsGroups, valueExpr: 'Id', displayExpr: 'Name' };
  }

  async getStatuses() {
    this.dsStatuses = await this.generalService.sendRequest({ StatusTypeId: 65, UserId: -2 }, '/config/get-statuses');
    this.statusEditorOptions = { dataSource: this.dsStatuses, valueExpr: "Id", displayExpr: "Name" };
  }

  async roles_onRowRemoved(e) {
    await this.generalService.sendRequest(e.data, '/admin/delete-user-join-role');
  }

  async permissions_onRowRemoved(e) {
    await this.generalService.sendRequest(e.data, '/admin/delete-user-join-permission');
  }

  employees_displayValueFormatter(item) {
    item = item[0];
    return item && item.Firstname + ' ' + item.Lastname;
  }

  gvEmployees_rowDblClick() {
    this.employeePopup.instance.close();
  }

  async gvRoles_rowDblClick(e) {
    let result = this.dsSelectedRoles.find((r) => {
      return r.RoleId == e.data.Id;
    });

    if (!result) {
      const row_id = await this.generalService.sendRequest({ UserId: this.Id, RoleId: e.data.Id }, '/admin/save-user-join-role');
      this.dsSelectedRoles.push({
        Id: row_id.ROWID,
        RoleId: e.data.Id,
        Code: e.data.Code,
        Name: e.data.Name
      });
    }

    this.rolePopup.instance.close();
  }

  async gvPermissions_rowDblClick(e) {
    let result = this.dsSelectedPermissions.find((r) => {
      return r.PermissionId == e.data.Id;
    });

    if (!result) {
      const row_id = await this.generalService.sendRequest({ UserId: this.Id, PermissionId: e.data.Id }, '/admin/save-user-join-permission');
      this.dsSelectedPermissions.push({
        Id: row_id.ROWID,
        PermissionId: e.data.Id,
        Code: e.data.Code,
        Name: e.data.Name
      });
    }

    this.permissionPopup.instance.close();
  }

  permissions_displayValueFormatter(item) {
    item = item[0];
    return item && item.Code + ' - ' + item.Name;
  }

  roles_displayValueFormatter(item) {
    item = item[0];
    return item && item.Code + ' - ' + item.Name;
  }

  rolesCreateStore() {
    const _this = this;

    this.dsRoles.store = new CustomStore({
      load: async function (loadOptions: any) {
        let request = {};
        if (loadOptions.filter) {
          for (var i = 0; i < loadOptions.filter.length; i += 2) {
            if (typeof loadOptions.filter[i] == "string") {
              request[loadOptions.filter[i]] = loadOptions.filter[2];
              break;
            }
            else {
              request[loadOptions.filter[i][0]] = loadOptions.filter[i][2];
            }
          }
        }

        if (!Object.entries(request).length) return null;
        else return _this.generalService.sendRequest(request, '/admin/get-roles').then(r => {
          return {
            data: r
          };
        });
      },
      byKey: function () { return _this.dsRoles; }
    });
  }

  permissionsCreateStore() {
    const _this = this;

    this.dsPermissions.store = new CustomStore({
      load: async function (loadOptions: any) {
        let request = {};
        if (loadOptions.filter) {
          for (var i = 0; i < loadOptions.filter.length; i += 2) {
            if (typeof loadOptions.filter[i] == "string") {
              request[loadOptions.filter[i]] = loadOptions.filter[2];
              break;
            }
            else {
              request[loadOptions.filter[i][0]] = loadOptions.filter[i][2];
            }
          }
        }

        if (!Object.entries(request).length) return null;
        else return _this.generalService.sendRequest(request, '/admin/get-permissions').then(r => {
          return {
            data: r
          };
        });
      },
      byKey: function () { return _this.dsPermissions; }
    });
  }

  createStore() {
    const _this = this;

    this.dsEmployees.store = new CustomStore({
      load: async function (loadOptions: any) {
        let request = {};
        if (loadOptions.filter) {
          for (var i = 0; i < loadOptions.filter.length; i += 2) {
            if (typeof loadOptions.filter[i] == "string") {
              request[loadOptions.filter[i]] = loadOptions.filter[2];
              break;
            }
            else {
              request[loadOptions.filter[i][0]] = loadOptions.filter[i][2];
            }
          }
        }

        if (!Object.entries(request).length) return null;
        else return _this.generalService.sendRequest(request, '/human/get-employees').then(r => {
          return {
            data: r
          };
        });
      },
      byKey: function () { return _this.dsEmployees; }
    });
  }
}
