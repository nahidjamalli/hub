import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageLayoutModule } from '../../../../@hubcore/components/page-layout/page-layout.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BreadcrumbsModule } from '../../../../@hubcore/components/breadcrumbs/breadcrumbs.module';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { CrudUserRoutingModule } from './crud-user-routing.module';
import { DxFormModule, DxDataGridModule, DxValidatorModule, DxDropDownBoxModule, DxTextBoxModule, DxTextAreaModule } from 'devextreme-angular';

import { MatSnackBarModule } from '@angular/material/snack-bar';
import { CrudUserComponent } from './crud-user.component';
import { DxiItemModule } from 'devextreme-angular/ui/nested';
import { DxButtonModule } from 'devextreme-angular';
import { MatTabsModule } from '@angular/material/tabs';

@NgModule({
  declarations: [CrudUserComponent],
  imports: [
    MatSnackBarModule,
    CommonModule,
    CrudUserRoutingModule,
    PageLayoutModule,
    FlexLayoutModule,
    BreadcrumbsModule,
    ReactiveFormsModule,
    MatButtonToggleModule,
    DxFormModule,
    DxiItemModule,
    DxButtonModule,
    DxValidatorModule,
    DxDropDownBoxModule,
    DxDataGridModule,
    MatTabsModule,
    DxTextBoxModule,
    DxTextAreaModule
  ],
  providers: []
})
export class CrudUserModule { }
