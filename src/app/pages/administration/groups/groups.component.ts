import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';
import { GeneralService } from 'src/app/shared/services/global.service';

@Component({
  selector: 'groups',
  templateUrl: './groups.component.html'
})
export class GroupsComponent implements OnInit {
  dataSource: any;

  constructor(private generalService: GeneralService, private router: Router) { }

  async ngOnInit() {
    this.dataSource = await this.generalService.sendRequest({}, '/admin/get-groups');
  }

  onRowRemoving(e) {
    this.generalService.sendRequest(e.data, '/admin/delete-group');
    e.cancel = false;
  }

  onInitNewRow(e) {
    this.router.navigate(['/administration/crud-group/0']);
    e.cancel = true;
  }

  onEditingStart(e) {
    this.router.navigate(['/administration/crud-group/' + e.data.Id]);
    e.cancel = true;
  }
}
