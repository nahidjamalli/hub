import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';
import { GeneralService } from 'src/app/shared/services/global.service';

@Component({
  selector: 'users',
  templateUrl: './users.component.html'
})
export class UsersComponent implements OnInit {
  dataSource: any;

  constructor(private generalService: GeneralService, private router: Router) { }

  async ngOnInit() {
    this.dataSource = await this.generalService.sendRequest({}, '/admin/get-users');
  }

  onRowRemoving(e) {
    this.generalService.sendRequest(e.data, '/admin/delete-user');
    e.cancel = false;
  }

  onInitNewRow(e) {
    this.router.navigate(['/administration/crud-user/0']);
    e.cancel = true;
  }

  onEditingStart(e) {
    this.router.navigate(['/administration/crud-user/' + e.data.Id]);
    e.cancel = true;
  }
}
