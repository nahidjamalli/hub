import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import CustomStore from 'devextreme/data/custom_store';
import { DxDropDownBoxComponent } from 'devextreme-angular';
import { MatSnackBar } from '@angular/material/snack-bar';
import { GeneralService } from 'src/app/shared/services/global.service';

@Component({
  selector: 'crud-group-demand',
  templateUrl: './crud-group.component.html',
  styleUrls: ['./crud-group.component.scss']
})
export class CrudGroupComponent implements OnInit {
  @ViewChild('permission') permissionPopup: DxDropDownBoxComponent;
  @ViewChild('role') rolePopup: DxDropDownBoxComponent;

  Id: number;
  user: any;

  // USERS
  dsGroup: any = {};

  // GROUPS
  dsGroups: any;
  groupEditorOptions = {};

  // STATUSES
  dsStatuses: any = {};
  statusEditorOptions = {};

  // ROLES
  dsRoles: any = {};
  dsSelectedRoles: any = [];
  selectedRole: any[] = [];

  // PERMISSIONS
  dsPermissions: any = {};
  dsSelectedPermissions: any = [];
  selectedPermission: any[] = [];

  constructor(private generalService: GeneralService, private route: ActivatedRoute, private snackbar: MatSnackBar, private router: Router) {
    this.Id = Number(this.route.snapshot.params.id);
  }

  ngOnInit() {
    try {
      this.user = JSON.parse(localStorage.getItem('user'));
    }
    catch {
      this.router.navigate(['/signin']);
      return;
    }

    if (this.Id != 0) {
      this.getGroup();
      this.getSelectedPermissions();
      this.getSelectedRoles();
    }
    else {
      this.dsGroup = {};
    }

    this.getGroups();
    this.getStatuses();
    this.rolesCreateStore();
    this.permissionsCreateStore();
  }

  async getSelectedPermissions() {
    this.dsSelectedPermissions = await this.generalService.sendRequest({ GroupId: this.Id }, '/admin/get-group-join-permissions');
  }

  async getSelectedRoles() {
    this.dsSelectedRoles = await this.generalService.sendRequest({ GroupId: this.Id }, '/admin/get-group-join-roles');
  }

  async getGroup() {
    this.dsGroup = await this.generalService.sendRequest({ Id: this.Id }, '/admin/get-group');

    if (this.Id != 0 && this.dsGroup == null) {
      await this.router.navigate(['/*']);
      return;
    }
  }

  async approve() {
    let messages = {
      Name: 'Fill in the name.'
    };

    const controls = ['Name'];

    let resp = this.generalService.validateDataSource(this.dsGroup, controls);
    if (resp === true) {
      let resp = await this.generalService.sendRequest(this.dsGroup, '/admin/save-group');

      if (resp && resp.ROWID) {
        if (!this.Id) {
          this.Id = resp.ROWID;
        }

        this.snackbar.open('Operation succeeded.', 'Ok', { duration: 3000 });
      }
      else {
        this.snackbar.open('Operation succeeded.', 'Ok', { duration: 3000 });
        await this.router.navigate(['/administration/groups']);
      }
    } else {
      this.snackbar.open('Operation failed. ' + messages[resp], 'Ok', { duration: 3000 });
    }
  }

  async getGroups() {
    this.dsGroups = await this.generalService.sendRequest({}, '/admin/get-groups');
    this.groupEditorOptions = { placeholder: 'Select a group...', dataSource: this.dsGroups, valueExpr: 'Id', displayExpr: 'Name' };
  }

  async getStatuses() {
    this.dsStatuses = await this.generalService.sendRequest({ StatusTypeId: 65, GroupId: -2 }, '/config/get-statuses');
    this.statusEditorOptions = { dataSource: this.dsStatuses, valueExpr: "Id", displayExpr: "Name" };
  }

  async roles_onRowRemoved(e) {
    await this.generalService.sendRequest(e.data, '/admin/delete-group-join-role');
  }

  async permissions_onRowRemoved(e) {
    await this.generalService.sendRequest(e.data, '/admin/delete-group-join-permission');
  }

  async gvRoles_rowDblClick(e) {
    let result = this.dsSelectedRoles.find((r) => {
      return r.RoleId == e.data.Id;
    });

    if (!result) {
      const row_id = await this.generalService.sendRequest({ GroupId: this.Id, RoleId: e.data.Id }, '/admin/save-group-join-role');
      this.dsSelectedRoles.push({
        Id: row_id.ROWID,
        RoleId: e.data.Id,
        Code: e.data.Code,
        Name: e.data.Name
      });
    }

    this.rolePopup.instance.close();
  }

  async gvPermissions_rowDblClick(e) {
    let result = this.dsSelectedPermissions.find((r) => {
      return r.PermissionId == e.data.Id;
    });

    if (!result) {
      const row_id = await this.generalService.sendRequest({ GroupId: this.Id, PermissionId: e.data.Id }, '/admin/save-group-join-permission');
      this.dsSelectedPermissions.push({
        Id: row_id.ROWID,
        PermissionId: e.data.Id,
        Code: e.data.Code,
        Name: e.data.Name
      });
    }

    this.permissionPopup.instance.close();
  }

  permissions_displayValueFormatter(item) {
    item = item[0];
    return item && item.Code + ' - ' + item.Name;
  }

  roles_displayValueFormatter(item) {
    item = item[0];
    return item && item.Code + ' - ' + item.Name;
  }

  rolesCreateStore() {
    const _this = this;

    this.dsRoles.store = new CustomStore({
      load: async function (loadOptions: any) {
        let request = {};
        if (loadOptions.filter) {
          for (var i = 0; i < loadOptions.filter.length; i += 2) {
            if (typeof loadOptions.filter[i] == "string") {
              request[loadOptions.filter[i]] = loadOptions.filter[2];
              break;
            }
            else {
              request[loadOptions.filter[i][0]] = loadOptions.filter[i][2];
            }
          }
        }

        if (!Object.entries(request).length) return null;
        else return _this.generalService.sendRequest(request, '/admin/get-roles').then(r => {
          return {
            data: r
          };
        });
      },
      byKey: function () { return _this.dsRoles; }
    });
  }

  permissionsCreateStore() {
    const _this = this;

    this.dsPermissions.store = new CustomStore({
      load: async function (loadOptions: any) {
        let request = {};
        if (loadOptions.filter) {
          for (var i = 0; i < loadOptions.filter.length; i += 2) {
            if (typeof loadOptions.filter[i] == "string") {
              request[loadOptions.filter[i]] = loadOptions.filter[2];
              break;
            }
            else {
              request[loadOptions.filter[i][0]] = loadOptions.filter[i][2];
            }
          }
        }

        if (!Object.entries(request).length) return null;
        else return _this.generalService.sendRequest(request, '/admin/get-permissions').then(r => {
          return {
            data: r
          };
        });
      },
      byKey: function () { return _this.dsPermissions; }
    });
  }
}
