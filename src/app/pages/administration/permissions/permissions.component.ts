import { Component, OnInit } from '@angular/core';
import { GeneralService } from 'src/app/shared/services/global.service';


@Component({
  selector: 'permissions',
  templateUrl: './permissions.component.html'
})
export class PermissionsComponent implements OnInit {
  dataSource: any;

  constructor(private generalService: GeneralService) { }

  async ngOnInit() {
    this.dataSource = await this.generalService.sendRequest({}, '/admin/get-permissions');
  }

  onRowInserting(e) {
    let x = this.generalService.sendRequest(e.data, '/admin/save-permission');
    x.then(x => this.dataSource[this.dataSource.length - 1].Id = x.ROWID);
    e.cancel = false;
  }

  onRowUpdating(e) {
    let updatedRow = Object.assign(e.oldData, e.newData);
    this.generalService.sendRequest(updatedRow, '/admin/save-permission');
    e.cancel = false;
  }

  onRowRemoving(e) {
    this.generalService.sendRequest(e.data, '/admin/delete-permission');
    e.cancel = false;
  }
}
