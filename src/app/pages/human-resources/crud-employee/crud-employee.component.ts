import { Component, OnInit } from '@angular/core';

import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { GeneralService } from 'src/app/shared/services/global.service';

@Component({
  selector: 'crud-employee',
  templateUrl: './crud-employee.component.html',
  styleUrls: ['./crud-employee.component.scss']
})
export class CrudEmployeeComponent implements OnInit {
  Id = 0;
  user: any;
  profilePictureURL: any;
  allowedFileExtensions = [".jpg", ".jpeg", ".png", ".pdf", ".doc", ".docx"];

  // EMPLOYEE
  dsEmployee: any = {};

  // STATUS
  dsStatuses: any;
  statusEditorOptions = {};

  // GENDERS
  dsGenders: any;
  genderEditorOptions = {};

  // MARITAL STATUSES
  dsMaritalStatuses: any;
  maritalStatusEditorOptions = {};

  // GENDERS
  dsMilitaryStatuses: any;
  militaryStatusEditorOptions = {};

  // CITIZENSHIP LIST
  dsCitizenshipList: any;
  citizenshipListEditorOptions = {};

  // BIRTH COUNTRIES
  dsBirthCountries: any;
  birthCountryEditorOptions = {};

  // BIRTH CITIES
  dsBirthCities: any;
  birthCityEditorOptions = {};

  // DRIVING CATEGORY
  dsDrivingCategories: any;
  drivingCategoryEditorOptions = {};

  // EMPLOYMENT TYPES
  dsEmploymentTypes: any;
  employmentTypeEditorOptions = {};

  // BLOOD GROUPS
  dsBloodGroups: any;
  bloodGroupEditorOptions = {};

  // RELIGIONS
  dsReligions: any;
  religionEditorOptions = {};

  // BIRTH DATE
  birthDateEditorOptions = {};

  constructor(private generalService: GeneralService, private route: ActivatedRoute, private snackbar: MatSnackBar, private router: Router) {
    this.Id = Number(this.route.snapshot.params.id);
  }

  ngOnInit() {
    try {
      this.user = JSON.parse(localStorage.getItem('user'));
    }
    catch {
      this.router.navigate(['/signin']);
      return;
    }

    this.getDrivingCategories();
    this.getEmployee();
    this.getStatuses();
    this.getGenders();
    this.getMaritalStatuses();
    this.getMilitaryStatuses();
    this.getCitizenshipList();
    this.getBirthCountry();
    this.getEmploymentTypes();
    this.getBloodGroups();
    this.getReligions();

    this.birthDateEditorOptions = {
      min: ((new Date()).getFullYear() - 100) + "/1/1",
      displayFormat: "yyyy/MM/dd",
      max: ((new Date()).getFullYear() - 14) + "/12/31",
      dateOutOfRangeMessage: "Birth Date is out of range",
      showClearButton: true
    };
  }

  async approve() {
    this.dsEmployee.JobOfferId = this.Id;
    let controls = ['StatusId', 'StartDate', 'FirstName', 'MiddleName', 'LastName', 'GenderId', 'EmploymentTypeId', 'Experience'];

    let messages = {
      StatusId: 'Choose any status.',
      StartDate: 'Choose any start date.',
      FirstName: 'Fill in the first name.',
      MiddleName: 'Fill in the middle name.',
      LastName: 'Fill in the last name.',
      GenderId: 'Choose any gender.',
      EmploymentTypeId: 'Choose any employment type.',
      Experience: 'Fill in the experience.'
    };

    let resp = this.generalService.validateDataSource(this.dsEmployee, controls);
    if (resp === true) {
      await this.generalService.sendRequest(this.dsEmployee, '/human/save-employee');
      this.snackbar.open('Operation succeeded.', 'Ok', { duration: 3000 });
      this.router.navigate(['/human-resources/employees']);
    }
    else {
      this.snackbar.open('Operation failed.\n' + messages[resp], 'Ok', { duration: 3000 });
    }
  }

  async getEmployee() {
    this.dsEmployee = await this.generalService.sendRequest({ Id: this.Id }, '/human/get-employee');

    this.getBirthCity(this.dsEmployee['BirthCountryId'] ?? 0);
    this.getEmployeeJoinDrivingCategories();
  }

  async getBloodGroups() {
    this.dsBloodGroups = await this.generalService.sendRequest({}, '/human/get-blood-groups');
    this.bloodGroupEditorOptions = { dataSource: this.dsBloodGroups, valueExpr: "Id", displayExpr: "Name", showClearButton: true };
  }

  async getReligions() {
    this.dsReligions = await this.generalService.sendRequest({}, '/human/get-religions');
    this.religionEditorOptions = { dataSource: this.dsReligions, valueExpr: "Id", displayExpr: "Name", showClearButton: true };
  }

  async getStatuses() {
    this.dsStatuses = await this.generalService.sendRequest({ StatusTypeId: 75, UserId: -3 }, '/config/get-statuses');
    this.statusEditorOptions = { dataSource: this.dsStatuses, valueExpr: "Id", displayExpr: "Name" };
  }

  async getGenders() {
    this.dsGenders = await this.generalService.sendRequest({}, '/human/get-genders');
    this.genderEditorOptions = { dataSource: this.dsGenders, valueExpr: "Id", displayExpr: "Name" };
  }

  async getMaritalStatuses() {
    this.dsMaritalStatuses = await this.generalService.sendRequest({}, '/human/get-marital-statuses');
    this.maritalStatusEditorOptions = { dataSource: this.dsMaritalStatuses, valueExpr: "Id", displayExpr: "Name", showClearButton: true };
  }

  async getMilitaryStatuses() {
    this.dsMilitaryStatuses = await this.generalService.sendRequest({}, '/human/get-military-statuses');
    this.militaryStatusEditorOptions = { dataSource: this.dsMilitaryStatuses, valueExpr: "Id", displayExpr: "Name", showClearButton: true };
  }

  async getCitizenshipList() {
    this.dsCitizenshipList = await this.generalService.sendRequest({}, '/human/get-citizenship-list');
    this.citizenshipListEditorOptions = { dataSource: this.dsCitizenshipList, valueExpr: "Id", displayExpr: "Name", showClearButton: true };
  }

  async getBirthCountry() {
    this.dsBirthCountries = await this.generalService.sendRequest({}, '/config/get-countries');

    const that = this;
    this.birthCountryEditorOptions = {
      dataSource: this.dsBirthCountries, valueExpr: "Id", displayExpr: "Name", showClearButton: true,
      onValueChanged: async function (e) {
        if (e.value) that.getBirthCity(e.value);
        else that.dsEmployee.BirthCityId = null;
      }
    };
  }

  async getBirthCity(countryId) {
    this.dsBirthCities = await this.generalService.sendRequest({ CountryId: countryId }, '/config/get-cities');
    this.birthCityEditorOptions = { dataSource: this.dsBirthCities, valueExpr: "Id", displayExpr: "Name", showClearButton: true };
  }

  async getDrivingCategories() {
    this.dsEmployee['DrivingCategories'] = [];
    this.dsDrivingCategories = await this.generalService.sendRequest({}, '/human/get-driving-categories');
    this.drivingCategoryEditorOptions = { dataSource: this.dsDrivingCategories, valueExpr: "Id", displayExpr: "Name", hideSelectedItems: true, value: this.dsEmployee['DrivingCategories'] };
  }

  async getEmployeeJoinDrivingCategories() {
    this.dsEmployee['DrivingCategories'] = [];
    let result = await this.generalService.sendRequest({ EmployeeId: this.Id }, '/human/get-employee-join-driving-categories');
    result.forEach(element => { this.dsEmployee['DrivingCategories'].push(element.Id); });
  }

  async getEmploymentTypes() {
    this.dsEmploymentTypes = await this.generalService.sendRequest({}, '/human/get-employment-types');
    this.employmentTypeEditorOptions = { dataSource: this.dsEmploymentTypes, valueExpr: "Id", displayExpr: "Name" };
  }

  showFiles(files) {
    if (files.length) {
      if (files[0].size <= 2000000) {
        var reader = new FileReader();
        reader.readAsDataURL(files[0]);

        reader.onload = (_event) => {
          this.profilePictureURL = reader.result;
        }
      }
      else {
        this.snackbar.open('Operation failed. Maximum size: 2 MB', 'Ok', { duration: 3000 });
      }
    }
  }
}
