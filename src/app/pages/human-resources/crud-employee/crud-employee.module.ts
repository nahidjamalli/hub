import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageLayoutModule } from '../../../../@hubcore/components/page-layout/page-layout.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BreadcrumbsModule } from '../../../../@hubcore/components/breadcrumbs/breadcrumbs.module';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { CrudEmployeeRoutingModule } from './crud-employee-routing.module';
import { DxFormModule, DxDataGridModule, DxTagBoxModule, DxFileManagerModule, DxTextAreaModule } from 'devextreme-angular';

import { MatSnackBarModule } from '@angular/material/snack-bar';
import { CrudEmployeeComponent } from './crud-employee.component';
import { DxiItemModule } from 'devextreme-angular/ui/nested';
import { DxButtonModule } from 'devextreme-angular';
import { MatTabsModule } from '@angular/material/tabs';

@NgModule({
  declarations: [CrudEmployeeComponent],
  imports: [
    MatSnackBarModule,
    CommonModule,
    CrudEmployeeRoutingModule,
    PageLayoutModule,
    FlexLayoutModule,
    BreadcrumbsModule,
    ReactiveFormsModule,
    MatButtonToggleModule,
    DxFormModule,
    DxiItemModule,
    DxDataGridModule,
    DxTagBoxModule,
    DxButtonModule,
    MatTabsModule,
    DxFileManagerModule,
    DxTextAreaModule
  ],
  providers: []
})
export class CrudEmployeeModule { }
