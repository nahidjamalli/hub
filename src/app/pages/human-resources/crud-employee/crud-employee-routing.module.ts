import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { VexRoutes } from '../../../../@hubcore/interfaces/vex-route.interface';
import { CrudEmployeeComponent } from './crud-employee.component';

const routes: VexRoutes = [
  {
    path: '',
    component: CrudEmployeeComponent,
    data: {
      toolbarShadowEnabled: true
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CrudEmployeeRoutingModule { }
