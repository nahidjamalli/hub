import { Component, OnInit, ViewChild } from '@angular/core';

import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import CustomStore from 'devextreme/data/custom_store';
import { DxDropDownBoxComponent } from 'devextreme-angular';
import { GeneralService } from 'src/app/shared/services/global.service';

@Component({
  selector: 'crud-exam-candidate',
  templateUrl: './crud-exam-candidate.component.html',
  styleUrls: ['./crud-exam-candidate.component.scss']
})
export class CrudExamCandidateComponent implements OnInit {
  @ViewChild('employee') employeePopup: DxDropDownBoxComponent;
  @ViewChild('candidate') candidatePopup: DxDropDownBoxComponent;
  @ViewChild('exam') examPopup: DxDropDownBoxComponent;


  Id = 0;
  user = null;

  dataSource: any = {};
  dsCandidates: any = {};
  dsEmployees: any = {};
  dsExams: any = {};

  selectedCandidate: any = [];
  selectedEmployee: any = [];
  selectedExam: any = [];

  constructor(private generalService: GeneralService, private route: ActivatedRoute, private snackbar: MatSnackBar, private router: Router) {
    this.Id = Number(this.route.snapshot.params.id);
  }
  ngOnInit() {
    try {
      this.user = JSON.parse(localStorage.getItem('user'));
    }
    catch {
      this.router.navigate(['/signin']);
      return;
    }

    this.exams_createStore();
    this.employees_createStore();
    this.candidates_createStore();

    if (this.Id == 0) this.dataSource = {};
    else {
      this.getExamCandidate();
    }
  }

  async getExamCandidate() {
    this.dataSource = await this.generalService.sendRequest({ Id: this.Id }, '/training/get-exam-candidate');

    this.getEmployee();
    this.getCandidate();
    this.getExam();
  }

  async getExam() {
    this.selectedExam = [await this.generalService.sendRequest({ Id: this.dataSource['ExamId'] }, '/training/get-exam')];
  }

  async getEmployee() {
    this.selectedEmployee = [await this.generalService.sendRequest({ Id: this.dataSource['CandidateId'] }, '/human/get-employee')];
  }

  async getCandidate() {
    this.selectedCandidate = [await this.generalService.sendRequest({ Id: this.dataSource['CandidateId'] }, '/human/get-candidate')];
  }

  async approve() {
    this.dataSource['CandidateId'] = null;

    if (this.selectedCandidate && this.selectedCandidate[0]) {
      this.dataSource['CandidateId'] = this.selectedCandidate[0].Id;
      this.dataSource['IsEmployee'] = false;
    }
    else if (this.selectedEmployee && this.selectedEmployee[0]) {
      this.dataSource['CandidateId'] = this.selectedEmployee[0].Id;
      this.dataSource['IsEmployee'] = true;
    }

    if (this.selectedExam[0]) {
      this.dataSource['ExamId'] = this.selectedExam[0].Id;
    }

    let messages = {
      ExamId: 'Choose any exam.',
      CandidateId: 'Choose any candidate or employee.'
    };

    const controls = ['ExamId', 'CandidateId'];

    let resp = this.generalService.validateDataSource(this.dataSource, controls);
    if (resp === true) {
      let resp = await this.generalService.sendRequest(this.dataSource, '/training/save-exam-candidate');

      if (resp && resp.ROWID) {
        if (!this.Id) {
          this.Id = resp.ROWID;
          this.dataSource['Code'] = resp.Code;
        }

        this.snackbar.open('Operation succeeded.', 'Ok', { duration: 3000 });
      }
      else {
        this.snackbar.open('Operation succeeded.', 'Ok', { duration: 3000 });
        await this.router.navigate(['/human-resources/exam-candidates']);
      }
    } else {
      this.snackbar.open('Operation failed. ' + messages[resp], 'Ok', { duration: 3000 });
    }
  }

  candidates_displayValueFormatter(item) {
    item = item[0];
    return item && item.Code + " - " + item.Firstname + " " + item.Lastname;
  }

  gvCandidates_rowDblClick() {
    this.candidatePopup.instance.close();
  }

  employees_displayValueFormatter(item) {
    item = item[0];
    return item && item.Code + " - " + item.Firstname + " " + item.Lastname;
  }

  gvEmployees_rowDblClick() {
    this.employeePopup.instance.close();
  }

  exams_displayValueFormatter(item) {
    item = item[0];
    return item && item.Code + " - " + item.Name;
  }

  gvExams_rowDblClick() {
    this.examPopup.instance.close();
  }

  employees_createStore() {
    const _this = this;

    this.dsEmployees.store = new CustomStore({
      load: async function (loadOptions: any) {
        let request = {};
        if (loadOptions.filter) {
          for (var i = 0; i < loadOptions.filter.length; i += 2) {
            if (typeof loadOptions.filter[i] == "string") {
              request[loadOptions.filter[i]] = loadOptions.filter[2];
              break;
            }
            else {
              request[loadOptions.filter[i][0]] = loadOptions.filter[i][2];
            }
          }
        }

        if (!Object.entries(request).length) return null;
        else return _this.generalService.sendRequest(request, '/human/get-employees').then(r => {
          return {
            data: r
          };
        });
      },
      byKey: function () { return _this.dsEmployees; }
    });
  }

  candidates_createStore() {
    const _this = this;

    this.dsCandidates.store = new CustomStore({
      load: async function (loadOptions: any) {
        let request = {};
        if (loadOptions.filter) {
          for (var i = 0; i < loadOptions.filter.length; i += 2) {
            if (typeof loadOptions.filter[i] == "string") {
              request[loadOptions.filter[i]] = loadOptions.filter[2];
              break;
            }
            else {
              request[loadOptions.filter[i][0]] = loadOptions.filter[i][2];
            }
          }
        }

        if (!Object.entries(request).length) return null;
        else return _this.generalService.sendRequest(request, '/human/get-candidates').then(r => {
          return {
            data: r
          };
        });
      },
      byKey: function () { return _this.dsCandidates; }
    });
  }

  exams_createStore() {
    const _this = this;

    this.dsExams.store = new CustomStore({
      load: async function (loadOptions: any) {
        let request = {};
        if (loadOptions.filter) {
          for (var i = 0; i < loadOptions.filter.length; i += 2) {
            if (typeof loadOptions.filter[i] == "string") {
              request[loadOptions.filter[i]] = loadOptions.filter[2];
              break;
            }
            else {
              request[loadOptions.filter[i][0]] = loadOptions.filter[i][2];
            }
          }
        }

        if (!Object.entries(request).length) return null;
        else return _this.generalService.sendRequest(request, '/training/get-exams').then(r => {
          return {
            data: r
          };
        });
      },
      byKey: function () { return _this.dsExams; }
    });
  }
}
