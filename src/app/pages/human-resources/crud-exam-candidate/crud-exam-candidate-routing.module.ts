import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { VexRoutes } from '../../../../@hubcore/interfaces/vex-route.interface';
import { CrudExamCandidateComponent } from './crud-exam-candidate.component';

const routes: VexRoutes = [
  {
    path: '',
    component: CrudExamCandidateComponent,
    data: {
      toolbarShadowEnabled: true
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CrudExamCandidateRoutingModule { }
