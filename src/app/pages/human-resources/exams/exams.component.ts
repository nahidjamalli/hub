import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';
import { GeneralService } from 'src/app/shared/services/global.service';

@Component({
  selector: 'exams',
  templateUrl: './exams.component.html'
})
export class ExamsComponent implements OnInit {
  dataSource: any = {};
  user = null;

  constructor(private generalService: GeneralService, private router: Router) { }

  async ngOnInit() {
    try {
      this.user = JSON.parse(localStorage.getItem('user'));
    }
    catch {
      this.router.navigate(['/signin']);
      return;
    }

    this.dataSource = await this.generalService.sendRequest({}, '/training/get-exams');
  }

  onRowRemoving(e) {
    this.generalService.sendRequest(e.data, '/training/delete-exam');
    e.cancel = false;
  }

  onInitNewRow(e) {
    this.router.navigate(['/human-resources/crud-exam/0']);
    e.cancel = true;
  }

  onEditingStart(e) {
    this.router.navigate(['/human-resources/crud-exam/' + e.data.Id]);
    e.cancel = true;
  }
}
