import { Component, OnInit } from '@angular/core';
import { GeneralService } from 'src/app/shared/services/global.service';


@Component({
  selector: 'departments',
  templateUrl: './departments.component.html'
})
export class DepartmentsComponent implements OnInit {
  dataSource: any;
  structuresDataSource: any;

  constructor(private generalService: GeneralService) { }

  async ngOnInit() {
    this.dataSource = await this.generalService.sendRequest({}, '/human/get-departments');
    this.structuresDataSource = await this.generalService.sendRequest({}, '/human/get-structures');
  }

  onRowInserting(e) {
    e.cancel = false;
    let parentNode = e.component.getNodeByKey(e.data.ParentId);

    if (parentNode) {
      e.data.ParentId = parentNode.data ? parentNode.data.Id : 0;
    }

    let x = this.generalService.sendRequest(e.data, '/human/save-department');
    x.then(y => this.dataSource[this.dataSource.length - 1].Id = y.ROWID);
  }

  onRowUpdating(e) {
    let updatedRow = Object.assign(e.oldData, e.newData);
    this.generalService.sendRequest(updatedRow, '/human/save-department');
    e.cancel = false;
  }

  onRowRemoving(e) {
    this.generalService.sendRequest(e.data, '/human/delete-department');
    e.cancel = false;
  }
}
