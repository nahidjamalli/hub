import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';
import { GeneralService } from 'src/app/shared/services/global.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'probation-periods',
  templateUrl: './probation-periods.component.html'
})
export class ProbationPeriodsComponent implements OnInit {
  dataSource: any;
  user = null;

  constructor(private generalService: GeneralService, private router: Router) { }

  async ngOnInit() {
    try {
      this.user = JSON.parse(localStorage.getItem('user'));
    }
    catch {
      this.router.navigate(['/signin']);
      return;
    }

    this.dataSource = await this.generalService.sendRequest({ UserId: this.user.Id }, '/human/get-probation-periods');
  }

  onRowRemoving(e) {
    this.generalService.sendRequest(e.data, '/human/delete-probation-period');
    e.cancel = false;
  }

  onInitNewRow(e) {
    this.router.navigate(['/human-resources/crud-probation-period/0']);
    e.cancel = true;
  }

  onEditingStart(e) {
    this.router.navigate(['/human-resources/crud-probation-period/' + e.data.Id]);
    e.cancel = true;
  }
}
