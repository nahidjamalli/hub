import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageLayoutModule } from '../../../../@hubcore/components/page-layout/page-layout.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BreadcrumbsModule } from '../../../../@hubcore/components/breadcrumbs/breadcrumbs.module';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { ProbationPeriodsRoutingModule } from './probation-periods-routing.module';
// tslint:disable-next-line:max-line-length
import { DxDataGridModule } from 'devextreme-angular';

import { ProbationPeriodsComponent } from './probation-periods.component'

@NgModule({
  declarations: [ProbationPeriodsComponent],
  imports: [
    CommonModule,
    ProbationPeriodsRoutingModule,
    PageLayoutModule,
    FlexLayoutModule,
    BreadcrumbsModule,
    ReactiveFormsModule,
    MatButtonToggleModule,
    DxDataGridModule
  ],
  providers: []
})
export class ProbationPeriodsModule { }
