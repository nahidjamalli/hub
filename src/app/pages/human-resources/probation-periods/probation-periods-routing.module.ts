import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { VexRoutes } from '../../../../@hubcore/interfaces/vex-route.interface';
import { ProbationPeriodsComponent } from './probation-periods.component';

const routes: VexRoutes = [
  {
    path: '',
    component: ProbationPeriodsComponent,
    data: {
      toolbarShadowEnabled: true
    }
  }
];

@NgModule({
  exports: [RouterModule],
  imports: [RouterModule.forChild(routes)]
})
export class ProbationPeriodsRoutingModule {
}
