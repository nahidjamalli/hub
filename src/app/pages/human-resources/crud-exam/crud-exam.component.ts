import { Component, OnInit, ViewChild } from '@angular/core';

import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import CustomStore from 'devextreme/data/custom_store';
import { DxDropDownBoxComponent } from 'devextreme-angular';
import { GeneralService } from 'src/app/shared/services/global.service';

@Component({
  selector: 'crud-exam',
  templateUrl: './crud-exam.component.html',
  styleUrls: ['./crud-exam.component.scss']
})
export class CrudExamComponent implements OnInit {
  @ViewChild('question') questionPopup: DxDropDownBoxComponent;

  selectedItems: any[] = [];
  selectedQuestion: any = [];
  dsRules: [];
  dataSource: any;
  dsExamTypes: any;
  dsQuestions: any = {};
  dsSelectedQuestions: any = [];
  dsExamJoinRules: any = [];
  dsLanguages: any = [];

  examTypesEditorOptions = {};
  languageEditorOptions = {};
  durationEditorOptions = { value: 1, showSpinButtons: true, mode: 'number', min: 1, max: 300 };
  requiredScoreEditorOptions = { value: 0, showSpinButtons: true, mode: 'number', min: 0, max: 1000 };

  Id = 0;
  user = null;

  constructor(private generalService: GeneralService, private route: ActivatedRoute, private snackbar: MatSnackBar, private router: Router) {
    this.Id = Number(this.route.snapshot.params.id);
  }

  ngOnInit() {
    try {
      this.user = JSON.parse(localStorage.getItem('user'));
    }
    catch {
      this.router.navigate(['/signin']);
      return;
    }

    this.getRules();
    this.getExamTypes();
    this.createStore();
    this.getLanguages();

    if (this.Id == 0) {
      this.dataSource = {};
    }
    else {
      this.getExam();
      this.getExamJoinRules();
      this.getSelectedQuestions();
    }
  }

  async getLanguages() {
    this.dsLanguages = await this.generalService.sendRequest({}, '/config/get-languages');
    this.languageEditorOptions = { dataSource: this.dsLanguages, valueExpr: "Id", displayExpr: "Name" };
  }

  async getSelectedQuestions() {
    this.dsSelectedQuestions = await this.generalService.sendRequest({ ExamId: this.Id }, '/training/get-exam-join-questions');
  }

  async getExam() {
    this.dataSource = await this.generalService.sendRequest({ Id: this.Id }, '/training/get-exam');
  }

  async getExamTypes() {
    this.dsExamTypes = await this.generalService.sendRequest({}, '/training/get-exam-types');
    this.examTypesEditorOptions = { dataSource: this.dsExamTypes, valueExpr: "Id", displayExpr: "Name" };
  }

  async getExamJoinRules() {
    this.dsExamJoinRules = await this.generalService.sendRequest({ Id: this.Id }, '/training/get-exam-join-rules');
    this.dsExamJoinRules.forEach(element => { this.selectedItems.push(element.RuleId); });
  }

  async getRules() {
    this.dsRules = await this.generalService.sendRequest({}, '/training/get-rules');
  }

  async approve() {
    let messages = {
      ExamTypeId: 'Choose any type.',
      Name: 'Fill in the name.',
      Duration: 'Fill in the duration.',
      RequiredScore: 'Fill in the score.'
    };

    const controls = ['Name', 'ExamTypeId', 'Duration', 'RequiredScore'];

    let resp = this.generalService.validateDataSource(this.dataSource, controls);
    if (resp === true) {
      let resp = await this.generalService.sendRequest(this.dataSource, '/training/save-exam');

      if (resp && resp.ROWID) {
        if (!this.Id) {
          this.Id = resp.ROWID;
          this.dataSource['Code'] = resp.Code;
        }

        this.snackbar.open('Operation succeeded.', 'Ok', { duration: 3000 });
      }
      else {
        this.snackbar.open('Operation succeeded.', 'Ok', { duration: 3000 });
        await this.router.navigate(['/human-resources/exams']);
      }
    } else {
      this.snackbar.open('Operation failed. ' + messages[resp], 'Ok', { duration: 3000 });
    }
  }

  async onSelectionChanged(e) {
    if (e.addedItems.length == 1) {
      await this.generalService.sendRequest({ RuleId: e.addedItems[0].Id, ExamId: this.Id }, '/training/save-exam-join-rule');
      this.snackbar.open('Operation succeeded.', 'Ok', { duration: 3000 });
    }
    else if (e.removedItems.length == 1) {
      await this.generalService.sendRequest({ RuleId: e.removedItems[0].Id, ExamId: this.Id }, '/training/delete-exam-join-rule');
      this.snackbar.open('Operation succeeded.', 'Ok', { duration: 3000 });
    }
  }

  createStore() {
    const _this = this;

    this.dsQuestions.store = new CustomStore({
      load: async function (loadOptions: any) {

        let request = {};
        if (loadOptions.filter) {
          for (var i = 0; i < loadOptions.filter.length; i += 2) {
            if (typeof loadOptions.filter[i] == "string") {
              request[loadOptions.filter[i]] = loadOptions.filter[2];
              break;
            }
            else {
              request[loadOptions.filter[i][0]] = loadOptions.filter[i][2];
            }
          }
        }

        if (!Object.entries(request).length) return null;
        else return _this.generalService.sendRequest(request, '/training/get-questions').then(r => {
          return {
            data: r
          };
        });
      },
      byKey: function () { return _this.dsQuestions; }
    });
  }

  questions_displayValueFormatter(item) {
    item = item[0];
    return item && item.Code + " - " + item.Name;
  }

  async onRowRemoved(e) {
    await this.generalService.sendRequest(e.data, '/training/delete-exam-join-question');
  }

  async onRowUpdated(e) {
    await this.generalService.sendRequest(e.data, '/training/update-exam-join-question');
  }

  async gvQuestions_rowDblClick(e) {
    let result = this.dsSelectedQuestions.find((r) => {
      return r.QuestionId == e.data.Id;
    });

    if (!result) {
      let resp = await this.generalService.sendRequest({ QuestionId: e.data.Id, ExamId: this.Id }, '/training/save-exam-join-question');
      this.dsSelectedQuestions.push({
        Id: resp.ROWID,
        QuestionId: e.data.Id,
        QuestionType: e.data.QuestionType,
        QuestionGroup: e.data.QuestionGroup,
        Code: e.data.Code,
        Name: e.data.Name,
        Duration: e.data.Duration,
        Score: e.data.Score,
        Order: 0
      });
    }

    this.questionPopup.instance.close();
  }
}
