import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageLayoutModule } from '../../../../@hubcore/components/page-layout/page-layout.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BreadcrumbsModule } from '../../../../@hubcore/components/breadcrumbs/breadcrumbs.module';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { CrudExamRoutingModule } from './crud-exam-routing.module';
import { DxFormModule, DxDataGridModule, DxTagBoxModule, DxValidatorModule, DxListModule, DxDropDownBoxModule, DxTextAreaModule } from 'devextreme-angular';

import { MatSnackBarModule } from '@angular/material/snack-bar';
import { CrudExamComponent } from './crud-exam.component';
import { DxiItemModule } from 'devextreme-angular/ui/nested';
import { DxButtonModule } from 'devextreme-angular';
import { MatTabsModule } from '@angular/material/tabs';
import { MatExpansionModule } from '@angular/material/expansion';

@NgModule({
  declarations: [CrudExamComponent],
  imports: [
    MatSnackBarModule,
    CommonModule,
    CrudExamRoutingModule,
    PageLayoutModule,
    FlexLayoutModule,
    BreadcrumbsModule,
    ReactiveFormsModule,
    MatButtonToggleModule,
    DxFormModule,
    DxiItemModule,
    DxDataGridModule,
    DxTagBoxModule,
    DxButtonModule,
    DxValidatorModule,
    MatTabsModule,
    MatExpansionModule,
    DxListModule,
    DxDropDownBoxModule,
    DxTextAreaModule
  ],
  providers: []
})
export class CrudExamModule { }
