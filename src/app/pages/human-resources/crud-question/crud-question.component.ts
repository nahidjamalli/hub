import { Component, OnInit } from '@angular/core';

import { ActivatedRoute, Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { GeneralService } from 'src/app/shared/services/global.service';

@Component({
  selector: 'crud-question',
  templateUrl: './crud-question.component.html',
  styleUrls: ['./crud-question.component.scss']
})
export class CrudQuestionComponent implements OnInit {
  questionTypeEditorOptions = {};
  questionGroupEditorOptions = {};
  durationEditorOptions = { value: 1, showSpinButtons: true, mode: 'number', min: 1, max: 60 };
  scoreEditorOptions = { value: 0, showSpinButtons: true, mode: 'number', min: 0, max: 1000 };

  selectedAnswerId: number = 0;
  dsAnswers: any = {};
  dsQuestionContents: any = {};
  dsAnswerContents: any = {};
  dataSource: any = {};
  dsQuestionTypes: any;
  dsQuestionGroups: any;

  Id = 0;
  user = null;
  constructor(private generalService: GeneralService, private route: ActivatedRoute, private snackbar: MatSnackBar, private router: Router) {
    this.Id = Number(this.route.snapshot.params.id);
  }

  ngOnInit() {
    try {
      this.user = JSON.parse(localStorage.getItem('user'));
    }
    catch {
      this.router.navigate(['/signin']);
      return;
    }

    this.getQuestionGroups();
    this.getQuestionTypes();
    this.getQuestionJoinAnswers();

    if (this.Id == 0) this.dataSource = {};
    else {
      this.getQuestion();
      this.getQuestionJoinContents();
    }
  }

  async getQuestionJoinAnswers() {
    this.dsAnswers = await this.generalService.sendRequest({ QuestionId: this.Id }, '/training/get-question-join-answers');
  }

  async getQuestionJoinContents() {
    this.dsQuestionContents = await this.generalService.sendRequest({ QuestionId: this.Id }, '/training/get-question-join-contents');
  }

  async getQuestion() {
    this.dataSource = await this.generalService.sendRequest({ Id: this.Id }, '/training/get-question');
  }

  async getAnswerJoinContents(answerId: number) {
    this.dsAnswerContents = await this.generalService.sendRequest({ AnswerId: answerId }, '/training/get-answer-join-contents');
  }

  async approve() {
    let messages = {
      Name: 'Fill in the name.',
      QuestionTypeId: 'Choose any type.',
      QuestionGroupId: 'Choose any group.',
      Duration: 'Fill in the duration.',
      Score: 'Fill in the score.'
    };

    const controls = ['Name', 'QuestionTypeId', 'QuestionGroupId', 'Duration', 'Score'];

    let resp = this.generalService.validateDataSource(this.dataSource, controls);
    if (resp === true) {
      let resp = await this.generalService.sendRequest(this.dataSource, '/training/save-question');

      if (resp && resp.ROWID) {
        if (!this.Id) {
          this.Id = resp.ROWID;
          this.dataSource['Code'] = resp.Code;
        }
        this.snackbar.open('Operation succeeded.', 'Ok', { duration: 3000 });
      }
      else {
        this.snackbar.open('Operation succeeded.', 'Ok', { duration: 3000 });
        await this.router.navigate(['/human-resources/questions']);
      }
    } else {
      this.snackbar.open('Operation failed. ' + messages[resp], 'Ok', { duration: 3000 });
    }
  }

  async getQuestionTypes() {
    this.dsQuestionTypes = await this.generalService.sendRequest({}, '/training/get-question-types');
    this.questionTypeEditorOptions = { dataSource: this.dsQuestionTypes, valueExpr: "Id", displayExpr: "Name" };
  }

  async getQuestionGroups() {
    this.dsQuestionGroups = await this.generalService.sendRequest({}, '/training/get-question-groups');
    this.questionGroupEditorOptions = { dataSource: this.dsQuestionGroups, valueExpr: "Id", displayExpr: "Name" };
  }

  contents_onRowInserting(e) {
    e.data['QuestionId'] = this.Id;
    let x = this.generalService.sendRequest(e.data, '/training/save-content');
    x.then(x => this.dsQuestionContents[this.dsQuestionContents.length - 1].Id = x.ROWID);
    e.cancel = false;
  }

  answerContents_onRowInserting(e) {
    e.data['AnswerId'] = this.selectedAnswerId;
    let x = this.generalService.sendRequest(e.data, '/training/save-content');
    x.then(x => this.dsAnswerContents[this.dsAnswerContents.length - 1].Id = x.ROWID);
    e.cancel = false;
  }

  contents_onRowUpdating(e) {
    let updatedRow = Object.assign(e.oldData, e.newData);
    this.generalService.sendRequest(updatedRow, '/training/save-content');
    e.cancel = false;
  }

  contents_onRowRemoving(e) {
    this.generalService.sendRequest(e.data, '/training/delete-content');
    e.cancel = false;
  }

  answers_onRowInserting(e) {
    e.data['QuestionId'] = this.Id;
    let x = this.generalService.sendRequest(e.data, '/training/save-answer');
    x.then(x => this.dsAnswers[this.dsAnswers.length - 1].Id = x.ROWID);
    e.cancel = false;
  }

  answers_onRowUpdating(e) {
    let updatedRow = Object.assign(e.oldData, e.newData);
    this.generalService.sendRequest(updatedRow, '/training/save-answer');
    e.cancel = false;
  }

  answers_onRowRemoving(e) {
    this.generalService.sendRequest(e.data, '/training/delete-answer');
    e.cancel = false;
  }

  answers_onFocusedRowChanged(e) {
    const rowData = e.row && e.row.data;

    if (rowData) {
      this.selectedAnswerId = rowData.Id;
      this.getAnswerJoinContents(rowData.Id);
    }
  }
}
