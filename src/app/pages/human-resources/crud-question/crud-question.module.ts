import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageLayoutModule } from '../../../../@hubcore/components/page-layout/page-layout.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BreadcrumbsModule } from '../../../../@hubcore/components/breadcrumbs/breadcrumbs.module';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { CrudQuestionRoutingModule } from './crud-question-routing.module';
import { DxFormModule, DxDataGridModule, DxValidatorModule, DxTextAreaModule } from 'devextreme-angular';

import { MatSnackBarModule } from '@angular/material/snack-bar';
import { CrudQuestionComponent } from './crud-question.component';
import { DxiItemModule } from 'devextreme-angular/ui/nested';
import { DxButtonModule } from 'devextreme-angular';
import { MatTabsModule } from '@angular/material/tabs';

@NgModule({
  declarations: [CrudQuestionComponent],
  imports: [
    MatSnackBarModule,
    CommonModule,
    CrudQuestionRoutingModule,
    PageLayoutModule,
    FlexLayoutModule,
    BreadcrumbsModule,
    ReactiveFormsModule,
    MatButtonToggleModule,
    DxFormModule,
    DxiItemModule,
    DxDataGridModule,
    DxButtonModule,
    DxValidatorModule,
    MatTabsModule,
    DxTextAreaModule
  ],
  providers: []
})
export class CrudQuestionModule { }
