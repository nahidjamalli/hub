import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';
import { GeneralService } from 'src/app/shared/services/global.service';

@Component({
  selector: 'job-offers',
  templateUrl: './job-offers.component.html'
})
export class JobOffersComponent implements OnInit {
  dataSource: any;
  user = null;

  constructor(private generalService: GeneralService, private router: Router) { }

  async ngOnInit() {
    try {
      this.user = JSON.parse(localStorage.getItem('user'));
    }
    catch {
      this.router.navigate(['/signin']);
      return;
    }
    this.dataSource = await this.generalService.sendRequest({ UserId: this.user.Id }, '/human/get-job-offers');
  }

  onRowRemoving(e) {
    this.generalService.sendRequest(e.data, '/human/delete-job-offer');
    e.cancel = false;
  }

  onInitNewRow(e) {
    this.router.navigate(['/human-resources/crud-job-offer/0']);
    e.cancel = true;
  }

  onEditingStart(e) {
    this.router.navigate(['/human-resources/crud-job-offer/' + e.data.Id]);
    e.cancel = true;
  }
}
