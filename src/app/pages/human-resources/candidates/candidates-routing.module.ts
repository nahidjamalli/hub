import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { VexRoutes } from '../../../../@hubcore/interfaces/vex-route.interface';
import { CandidatesComponent } from './candidates.component';

const routes: VexRoutes = [
  {
    path: '',
    component: CandidatesComponent,
    data: {
      toolbarShadowEnabled: true
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CandidatesRoutingModule { }
