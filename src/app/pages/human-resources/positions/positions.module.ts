import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageLayoutModule } from '../../../../@hubcore/components/page-layout/page-layout.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BreadcrumbsModule } from '../../../../@hubcore/components/breadcrumbs/breadcrumbs.module';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { PositionsRoutingModule } from './positions-routing.module';
import { DxTreeListModule, DxLookupModule } from 'devextreme-angular';

import { PositionsComponent } from './positions.component';

@NgModule({
  declarations: [PositionsComponent],
  imports: [
    CommonModule,
    PositionsRoutingModule,
    PageLayoutModule,
    FlexLayoutModule,
    BreadcrumbsModule,
    ReactiveFormsModule,
    MatButtonToggleModule,
    DxTreeListModule,
    DxLookupModule
  ],
  providers: []
})
export class PositionsModule { }
