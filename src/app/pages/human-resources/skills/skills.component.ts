import { Component, OnInit } from '@angular/core';
import { GeneralService } from 'src/app/shared/services/global.service';


@Component({
  selector: 'skills',
  templateUrl: './skills.component.html'
})
export class SkillsComponent implements OnInit {
  dataSource: any;
  skillGroupDataSource: any;

  constructor(private generalService: GeneralService) { }

  async ngOnInit() {
    this.dataSource = await this.generalService.sendRequest({}, '/human/get-skills');
    this.skillGroupDataSource = await this.generalService.sendRequest({}, '/human/get-skill-types');
  }

  onRowInserting(e) {
    let x = this.generalService.sendRequest(e.data, '/human/save-skill');
    x.then(x => this.dataSource[this.dataSource.length - 1].Id = x.ROWID);
    e.cancel = false;
  }

  onRowUpdating(e) {
    let updatedRow = Object.assign(e.oldData, e.newData);
    this.generalService.sendRequest(updatedRow, '/human/save-skill');
    e.cancel = false;
  }

  onRowRemoving(e) {
    this.generalService.sendRequest(e.data, '/human/delete-skill');
    e.cancel = false;
  }
}
