import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageLayoutModule } from '../../../../@hubcore/components/page-layout/page-layout.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BreadcrumbsModule } from '../../../../@hubcore/components/breadcrumbs/breadcrumbs.module';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { StructuresRoutingModule } from './structures-routing.module';
import { DxDataGridModule } from 'devextreme-angular';

import { StructuresComponent } from './structures.component';

@NgModule({
  declarations: [StructuresComponent],
  imports: [
    CommonModule,
    StructuresRoutingModule,
    PageLayoutModule,
    FlexLayoutModule,
    BreadcrumbsModule,
    ReactiveFormsModule,
    MatButtonToggleModule,
    DxDataGridModule
  ],
  providers: []
})
export class StructuresModule { }
