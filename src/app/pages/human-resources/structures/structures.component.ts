import { Component, OnInit } from '@angular/core';
import { GeneralService } from 'src/app/shared/services/global.service';


@Component({
  selector: 'structures',
  templateUrl: './structures.component.html'
})
export class StructuresComponent implements OnInit {
  dataSource: any;

  constructor(private generalService: GeneralService) { }

  async ngOnInit() {
    this.dataSource = await this.generalService.sendRequest({}, '/human/get-structures');
  }

  onRowInserting(e) {
    let x = this.generalService.sendRequest(e.data, '/human/save-structure');
    x.then(x => this.dataSource[this.dataSource.length - 1].Id = x.ROWID);
    e.cancel = false;
  }

  onRowUpdating(e) {
    let updatedRow = Object.assign(e.oldData, e.newData);
    this.generalService.sendRequest(updatedRow, '/human/save-structure');
    e.cancel = false;
  }

  onRowRemoving(e) {
    this.generalService.sendRequest(e.data, '/human/delete-structure');
    e.cancel = false;
  }
}
