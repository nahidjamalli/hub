import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EducationDegreesComponent } from './education-degrees.component';


const routes: Routes = [
  {
    path: '',
    component: EducationDegreesComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EducationDegreesRoutingModule { }
