import { Component, OnInit } from '@angular/core';
import { GeneralService } from 'src/app/shared/services/global.service';


@Component({
  selector: 'reasons',
  templateUrl: './reasons.component.html'
})
export class ReasonsComponent implements OnInit {
  dataSource: any;

  constructor(private generalService: GeneralService) { }

  async ngOnInit() {
    this.dataSource = await this.generalService.sendRequest({}, '/human/get-reasons');
  }

  onRowInserting(e) {
    let x = this.generalService.sendRequest(e.data, '/human/save-reason');
    x.then(x => this.dataSource[this.dataSource.length - 1].Id = x.ROWID);
    e.cancel = false;
  }

  onRowUpdating(e) {
    let updatedRow = Object.assign(e.oldData, e.newData);
    this.generalService.sendRequest(updatedRow, '/human/save-reason');
    e.cancel = false;
  }

  onRowRemoving(e) {
    this.generalService.sendRequest(e.data, '/human/delete-reason');
    e.cancel = false;
  }
}
