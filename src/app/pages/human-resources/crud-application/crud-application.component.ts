import { Component, OnInit, ViewChild } from '@angular/core';

import { ActivatedRoute, Router } from '@angular/router';
import CustomStore from 'devextreme/data/custom_store';
import { DxDropDownBoxComponent } from 'devextreme-angular';
import { MatSnackBar } from '@angular/material/snack-bar';
import { AppointmentHistory } from './crud-application.model';
import { GeneralService } from 'src/app/shared/services/global.service';

@Component({
  selector: 'crud-application',
  templateUrl: './crud-application.component.html',
  styleUrls: ['./crud-application.component.scss']
})
export class CrudApplicationComponent implements OnInit {
  @ViewChild('employeeDemand') employeeDemandPopup: DxDropDownBoxComponent;
  @ViewChild('candidate') candidatePopup: DxDropDownBoxComponent;
  @ViewChild('moderator') moderatorPopup: DxDropDownBoxComponent;
  @ViewChild('approvalEmployee') approvalEmployeePopup: DxDropDownBoxComponent;

  Id = 0;
  user = null;

  statusEditorOptions: any;
  selectedCandidate: any = [];
  selectedEmployeeDemand: any = [];
  dataSource: any = {};
  IsReadOnly = false;
  ExpectedSalaryEditorOptions = {};
  NoteReadOnlyEditorOptions = { readOnly: false, height: 100 };

  // INTERVIEW TYPES
  dsInterviewTypes: any;

  // DECISIONS
  dsDecisions: any;

  // EMPLOYEE DEMANDS
  dsEmployeeDemands: any = {};

  // CANDIDATES
  dsCandidates: any = {};

  // STATUSES
  dsStatuses: any;

  // EMPLOYEES
  dsEmployees: any = {};

  // APPROVAL EMPLOYEES
  dsApprovalEmployees = [];

  // MODERATORS
  dsModerators = [];

  // APPOINTMENTS
  dsAppointments = [];

  dsInterviews: AppointmentHistory[] = [];

  constructor(private generalService: GeneralService, private route: ActivatedRoute, private snackbar: MatSnackBar, private router: Router) {
    this.Id = Number(this.route.snapshot.params.id);
  }

  ngOnInit() {
    try {
      this.user = JSON.parse(localStorage.getItem('user'));
    }
    catch {
      this.router.navigate(['/signin']);
      return;
    }

    if (this.Id == 0) {
      this.dataSource = {};
      this.dataSource.StatusId = 300;
      this.dataSource['ApprovalEmployees'] = [];
      this.dataSource['Moderators'] = [];
    }
    else {
      this.getApplication();
      this.getDecisions();
      this.getInterviewTypes();
      this.getInterviews();
      this.getAppointments();
      this.getStatuses();
    }

    this.employees_createStore();
    this.employeeDemands_createStore();
    this.candidates_createStore();
  }

  async getApplication() {
    this.dataSource = await this.generalService.sendRequest({ Id: this.Id }, '/human/get-application');

    if (this.Id != 0 && this.dataSource == null) {
      await this.router.navigate(['/*']);
      return;
    }

    let resp = await this.generalService.sendRequest({ ApplicationId: this.Id, UserId: this.user.Id }, '/human/is-readonly-application');

    this.IsReadOnly = Boolean(resp.RESPONSE);

    if (this.IsReadOnly)
      this.dataSource['ExpectedSalary'] = 0;

    this.NoteReadOnlyEditorOptions = { readOnly: this.IsReadOnly, height: 100 };
    this.ExpectedSalaryEditorOptions = { readOnly: this.IsReadOnly, format: '#0 AZN' };
    this.statusEditorOptions = { dataSource: this.dsStatuses, valueExpr: "Id", displayExpr: "Name", readOnly: this.IsReadOnly };

    this.selectedEmployeeDemand = [await this.generalService.sendRequest({ Id: this.dataSource.EmployeeDemandId }, '/human/get-employee-demand')];
    this.selectedCandidate = [await this.generalService.sendRequest({ Id: this.dataSource.CandidateId }, '/human/get-candidate')];

    this.getModerators();
    this.getApprovalEmployees();
  }

  async getInterviews() {
    this.dsInterviews = await this.generalService.sendRequest({ ApplicationId: this.Id }, '/human/get-application-join-interviews');
  }

  async getAppointments() {
    this.dsAppointments = await this.generalService.sendRequest({ UserId: this.user.Id, ApplicationId: this.Id }, '/human/get-application-join-appointments');
  }

  async getDecisions() {
    this.dsDecisions = await this.generalService.sendRequest({ StatusTypeId: 70, UserId: 0 }, '/config/get-statuses');
  }

  async getInterviewTypes() {
    this.dsInterviewTypes = await this.generalService.sendRequest({}, '/human/get-interview-types');
  }

  async getModerators() {
    this.dataSource['Moderators'] = [];

    this.dsModerators = await this.generalService.sendRequest({ ApplicationId: this.Id, IsModerator: true }, '/human/get-application-join-approval-employees');
    this.dsModerators.forEach(element => { this.dataSource['Moderators'].push(element.Id); });
  }

  async getApprovalEmployees() {
    this.dataSource['ApprovalEmployees'] = [];

    this.dsApprovalEmployees = await this.generalService.sendRequest({ ApplicationId: this.Id, IsModerator: false }, '/human/get-application-join-approval-employees');
    this.dsApprovalEmployees.forEach(element => { this.dataSource['ApprovalEmployees'].push(element.Id); });
  }

  async getStatuses() {
    this.dsStatuses = await this.generalService.sendRequest({ StatusTypeId: 65, UserId: -2 }, '/config/get-statuses');
    this.statusEditorOptions = { dataSource: this.dsStatuses, valueExpr: "Id", displayExpr: "Name" };
  }

  async approve() {
    if (!this.dataSource['Moderators'].includes(this.user.Id)) {
      this.dataSource['Moderators'].push(this.user.Id);
    }

    this.dataSource['EmployeeDemandId'] = this.selectedEmployeeDemand[0].Id;
    if (this.selectedCandidate[0]) this.dataSource['CandidateId'] = this.selectedCandidate[0].Id;
    this.dataSource['CreatorUserId'] = this.user.Id;

    let controls = ['EmployeeDemandId', 'CandidateId', 'ExpectedSalary', 'StatusId', 'ApprovalEmployees'];

    let messages = {
      EmployeeDemandId: 'Choose any employee demand.',
      CandidateId: 'Choose any candidate.',
      ExpectedSalary: 'Fill in the expected salary.',
      StatusId: 'Choose any status.',
      ApprovalEmployees: 'Choose any approval employee(s).'
    };

    let resp = this.generalService.validateDataSource(this.dataSource, controls);
    if (resp === true) {
      await this.generalService.sendRequest(this.dataSource, '/human/save-application');
      this.snackbar.open('Operation succeeded.', 'Ok', { duration: 3000 });
      this.router.navigate(['/human-resources/applications']);
    }
    else {
      this.snackbar.open('Operation failed.\n' + messages[resp], 'Ok', { duration: 3000 });
    }
  }

  onRowInserting(e) {
    let request = e.data;
    request['CreatorUserId'] = this.user.Id;
    request['ApplicationId'] = this.Id;
    request['Id'] = 0;
    let resp = this.generalService.sendRequest(request, '/human/save-appointment');
    resp.then(x => this.dsAppointments[this.dsAppointments.length - 1].Id = x.ROWID);
  }

  onRowUpdating(e) {
    let updatedRow = Object.assign(e.oldData, e.newData);
    this.generalService.sendRequest(updatedRow, '/human/save-appointment');
  }

  onRowRemoved(e) {
    this.generalService.sendRequest(e.data, '/human/delete-appointment');
  }

  employeeDemands_displayValueFormatter(item) {
    item = item[0];
    return item && item.Code + " - " + item.Department + " - " + item.Position;
  }

  candidates_displayValueFormatter(item) {
    item = item[0];
    return item && item.Firstname + " " + item.Lastname;
  }

  employeeDemands_createStore() {
    const _this = this;

    this.dsEmployeeDemands.store = new CustomStore({
      load: async function (loadOptions: any) {
        let request = { UserId: _this.user.Id };
        if (loadOptions.filter) {
          for (var i = 0; i < loadOptions.filter.length; i += 2) {
            if (typeof loadOptions.filter[i] == "string") {
              request[loadOptions.filter[i]] = loadOptions.filter[2];
              break;
            }
            else {
              request[loadOptions.filter[i][0]] = loadOptions.filter[i][2];
            }
          }
        }

        if (Object.entries(request).length < 2) return null;
        else return _this.generalService.sendRequest(request, '/human/get-employee-demands').then(r => {
          return {
            data: r
          };
        });
      },
      byKey: function () { return _this.dsEmployeeDemands; }
    });
  }

  candidates_createStore() {
    const _this = this;

    this.dsCandidates.store = new CustomStore({
      load: async function (loadOptions: any) {
        let request = {};
        if (loadOptions.filter) {
          for (var i = 0; i < loadOptions.filter.length; i += 2) {
            if (typeof loadOptions.filter[i] == "string") {
              request[loadOptions.filter[i]] = loadOptions.filter[2];
              break;
            }
            else {
              request[loadOptions.filter[i][0]] = loadOptions.filter[i][2];
            }
          }
        }

        if (!Object.entries(request).length) return null;
        else return _this.generalService.sendRequest(request, '/human/get-candidates').then(r => {
          return {
            data: r
          };
        });
      },
      byKey: function () { return _this.dsCandidates; }
    });
  }

  employees_createStore() {
    const _this = this;

    this.dsEmployees.store = new CustomStore({
      load: async function (loadOptions: any) {
        let request = { UserId: _this.user.Id };
        if (loadOptions.filter) {
          for (var i = 0; i < loadOptions.filter.length; i += 2) {
            if (typeof loadOptions.filter[i] == "string") {
              request[loadOptions.filter[i]] = loadOptions.filter[2];
              break;
            }
            else {
              request[loadOptions.filter[i][0]] = loadOptions.filter[i][2];
            }
          }
        }

        if (Object.entries(request).length < 2) return null;
        else return _this.generalService.sendRequest(request, '/human/get-employees-join-users').then(r => {
          return {
            data: r
          };
        });
      },
      byKey: function () { return _this.dsEmployees; }
    });
  }

  gvEmployeeDemands_rowDblClick() {
    this.employeeDemandPopup.instance.close();
  }

  gvCandidates_rowDblClick() {
    this.candidatePopup.instance.close();
  }

  moderators_OnValueChanged(e) {
    if (this.dataSource['Moderators'] != [])
      this.dataSource['Moderators'] = e.value;
  }

  approvalEmployees_OnValueChanged(e) {
    if (this.dataSource['ApprovalEmployees'] != [])
      this.dataSource['ApprovalEmployees'] = e.value;
  }

  gvModerators_rowDblClick(e) {
    this.moderatorPopup.instance.close();

    if (!this.dataSource['Moderators']) {
      this.dsModerators.push({ Id: e.data.UserId, FullName: e.data.Firstname + ' ' + e.data.Middlename + ' ' + e.data.Lastname });
      this.dataSource['Moderators'] = [e.data.UserId];
    }

    let result = this.dsModerators.find((r) => r.Id == e.data.UserId);

    if (!result) {
      this.dsModerators.push({ Id: e.data.UserId, FullName: e.data.Firstname + ' ' + e.data.Middlename + ' ' + e.data.Lastname });
    }

    result = this.dataSource['Moderators'].find((r: number) => r == e.data.UserId);

    if (!result) {
      this.dataSource['Moderators'].push(e.data.UserId);
    }
  }

  gvApprovalEmployees_rowDblClick(e) {
    this.approvalEmployeePopup.instance.close();

    if (!this.dataSource['ApprovalEmployees']) {
      this.dsApprovalEmployees.push({ Id: e.data.UserId, FullName: e.data.Firstname + ' ' + e.data.Middlename + ' ' + e.data.Lastname });
      this.dataSource['ApprovalEmployees'] = [e.data.UserId];
    }

    let result = this.dsApprovalEmployees.find((r) => r.Id == e.data.UserId);

    if (!result) {
      this.dsApprovalEmployees.push({ Id: e.data.UserId, FullName: e.data.Firstname + ' ' + e.data.Middlename + ' ' + e.data.Lastname });
    }

    result = this.dataSource['ApprovalEmployees'].find((r: number) => r == e.data.UserId);

    if (!result) {
      this.dataSource['ApprovalEmployees'].push(e.data.UserId);
    }
  }
}
