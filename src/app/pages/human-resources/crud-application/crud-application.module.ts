import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageLayoutModule } from '../../../../@hubcore/components/page-layout/page-layout.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BreadcrumbsModule } from '../../../../@hubcore/components/breadcrumbs/breadcrumbs.module';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { CrudApplicationRoutingModule } from './crud-application-routing.module';
import {
  DxFormModule, DxDataGridModule, DxTagBoxModule, DxSelectBoxModule,
  DxValidatorModule, DxDropDownBoxModule, DxPopupModule, DxAccordionModule, DxTextAreaModule
} from 'devextreme-angular';

import { MatSnackBarModule } from '@angular/material/snack-bar';
import { CrudApplicationComponent } from './crud-application.component';
import { DxiItemModule } from 'devextreme-angular/ui/nested';
import { DxButtonModule } from 'devextreme-angular';
import { MatTabsModule } from '@angular/material/tabs';
import { MatExpansionModule } from '@angular/material/expansion';

@NgModule({
  declarations: [CrudApplicationComponent],
  imports: [
    MatSnackBarModule,
    CommonModule,
    CrudApplicationRoutingModule,
    PageLayoutModule,
    FlexLayoutModule,
    BreadcrumbsModule,
    ReactiveFormsModule,
    MatButtonToggleModule,
    DxFormModule,
    DxiItemModule,
    DxDataGridModule,
    DxTagBoxModule,
    DxSelectBoxModule,
    DxButtonModule,
    DxValidatorModule,
    DxDropDownBoxModule,
    DxPopupModule,
    DxAccordionModule,
    MatTabsModule,
    MatExpansionModule,
    DxTextAreaModule
  ],
  providers: []
})
export class CrudApplicationModule { }
