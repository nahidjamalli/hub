export class AppointmentHistory {
    fullName: string;
    details: Details[];
}

class Details {
    name: string;
    location: string;
    interviewType: string;
    decision: string;
    fromDateTime: string;
    toDateTime: string;
    note: string;
}
