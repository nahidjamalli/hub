import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { VexRoutes } from '../../../../@hubcore/interfaces/vex-route.interface';
import { EmployeesComponent } from './employees.component';

const routes: VexRoutes = [
  {
    path: '',
    component: EmployeesComponent,
    data: {
      toolbarShadowEnabled: true
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmployeesRoutingModule {
}
