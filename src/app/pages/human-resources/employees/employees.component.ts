import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';
import { GeneralService } from 'src/app/shared/services/global.service';

@Component({
  selector: 'employees',
  templateUrl: './employees.component.html'
})
export class EmployeesComponent implements OnInit {
  dataSource: any;

  constructor(private generalService: GeneralService, private router: Router) { }

  async ngOnInit() {
    this.dataSource = await this.generalService.sendRequest({}, '/human/get-employees');
  }

  onRowRemoving(e) {
    this.generalService.sendRequest(e.data, '/admin/delete-employee');
    e.cancel = false;
  }

  onInitNewRow(e) {
    this.router.navigate(['/human-resources/crud-employee/0']);
    e.cancel = true;
  }

  onEditingStart(e) {
    this.router.navigate(['/human-resources/crud-employee/' + e.data.Id]);
    e.cancel = true;
  }
}
