import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';
import { GeneralService } from 'src/app/shared/services/global.service';

@Component({
  selector: 'rules',
  templateUrl: './rules.component.html'
})
export class RulesComponent implements OnInit {
  dataSource: any;
  user = null;

  constructor(private generalService: GeneralService, private router: Router) { }

  async ngOnInit() {
    try {
      this.user = JSON.parse(localStorage.getItem('user'));
    }
    catch {
      this.router.navigate(['/signin']);
      return;
    }
    this.dataSource = await this.generalService.sendRequest({ UserId: this.user.Id }, '/training/get-rules');
  }

  onRowInserting(e) {
    let x = this.generalService.sendRequest(e.data, '/training/save-rule');
    x.then(x => this.dataSource[this.dataSource.length - 1].Id = x.ROWID);
    e.cancel = false;
  }

  onRowUpdating(e) {
    let updatedRow = Object.assign(e.oldData, e.newData);
    this.generalService.sendRequest(updatedRow, '/training/save-rule');
    e.cancel = false;
  }

  onRowRemoving(e) {
    this.generalService.sendRequest(e.data, '/training/delete-rule');
    e.cancel = false;
  }
}
