import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { GeneralService } from 'src/app/shared/services/global.service';

@Component({
  selector: 'questions',
  templateUrl: './questions.component.html'
})
export class QuestionsComponent implements OnInit {
  dataSource: any = {};
  user = null;

  constructor(private generalService: GeneralService, private router: Router, public translate: TranslateService) {
    translate.addLangs(['en', 'az']);
    translate.use(localStorage.getItem('lang'));
  }

  async ngOnInit() {
    try {
      this.user = JSON.parse(localStorage.getItem('user'));
    }
    catch {
      this.router.navigate(['/signin']);
      return;
    }

    this.dataSource = await this.generalService.sendRequest({ UserId: this.user.Id }, '/training/get-questions');
  }

  onRowRemoving(e) {
    this.generalService.sendRequest(e.data, '/training/delete-question');
    e.cancel = false;
  }

  onInitNewRow(e) {
    this.router.navigate(['/human-resources/crud-question/0']);
    e.cancel = true;
  }

  onEditingStart(e) {
    this.router.navigate(['/human-resources/crud-question/' + e.data.Id]);
    e.cancel = true;
  }
}
