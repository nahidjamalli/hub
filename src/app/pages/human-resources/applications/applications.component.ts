import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GeneralService } from 'src/app/shared/services/global.service';

@Component({
  selector: 'applications',
  templateUrl: './applications.component.html'
})
export class ApplicationsComponent implements OnInit {
  dataSource: any;
  user = null;

  constructor(private generalService: GeneralService, private router: Router) { }

  async ngOnInit() {
    try {
      this.user = JSON.parse(localStorage.getItem('user'));
    }
    catch {
      this.router.navigate(['/signin']);
      return;
    }
    this.dataSource = await this.generalService.sendRequest({ UserId: this.user.Id }, '/human/get-applications');
  }

  onRowRemoving(e) {
    this.generalService.sendRequest(e.data, '/human/delete-application');
    e.cancel = false;
  }

  onInitNewRow(e) {
    this.router.navigate(['/human-resources/crud-application/0']);
    e.cancel = true;
  }

  onEditingStart(e) {
    this.router.navigate(['/human-resources/crud-application/' + e.data.Id]);
    e.cancel = true;
  }
}
