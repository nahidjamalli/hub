import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { VexRoutes } from '../../../../@hubcore/interfaces/vex-route.interface';
import { CrudProbationPeriodComponent } from './crud-probation-period.component';

const routes: VexRoutes = [
  {
    path: '',
    component: CrudProbationPeriodComponent,
    data: {
      toolbarShadowEnabled: true
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CrudProbationPeriodRoutingModule {
}
