import { NgModule } from '@angular/core';
import { CommonModule, DatePipe } from '@angular/common';
import { PageLayoutModule } from '../../../../@hubcore/components/page-layout/page-layout.module';
import { BreadcrumbsModule } from '../../../../@hubcore/components/breadcrumbs/breadcrumbs.module';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { CrudProbationPeriodRoutingModule } from './crud-probation-period-routing.module';
import {
  DxFormModule, DxDataGridModule, DxDropDownBoxModule, DxTextAreaModule
} from 'devextreme-angular';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { CrudProbationPeriodComponent } from './crud-probation-period.component';
import { DxiItemModule, DxiValidationRuleModule } from 'devextreme-angular/ui/nested';
import { DxButtonModule } from 'devextreme-angular';
import { MatTabsModule } from '@angular/material/tabs';
import { FlexLayoutModule } from '@angular/flex-layout';

@NgModule({
  declarations: [CrudProbationPeriodComponent],
  imports: [
    CrudProbationPeriodRoutingModule,
    MatSnackBarModule,
    CommonModule,
    PageLayoutModule,
    BreadcrumbsModule,
    ReactiveFormsModule,
    MatButtonToggleModule,
    DxFormModule,
    DxiItemModule,
    DxDataGridModule,
    DxButtonModule,
    DxDropDownBoxModule,
    MatTabsModule,
    FlexLayoutModule,
    DxiValidationRuleModule,
    DxTextAreaModule
  ],
  providers: [DatePipe]
})
export class CrudProbationPeriodModule { }
