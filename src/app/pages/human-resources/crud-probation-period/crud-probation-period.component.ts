import { Component, OnInit, ViewChild } from '@angular/core';

import { ActivatedRoute, Router } from '@angular/router';
import CustomStore from 'devextreme/data/custom_store';
import { DxDropDownBoxComponent } from 'devextreme-angular';
import { MatSnackBar } from '@angular/material/snack-bar';
import { GeneralService } from 'src/app/shared/services/global.service';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'crud-probation-period',
  templateUrl: './crud-probation-period.component.html',
  styleUrls: ['./crud-probation-period.component.scss']
})
export class CrudProbationPeriodComponent implements OnInit {
  @ViewChild('approvalEmployee') approvalEmployeeDemandPopup: DxDropDownBoxComponent;
  @ViewChild('employee') employeePopup: DxDropDownBoxComponent;

  Id = 0;
  user = null;
  now = new Date();
  IsReadOnly = false;
  ReadOnlyEditorOptions = {};

  dsProbationPeriod: any = {};

  // EMPLOYEES
  dsApprovalEmployees: any = {};
  selectedApprovalEmployee: any = [];

  // CANDIDATES
  dsEmployees: any = {};
  selectedEmployee: any = [];

  // SKILLS
  dsSkills: any = {};
  skillName: string;
  skillNote: string;
  skillEvaluation: number;

  // STATUSES
  dsStatuses: any = {};
  statusEditorOptions: any;

  // DATES
  dateEditorOptions = {};

  // tslint:disable-next-line:max-line-length
  constructor(private generalService: GeneralService, private route: ActivatedRoute, private snackbar: MatSnackBar, private router: Router) {
    this.Id = Number(this.route.snapshot.params.id);
  }

  ngOnInit() {
    try {
      this.user = JSON.parse(localStorage.getItem('user'));
    } catch {
      this.router.navigate(['/signin']);
      return;
    }

    this.getStatuses();
    this.getSkills();

    this.approvalEmployees_createStore();
    this.employees_createStore();

    this.getProbationPeriod();
  }

  async getEmployee() {
    // tslint:disable-next-line:max-line-length
    this.selectedEmployee = [await this.generalService.sendRequest({ Id: this.dsProbationPeriod.EmployeeId }, '/human/get-employee')];
  }

  async getApprovalEmployee() {
    // tslint:disable-next-line:max-line-length
    this.selectedApprovalEmployee = [await this.generalService.sendRequest({ UserId: this.dsProbationPeriod.ApprovalEmployeeId }, '/admin/get-user-join-employee')];
  }

  async getProbationPeriod() {
    // tslint:disable-next-line:triple-equals
    if (this.Id == 0) {
      this.dsProbationPeriod = { StatusId: 300 };
      // tslint:disable-next-line:max-line-length
      this.dateEditorOptions = { openOnFieldClick: true, type: "date", displayFormat: 'dd-MMM-yyyy', value: this.now, dateSerializationFormat: 'yyyy-MM-dd' };
    } else {
      this.dsProbationPeriod = await this.generalService.sendRequest({ Id: this.Id }, '/human/get-probation-period');
      this.getEmployee();
      this.getApprovalEmployee();

      let iro = await this.generalService.sendRequest({ ProbationPeriodId: this.Id, UserId: this.user.Id }, '/human/is-readonly-probation-period');

      if (Boolean(iro.RESPONSE)) {
        this.IsReadOnly = true;
        this.ReadOnlyEditorOptions = { readOnly: this.IsReadOnly };
      }

      this.dateEditorOptions = { openOnFieldClick: true, readOnly: this.IsReadOnly, type: "date", displayFormat: 'dd-MMM-yyyy', dateSerializationFormat: 'yyyy-MM-dd' };
    }

    this.ReadOnlyEditorOptions = { readOnly: this.IsReadOnly, height: 100 };
  }

  async getStatuses() {
    this.dsStatuses = await this.generalService.sendRequest({ StatusTypeId: 65, UserId: -2 }, '/config/get-statuses');
    this.statusEditorOptions = { dataSource: this.dsStatuses, valueExpr: 'Id', displayExpr: 'Name' };
  }

  async getSkills() {
    // tslint:disable-next-line:max-line-length
    this.dsSkills = await this.generalService.sendRequest({ SkillTypeCode: 200, ProbationPeriodId: this.Id }, '/human/get-probation-period-join-skills');
  }

  async approve() {
    if (this.selectedApprovalEmployee[0])
      this.dsProbationPeriod.ApprovalEmployeeId = this.selectedApprovalEmployee[0].UserId;

    if (this.selectedEmployee[0])
      this.dsProbationPeriod.EmployeeId = this.selectedEmployee[0].Id;

    this.dsProbationPeriod.CreatorUserId = this.user.Id;

    let messages = {
      StatusId: 'Choose any status.',
      EmployeeId: 'Choose any employee.',
      ApprovalEmployeeId: 'Choose any approval employee.',
      StartDate: 'Choose any start date.',
      EndDate: 'Choose any end date.'
    };

    const controls = ['StatusId', 'EmployeeId', 'ApprovalEmployeeId', 'StartDate', 'EndDate'];

    let resp = this.generalService.validateDataSource(this.dsProbationPeriod, controls);
    if (resp === true) {
      await this.generalService.sendRequest(this.dsProbationPeriod, '/human/save-probation-period');
      await this.router.navigate(['/human-resources/probation-periods']);
      this.snackbar.open('Operation succeeded.', 'Ok', { duration: 3000 });
    } else {
      this.snackbar.open('Operation failed. ' + messages[resp], 'Ok', { duration: 3000 });
    }
  }

  onRowUpdating(e) {
    const updatedRow = Object.assign(e.oldData, e.newData);
    this.generalService.sendRequest({
      SkillId: updatedRow.Id,
      ProbationPeriodId: this.Id,
      Evaluation: updatedRow.Evaluation
    }, '/human/save-probation-period-join-skills');
    e.cancel = false;
  }

  gvApprovalEmployees_rowDblClick() {
    this.approvalEmployeeDemandPopup.instance.close();
  }

  gvEmployees_rowDblClick() {
    this.employeePopup.instance.close();
  }

  approvalEmployees_displayValueFormatter(item) {
    item = item[0];
    return item && item.Firstname + ' ' + item.Lastname;
  }

  employees_displayValueFormatter(item) {
    item = item[0];
    return item && item.Firstname + ' ' + item.Lastname;
  }

  approvalEmployees_createStore() {
    const _this = this;

    this.dsApprovalEmployees.store = new CustomStore({
      async load(loadOptions: any) {
        const request = { UserId: _this.user.Id };
        if (loadOptions.filter) {
          for (let i = 0; i < loadOptions.filter.length; i += 2) {
            // tslint:disable-next-line:triple-equals
            if (typeof loadOptions.filter[i] == 'string') {
              request[loadOptions.filter[i]] = loadOptions.filter[2];
              break;
            } else {
              request[loadOptions.filter[i][0]] = loadOptions.filter[i][2];
            }
          }
        }

        if (Object.entries(request).length < 2) return null;
        else return _this.generalService.sendRequest(request, '/human/get-employees-join-users').then(r => {
          return {
            data: r
          };
        });
      },
      byKey() { return _this.dsApprovalEmployees; }
    });
  }

  employees_createStore() {
    const _this = this;

    this.dsEmployees.store = new CustomStore({
      async load(loadOptions: any) {
        const request = {};
        if (loadOptions.filter) {
          for (let i = 0; i < loadOptions.filter.length; i += 2) {
            // tslint:disable-next-line:triple-equals
            if (typeof loadOptions.filter[i] == 'string') {
              request[loadOptions.filter[i]] = loadOptions.filter[2];
              break;
            } else {
              request[loadOptions.filter[i][0]] = loadOptions.filter[i][2];
            }
          }
        }

        if (!Object.entries(request).length) return null;
        else return _this.generalService.sendRequest(request, '/human/get-employees').then(r => {
          return {
            data: r
          };
        });
      },
      byKey() { return _this.dsEmployees; }
    });
  }

  onFocusedRowChanged(e) {
    const rowData = e.row && e.row.data;

    if (rowData) {
      this.skillName = rowData.Name;
      this.skillNote = rowData.Note;
      this.skillEvaluation = rowData.Evaluation;
    }
  }

  validateEvaluation(e) {
    return e.value >= 0 && e.value <= 4;
  }
}
