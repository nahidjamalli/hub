import { Component, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import CustomStore from 'devextreme/data/custom_store';
import { DxTreeViewComponent, DxDropDownBoxComponent } from 'devextreme-angular';
import DataSource from 'devextreme/data/data_source';
import ArrayStore from 'devextreme/data/array_store';
import { MatSnackBar } from '@angular/material/snack-bar';
import { DecisionHistory } from './crud-employee-demand.model';
import RemoteFileSystemProvider from 'devextreme/file_management/remote_provider';
import { GeneralService } from 'src/app/shared/services/global.service';
import { BaseService } from 'src/app/shared/services/base.service';

@Component({
  selector: 'crud-employee-demand',
  templateUrl: './crud-employee-demand.component.html',
  styleUrls: ['./crud-employee-demand.component.scss']
})
export class CrudEmployeeDemandComponent implements OnInit {
  @ViewChild(DxTreeViewComponent, { static: false }) treeView;
  @ViewChild('department') departmentPopup: DxDropDownBoxComponent;
  @ViewChild('position') positionPopup: DxDropDownBoxComponent;
  @ViewChild('moderator') moderatorPopup: DxDropDownBoxComponent;
  @ViewChild('approvalEmployee') approvalEmployeePopup: DxDropDownBoxComponent;

  allowedFileExtensions = [".jpg", ".jpeg", ".png", ".pdf", ".doc", ".docx"];
  value: any[] = [];
  dataSource = {};
  remoteProvider: RemoteFileSystemProvider;
  popupVisible = false;
  docPopupVisible = false;

  // DEPARTMENTS
  dsGrouppedDepartments: any;

  // POSITIONS
  dsGrouppedPositions: any;

  // CONTRACTS
  dsContractTypes: any;

  // GENDERS
  dsGenders: any;

  // EDUCATION DEGREES
  dsEducationDegrees: any;

  // REQUIRED LANGUAGES
  dsRequiredLanguages = [];

  // LANGUAGES
  dsLanguages: any;

  // SKILLS
  dsSkills: any;

  // STATUSES
  dsStatuses: any;

  // REASONS
  dsReasons: any;

  // EMPLOYEES
  dsEmployees: any = {};

  // APPROVAL EMPLOYEES
  dsApprovalEmployees = [];

  // MODERATORS
  dsModerators = [];

  // STRUCTURES 
  dsStructures: any;

  // HISTORY
  dsHistory: DecisionHistory[];

  Id = 0;
  user = null;

  IsReadOnly = false;
  StatusIsReadOnly = false;
  ReadOnlyEditorOptions = {};
  NoteReadOnlyEditorOptions = { readOnly: false, height: 150 };
  AgeReadOnlyEditorOptions = { value: 18, showSpinButtons: true, mode: 'number', min: 18, max: 88, readOnly: false };
  ExperienceReadOnlyEditorOptions = { value: 0, showSpinButtons: true, mode: 'number', min: 0, max: 33, readOnly: false };
  EmployeeCountReadOnlyEditorOptions = { value: 1, showSpinButtons: true, mode: 'number', min: 1, max: 333, readOnly: false };
  EDNoteReadOnlyEditorOptions = { height: 150 }; // should be always editable

  constructor(private generalService: GeneralService, private route: ActivatedRoute, private snackbar: MatSnackBar, private router: Router, baseService: BaseService) {
    this.Id = Number(this.route.snapshot.params.id);

    this.remoteProvider = new RemoteFileSystemProvider({
      endpointUrl: baseService.API_EndPoint + "/document/file-manager-employee-demand?employeeDemandId=" + this.Id,

    });
  }

  ngOnInit() {
    try {
      this.user = JSON.parse(localStorage.getItem('user'));
    }
    catch {
      this.router.navigate(['/signin']);
      return;
    }

    if (this.Id != 0) {
      this.getEmployeeDemand();
      this.getHistory();
      this.isReadOnly();
    }
    else {
      this.dataSource = { EmployeeCount: 1, MinAge: 18, MaxAge: 25, MinExperience: 0, MaxExperience: 1, StatusId: 100 };
      this.dataSource['Moderators'] = [];
      this.dataSource['ApprovalEmployees'] = [];
    }

    this.getLanguages();
    this.createStore();
    this.getReasons();
    this.getGenders();
    this.getStructures();
    this.getContractTypes();
    this.getSkills();
    this.getEducationDegrees();
  }

  async isReadOnly() {
    let resp = await this.generalService.sendRequest({ EmployeeDemandId: this.Id, UserId: this.user.Id }, '/human/is-readonly-employee-demand');

    this.IsReadOnly = Boolean(resp.RESPONSE);

    this.NoteReadOnlyEditorOptions = { readOnly: this.IsReadOnly, height: 150 };
    this.ReadOnlyEditorOptions = { readOnly: this.IsReadOnly };
    this.AgeReadOnlyEditorOptions = { value: 18, showSpinButtons: true, mode: 'number', min: 18, max: 88, readOnly: this.IsReadOnly };
    this.ExperienceReadOnlyEditorOptions = { value: 0, showSpinButtons: true, mode: 'number', min: 0, max: 33, readOnly: this.IsReadOnly };
    this.EmployeeCountReadOnlyEditorOptions = { value: 1, showSpinButtons: true, mode: 'number', min: 1, max: 333, readOnly: this.IsReadOnly };
  }

  async getEmployeeDemand() {
    this.dataSource = await this.generalService.sendRequest({ Id: this.Id, UserId: this.user.Id }, '/human/get-employee-demand');

    if (this.dataSource == null) {
      await this.router.navigate(['/*']);
      return;
    }

    this.markAsReadNotification();
    this.getEducationDegrees();
    this.getModerators();
    this.getApprovalEmployees();
    this.getEmployeeDemandJoinEducationDegrees();
    this.getEmployeeDemandJoinSkills();
  }

  async markAsReadNotification() {
    this.generalService.sendRequest({ UserId: this.user.Id, RefId: this.Id, NotificationTypeId: 100 }, '/admin/mark-as-read-notification');
    this.generalService.sendRequest({ UserId: this.user.Id, RefId: this.Id, NotificationTypeId: 200 }, '/admin/mark-as-read-notification');
  }

  async getLanguages() {
    this.dsLanguages = await this.generalService.sendRequest({}, '/config/get-languages');
    this.dsRequiredLanguages = await this.generalService.sendRequest({ EmployeeDemandId: this.Id }, '/human/get-employee-demand-join-languages');
  }

  async getReasons() {
    this.dsReasons = await this.generalService.sendRequest({}, '/human/get-reasons');
  }

  async getGenders() {
    this.dsGenders = await this.generalService.sendRequest({}, '/human/get-genders');
  }

  async getStructures() {
    this.dsStructures = await this.generalService.sendRequest({}, '/human/get-structures');
  }

  async getContractTypes() {
    this.dsContractTypes = await this.generalService.sendRequest({}, '/contract/get-contract-types');
  }

  async getStatuses() {
    const result = this.dataSource['Moderators'].some(element => element == this.user.Id);

    this.dsStatuses = await this.generalService.sendRequest({ StatusTypeId: 65, UserId: (result ? -1 : 0) }, '/config/get-statuses');
  }

  async getModerators() {
    this.dataSource['Moderators'] = [];
    this.dsModerators = await this.generalService.sendRequest({ EmployeeDemandId: this.Id, IsModerator: true }, '/human/get-employee-demand-join-approval-employees');
    this.dsModerators.forEach(element => { this.dataSource['Moderators'].push(element.Id); });
    this.getStatuses();
  }

  async getApprovalEmployees() {
    this.dataSource['ApprovalEmployees'] = [];
    this.dsApprovalEmployees = await this.generalService.sendRequest({ EmployeeDemandId: this.Id, IsModerator: false }, '/human/get-employee-demand-join-approval-employees');
    this.dsApprovalEmployees.forEach(element => { this.dataSource['ApprovalEmployees'].push(element.Id); });
  }

  async getSkills() {
    this.dataSource['Skills'] = [];
    this.dsSkills = await this.generalService.sendRequest({}, '/human/get-skills');

    this.dsSkills = new DataSource({
      store: new ArrayStore({
        data: this.dsSkills,
        key: "Id",
      }),
      group: "SkillType"
    });
  }

  async getEmployeeDemandJoinSkills() {
    this.dataSource['Skills'] = [];
    let result = await this.generalService.sendRequest({ EmployeeDemandId: this.Id }, '/human/get-employee-demand-join-skills');
    result.forEach(element => { this.dataSource['Skills'].push(element.Id); });
  }

  async getEducationDegrees() {
    this.dataSource['EducationDegrees'] = [];
    this.dsEducationDegrees = await this.generalService.sendRequest({}, '/human/get-education-degrees');
  }

  async getEmployeeDemandJoinEducationDegrees() {
    this.dataSource['EducationDegrees'] = [];
    let result = await this.generalService.sendRequest({ EmployeeDemandId: this.Id }, '/human/get-employee-demand-join-education-degrees');
    result.forEach(element => { this.dataSource['EducationDegrees'].push(element.Id); });
  }

  async getHistory() {
    this.dsHistory = await this.generalService.sendRequest({ EmployeeDemandId: this.Id }, '/human/get-employee-demand-join-approval-employees-history');
  }

  async approve() {
    this.dataSource['Languages'] = this.dsRequiredLanguages;
    this.dataSource['Id'] = this.Id;
    this.dataSource['CreatorUserId'] = this.user.Id;

    if (!this.dataSource['Moderators'].includes(this.user.Id)) {
      this.dataSource['Moderators'].push(this.user.Id);
    }

    let controls = ['Name', 'StructureId', 'DepartmentId', 'PositionId', 'ContractTypeId', 'ResponsibilityNote', 'GenderId', 'ReasonId', 'ReasonNote', 'ApprovalEmployees', 'Note'];

    let messages = {
      Name: 'Fill in the name.',
      StructureId: 'Choose any structure.',
      DepartmentId: 'Choose any department.',
      PositionId: 'Choose any position.',
      ContractTypeId: 'Choose any contract type.',
      ResponsibilityNote: 'Fill in the responsibility note.',
      GenderId: 'Choose any gender.',
      ReasonId: 'Choose any reason.',
      ReasonNote: 'Fill in the reason note.',
      ApprovalEmployees: 'Choose any approval employee(s).',
      Note: 'Fill in the note.'
    };

    let resp = this.generalService.validateDataSource(this.dataSource, controls);
    if (resp === true) {
      await this.generalService.sendRequest(this.dataSource, '/human/save-employee-demand');
      this.snackbar.open('Operation succeeded.', 'Ok', { duration: 3000 });
      this.router.navigate(['/human-resources/employee-demands']);
    }
    else {
      this.snackbar.open('Operation failed.\n' + messages[resp], 'Ok', { duration: 3000 });
    }
  }

  syncDepartmentSelection() {
    if (!this.treeView) return;

    if (!this.dataSource['DepartmentId']) {
      this.treeView.instance.unselectAll();
    } else {
      this.treeView.instance.selectItem(this.dataSource['DepartmentId']);
    }
  }

  getSelectedDepartments(items) {
    const that = this;

    items.forEach(function (item) {
      if (item.selected) {
        that.dataSource['DepartmentId'] = item.key;
        that.dataSource['Department'] = item.text;
      }
      else if (item.items.length) {
        that.getSelectedDepartments(item.items);
      }
    });
  }

  departmentItemSelectionChanged(e) {
    const nodes = e.component.getNodes();
    this.getSelectedDepartments(nodes);
    this.departmentPopup.instance.close();
  }

  syncPositionSelection() {
    if (!this.treeView) return;

    if (!this.dataSource['Position']) {
      this.treeView.instance.unselectAll();
    } else {
      this.treeView.instance.selectItem(this.dataSource['Position']);
    }
  }

  getSelectedPositions(items) {
    const that = this;

    items.forEach(function (item) {
      if (item.selected) {
        that.dataSource['Position'] = item.text;
        that.dataSource['PositionId'] = item.key;
        if (!that.dataSource['Name'])
          that.dataSource['Name'] = item.text;
      }
      else if (item.items.length) {
        that.getSelectedPositions(item.items);
      }
    });
  }

  positionItemSelectionChanged(e) {
    const nodes = e.component.getNodes();
    this.getSelectedPositions(nodes);
    this.positionPopup.instance.close();
  }

  async onStructureValueChanged(e) {
    const dsDp = await this.generalService.sendRequest({ StructureId: e.value }, '/human/get-departments');
    const dsPs = await this.generalService.sendRequest({ StructureId: e.value }, '/human/get-positions');

    if (!dsDp.length) {
      this.dsGrouppedDepartments = [];
      this.dataSource['DepartmentId'] = null;
    }
    else {
      if (e.previousValue) {
        this.dataSource['DepartmentId'] = null;
      }
      this.dsGrouppedDepartments = new CustomStore({
        loadMode: "raw",
        key: "Id",
        load: function () { return dsDp; }
      });
    }

    if (!dsPs.length) {
      this.dsGrouppedPositions = [];
      this.dataSource['PositionId'] = null;
    }
    else {
      if (e.previousValue) {
        this.dataSource['PositionId'] = null;
      }
      this.dsGrouppedPositions = new CustomStore({
        loadMode: "raw",
        key: "Id",
        load: function () { return dsPs; }
      });
    }
  }

  gvModerators_rowDblClick(e) {
    this.moderatorPopup.instance.close();

    if (!this.dataSource['Moderators']) {
      this.dsModerators.push({ Id: e.data.UserId, FullName: e.data.Firstname + ' ' + e.data.Middlename + ' ' + e.data.Lastname });
      this.dataSource['Moderators'] = [e.data.UserId];
    }

    let result = this.dsModerators.find((r) => r.Id == e.data.UserId);

    if (!result) {
      this.dsModerators.push({ Id: e.data.UserId, FullName: e.data.Firstname + ' ' + e.data.Middlename + ' ' + e.data.Lastname });
    }

    result = this.dataSource['Moderators'].find((r: number) => r == e.data.UserId);

    if (!result) {
      this.dataSource['Moderators'].push(e.data.UserId);
    }
  }

  gvApprovalEmployees_rowDblClick(e) {
    this.approvalEmployeePopup.instance.close();

    if (!this.dataSource['ApprovalEmployees']) {
      this.dsApprovalEmployees.push({ Id: e.data.UserId, FullName: e.data.Firstname + ' ' + e.data.Middlename + ' ' + e.data.Lastname });
      this.dataSource['ApprovalEmployees'] = [e.data.UserId];
    }

    let result = this.dsApprovalEmployees.find((r) => r.Id == e.data.UserId);

    if (!result) {
      this.dsApprovalEmployees.push({ Id: e.data.UserId, FullName: e.data.Firstname + ' ' + e.data.Middlename + ' ' + e.data.Lastname });
    }

    result = this.dataSource['ApprovalEmployees'].find((r: number) => r == e.data.UserId);

    if (!result) {
      this.dataSource['ApprovalEmployees'].push(e.data.UserId);
    }
  }

  createStore() {
    const _this = this;

    this.dsEmployees.store = new CustomStore({
      load: async function (loadOptions: any) {

        let request = { UserId: _this.user.Id };
        if (loadOptions.filter) {
          for (var i = 0; i < loadOptions.filter.length; i += 2) {
            if (typeof loadOptions.filter[i] == "string") {
              request[loadOptions.filter[i]] = loadOptions.filter[2];
              break;
            }
            else {
              request[loadOptions.filter[i][0]] = loadOptions.filter[i][2];
            }
          }
        }

        if (Object.entries(request).length < 2) return null;
        else return _this.generalService.sendRequest(request, '/human/get-employees-join-users').then(r => {
          return {
            data: r
          };
        });
      },
      byKey: function () { return _this.dsEmployees; }
    });
  }

  Moderators_OnValueChanged(e) {
    if (this.dataSource['Moderators'] != [])
      this.dataSource['Moderators'] = e.value;
  }

  ApprovalEmployees_OnValueChanged(e) {
    if (this.dataSource['ApprovalEmployees'] != [])
      this.dataSource['ApprovalEmployees'] = e.value;
  }
}
