import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageLayoutModule } from '../../../../@hubcore/components/page-layout/page-layout.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BreadcrumbsModule } from '../../../../@hubcore/components/breadcrumbs/breadcrumbs.module';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { CrudEmployeeDemandRoutingModule } from './crud-employee-demand-routing.module';
import {
  DxLookupModule, DxFormModule, DxDataGridModule, DxFileUploaderModule, DxTagBoxModule, DxSelectBoxModule, DxValidatorModule,
  DxDropDownBoxModule, DxTreeViewModule, DxPopupModule, DxAccordionModule, DxScrollViewModule, DxFileManagerModule, DxTextAreaModule
} from 'devextreme-angular';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { CrudEmployeeDemandComponent } from './crud-employee-demand.component';
import { DxiItemModule } from 'devextreme-angular/ui/nested';
import { DxButtonModule } from 'devextreme-angular';

@NgModule({
  declarations: [CrudEmployeeDemandComponent],
  imports: [
    MatSnackBarModule,
    CommonModule,
    CrudEmployeeDemandRoutingModule,
    PageLayoutModule,
    FlexLayoutModule,
    BreadcrumbsModule,
    ReactiveFormsModule,
    MatButtonToggleModule,
    DxLookupModule,
    DxFormModule,
    DxiItemModule,
    DxDataGridModule,
    DxFileUploaderModule,
    DxTagBoxModule,
    DxSelectBoxModule,
    DxButtonModule,
    DxValidatorModule,
    DxDropDownBoxModule,
    DxTreeViewModule,
    DxPopupModule,
    DxAccordionModule,
    DxScrollViewModule,
    DxFileManagerModule,
    DxTextAreaModule
  ],
  providers: []
})
export class CrudEmployeeDemandModule { }
