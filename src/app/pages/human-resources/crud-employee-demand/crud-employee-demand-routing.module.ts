import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { VexRoutes } from '../../../../@hubcore/interfaces/vex-route.interface';
import { CrudEmployeeDemandComponent } from './crud-employee-demand.component';

const routes: VexRoutes = [
  {
    path: '',
    component: CrudEmployeeDemandComponent,
    data: {
      toolbarShadowEnabled: true
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CrudEmployeeDemandRoutingModule { }
