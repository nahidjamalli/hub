export class DecisionHistory {
    FullName: string;
    Status: string;
    Note: string;
    DateTime: string;
}

export class FileItem {
    name: string;
    isDirectory: boolean;
    size?: number;
    items?: FileItem[];
}
