import { Component, OnInit } from '@angular/core';

import { ActivatedRoute, Router } from '@angular/router';
import DataSource from 'devextreme/data/data_source';
import ArrayStore from 'devextreme/data/array_store';
import { MatSnackBar } from '@angular/material/snack-bar';
import { GeneralService } from 'src/app/shared/services/global.service';

@Component({
  selector: 'crud-candidate',
  templateUrl: './crud-candidate.component.html',
  styleUrls: ['./crud-candidate.component.scss']
})
export class CrudCandidateComponent implements OnInit {

  Id = 0;
  user = null;

  dataSource: any = {};
  dsSkills: any;
  dsSelectedLanguages: any;
  dsLanguages: any;
  dsEmploymentTypes: any;
  dsEducationDegrees: any;
  dsSources: any;
  dsGenders: any;
  dsMilitaryStatuses: any;
  dsMaritalStatuses: any;
  dsDrivingCategories: any;
  dsCitizenshipList: any;
  dsCountries: any;
  dsCities: any;
  dsCandidateJoinContacts: any;
  dsContactTypes: any;
  EmploymentTypeEditorOptions = {};
  EducationDegreeEditorOptions = {};
  SourceEditorOptions = {};
  GenderEditorOptions = {};
  MaritalStatusEditorOptions = {};
  MilitaryStatusEditorOptions = {};
  DrivingCategoryEditorOptions = {};
  CitizenshipEditorOptions = {};
  BirthCountryEditorOptions = {};
  BirthCityEditorOptions = {};
  BirthDateEditorOptions = {};

  constructor(private generalService: GeneralService, private route: ActivatedRoute, private snackbar: MatSnackBar, private router: Router) {
    this.Id = Number(this.route.snapshot.params.id);
  }

  ngOnInit() {
    try {
      this.user = JSON.parse(localStorage.getItem('user'));
    }
    catch {
      this.router.navigate(['/signin']);
      return;
    }

    if (this.Id != 0) {
      this.getCandidate();
    }

    this.getEmploymentTypes();
    this.getSources();
    this.getEducationDegrees();
    this.getGenders();
    this.getMaritalStatuses();
    this.getMilitaryStatuses();
    this.getCitizenshipList();
    this.getBirthCountry();
    this.getDrivingCategories();
    this.getContactTypes();
    this.getLanguages();
    this.getSkills();

    this.BirthDateEditorOptions = {
      min: ((new Date()).getFullYear() - 100) + "/1/1",
      displayFormat: "yyyy/MM/dd",
      max: ((new Date()).getFullYear() - 14) + "/12/31",
      dateOutOfRangeMessage: "Birth Date is out of range",
      showClearButton: true
    };
  }

  async getCandidate() {
    this.dataSource = await this.generalService.sendRequest({ Id: this.Id }, '/human/get-candidate');

    if (this.Id != 0 && this.dataSource == null) {
      await this.router.navigate(['/*']);
      return;
    }

    this.getCandidateJoinDrivingCategories();
    this.getCandidateJoinEducationDegrees();
    this.getCandidateJoinSkills();
    this.getBirthCity(this.dataSource['BirthCountryId'] ?? 0);
  }

  async getSources() {
    this.dsSources = await this.generalService.sendRequest({}, '/human/get-sources');
    this.SourceEditorOptions = { dataSource: this.dsSources, valueExpr: "Id", displayExpr: "Name" };
  }

  async getEmploymentTypes() {
    this.dsEmploymentTypes = await this.generalService.sendRequest({}, '/human/get-employment-types');
    this.EmploymentTypeEditorOptions = { dataSource: this.dsEmploymentTypes, valueExpr: "Id", displayExpr: "Name" };
  }

  async getGenders() {
    this.dsGenders = await this.generalService.sendRequest({}, '/human/get-genders');
    this.GenderEditorOptions = { dataSource: this.dsGenders, valueExpr: "Id", displayExpr: "Name" };
  }

  async getMaritalStatuses() {
    this.dsMaritalStatuses = await this.generalService.sendRequest({}, '/human/get-marital-statuses');
    this.MaritalStatusEditorOptions = { dataSource: this.dsMaritalStatuses, valueExpr: "Id", displayExpr: "Name", showClearButton: true };
  }

  async getMilitaryStatuses() {
    this.dsMilitaryStatuses = await this.generalService.sendRequest({}, '/human/get-military-statuses');
    this.MilitaryStatusEditorOptions = { dataSource: this.dsMilitaryStatuses, valueExpr: "Id", displayExpr: "Name", showClearButton: true };
  }

  async getCitizenshipList() {
    this.dsCitizenshipList = await this.generalService.sendRequest({}, '/human/get-citizenship-list');
    this.CitizenshipEditorOptions = { dataSource: this.dsCitizenshipList, valueExpr: "Id", displayExpr: "Name", showClearButton: true };
  }

  async getContactTypes() {
    this.dsContactTypes = await this.generalService.sendRequest({}, '/config/get-contact-types');
    this.dsCandidateJoinContacts = await this.generalService.sendRequest({ CandidateId: this.Id }, '/human/get-candidate-join-contacts');
  }

  async getBirthCountry() {
    this.dsCountries = await this.generalService.sendRequest({}, '/config/get-countries');

    const that = this;
    this.BirthCountryEditorOptions = {
      dataSource: this.dsCountries, valueExpr: "Id", displayExpr: "Name", showClearButton: true,
      onValueChanged: async function (e) {
        if (e.value) that.getBirthCity(e.value);
        else that.dataSource.BirthCityId = null;
      }
    };
  }

  async getBirthCity(countryId) {
    this.dsCities = await this.generalService.sendRequest({ CountryId: countryId }, '/config/get-cities');
    this.BirthCityEditorOptions = { dataSource: this.dsCities, valueExpr: "Id", displayExpr: "Name", showClearButton: true };
  }

  async getLanguages() {
    this.dsLanguages = await this.generalService.sendRequest({}, '/config/get-languages');
    this.dsSelectedLanguages = await this.generalService.sendRequest({ CandidateId: this.Id }, '/human/get-candidate-join-languages');
  }

  async getSkills() {
    this.dataSource['Skills'] = [];
    this.dsSkills = await this.generalService.sendRequest({}, '/human/get-skills');

    this.dsSkills = new DataSource({
      store: new ArrayStore({
        data: this.dsSkills,
        key: "Id",
      }),
      group: "SkillType"
    });
  }

  async getCandidateJoinSkills() {
    this.dataSource['Skills'] = [];
    let result = await this.generalService.sendRequest({ CandidateId: this.Id }, '/human/get-candidate-join-skills');
    result.forEach(element => { this.dataSource['Skills'].push(element.Id); });
  }

  async getDrivingCategories() {
    this.dataSource['DrivingCategories'] = [];
    this.dsDrivingCategories = await this.generalService.sendRequest({}, '/human/get-driving-categories');
    this.DrivingCategoryEditorOptions = { dataSource: this.dsDrivingCategories, valueExpr: "Id", displayExpr: "Name", hideSelectedItems: true, value: this.dataSource['DrivingCategories'] };
  }

  async getCandidateJoinDrivingCategories() {
    this.dataSource['DrivingCategories'] = [];
    let result = await this.generalService.sendRequest({ CandidateId: this.Id }, '/human/get-candidate-join-driving-categories');
    result.forEach(element => { this.dataSource['DrivingCategories'].push(element.Id); });
  }

  async getEducationDegrees() {
    this.dataSource['EducationDegrees'] = [];
    this.dsEducationDegrees = await this.generalService.sendRequest({}, '/human/get-education-degrees');
    this.EducationDegreeEditorOptions = { dataSource: this.dsEducationDegrees, valueExpr: "Id", displayExpr: "Name", hideSelectedItems: true, value: this.dataSource['EducationDegrees'] };
  }

  async getCandidateJoinEducationDegrees() {
    this.dataSource['EducationDegrees'] = [];
    let result = await this.generalService.sendRequest({ CandidateId: this.Id }, '/human/get-candidate-join-education-degrees');
    result.forEach(element => { this.dataSource['EducationDegrees'].push(element.Id); });
  }

  async approve() {
    let controls = ['Firstname', 'Lastname', 'EmploymentTypeId', 'SourceId', 'GenderId'];
    this.dataSource['Contacts'] = this.dsCandidateJoinContacts;
    this.dataSource['Languages'] = this.dsSelectedLanguages;
    this.dataSource['CreatorUserId'] = this.user.Id;

    let messages = {
      Firstname: 'Fill in the first name.',
      Lastname: 'Fill in the last name.',
      EmploymentTypeId: 'Choose any employment type.',
      SourceId: 'Choose any source.',
      GenderId: 'Choose any gender.'
    };

    let resp = this.generalService.validateDataSource(this.dataSource, controls);
    if (resp === true) {
      await this.generalService.sendRequest(this.dataSource, '/human/save-candidate');
      this.snackbar.open('Operation succeeded.', 'Ok', { duration: 3000 });
      this.router.navigate(['/human-resources/candidates']);
    }
    else {
      this.snackbar.open('Operation failed.\n' + messages[resp], 'Ok', { duration: 3000 });
    }
  }

  onRowInserted() {
    this.dsCandidateJoinContacts[this.dsCandidateJoinContacts.length - 1].Id = 0;
  }
}
