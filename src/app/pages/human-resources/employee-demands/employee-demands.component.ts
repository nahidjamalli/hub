import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';
import { GeneralService } from 'src/app/shared/services/global.service';

@Component({
  selector: 'employee-demands',
  templateUrl: './employee-demands.component.html'
})
export class EmployeeDemandsComponent implements OnInit {
  dataSource: any;
  user = null;

  constructor(private generalService: GeneralService, private router: Router) { }

  async ngOnInit() {
    try {
      this.user = JSON.parse(localStorage.getItem('user'));
    }
    catch {
      await this.router.navigate(['/signin']);
      return;
    }
    this.dataSource = await this.generalService.sendRequest({ UserId: this.user.Id }, '/human/get-employee-demands');
  }

  onRowRemoving(e) {
    this.generalService.sendRequest(e.data, '/human/delete-employee-demand');
    e.cancel = false;
  }

  onInitNewRow(e) {
    this.router.navigate(['/human-resources/crud-employee-demand/0']);
    e.cancel = true;
  }

  onEditingStart(e) {
    this.router.navigate(['/human-resources/crud-employee-demand/' + e.data.Id]);
    e.cancel = true;
  }
}
