import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { VexRoutes } from '../../../../@hubcore/interfaces/vex-route.interface';
import { EmployeeDemandsComponent } from './employee-demands.component';

const routes: VexRoutes = [
  {
    path: '',
    component: EmployeeDemandsComponent,
    data: {
      toolbarShadowEnabled: true
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EmployeeDemandsRoutingModule {
}
