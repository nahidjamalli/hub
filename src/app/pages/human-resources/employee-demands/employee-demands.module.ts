import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageLayoutModule } from '../../../../@hubcore/components/page-layout/page-layout.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BreadcrumbsModule } from '../../../../@hubcore/components/breadcrumbs/breadcrumbs.module';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { EmployeeDemandsRoutingModule } from './employee-demands-routing.module';
import { DxDataGridModule } from 'devextreme-angular';

import { EmployeeDemandsComponent } from './employee-demands.component';

@NgModule({
  declarations: [EmployeeDemandsComponent],
  imports: [
    CommonModule,
    EmployeeDemandsRoutingModule,
    PageLayoutModule,
    FlexLayoutModule,
    BreadcrumbsModule,
    ReactiveFormsModule,
    MatButtonToggleModule,
    DxDataGridModule
  ],
  providers: []
})
export class EmployeeDemandsModule { }
