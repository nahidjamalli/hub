import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { VexRoutes } from '../../../../@hubcore/interfaces/vex-route.interface';
import { ExamCandidatesComponent } from './exam-candidates.component';

const routes: VexRoutes = [
  {
    path: '',
    component: ExamCandidatesComponent,
    data: {
      toolbarShadowEnabled: true
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ExamCandidatesRoutingModule { }
