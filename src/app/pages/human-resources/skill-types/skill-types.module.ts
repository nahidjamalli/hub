import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageLayoutModule } from '../../../../@hubcore/components/page-layout/page-layout.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BreadcrumbsModule } from '../../../../@hubcore/components/breadcrumbs/breadcrumbs.module';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { SkillTypesComponent } from './skill-types.component';
import { SkillTypesRoutingModule } from './skill-types-routing.module';
import { DxDataGridModule } from 'devextreme-angular';


@NgModule({
  declarations: [SkillTypesComponent],
  imports: [
    CommonModule,
    SkillTypesRoutingModule,
    PageLayoutModule,
    FlexLayoutModule,
    BreadcrumbsModule,
    ReactiveFormsModule,
    MatButtonToggleModule,
    DxDataGridModule
  ],
  providers: []
})
export class SkillTypesModule { }
