import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SkillTypesComponent } from './skill-types.component';


const routes: Routes = [
  {
    path: '',
    component: SkillTypesComponent,
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SkillTypesRoutingModule {
}
