import { Component, OnInit } from '@angular/core';
import { GeneralService } from 'src/app/shared/services/global.service';


@Component({
  selector: 'skill-types',
  templateUrl: './skill-types.component.html'
})
export class SkillTypesComponent implements OnInit {
  dataSource: any;

  constructor(private generalService: GeneralService) { }

  async ngOnInit() {
    this.dataSource = await this.generalService.sendRequest({}, '/human/get-skill-types');
  }

  onRowInserting(e) {
    let x = this.generalService.sendRequest(e.data, '/human/save-skill-type');
    x.then(x => this.dataSource[this.dataSource.length - 1].Id = x.ROWID);
    e.cancel = false;
  }

  onRowUpdating(e) {
    let updatedRow = Object.assign(e.oldData, e.newData);
    this.generalService.sendRequest(updatedRow, '/human/save-skill-type');
    e.cancel = false;
  }

  onRowRemoving(e) {
    this.generalService.sendRequest(e.data, '/human/delete-skill-type');
    e.cancel = false;
  }
}
