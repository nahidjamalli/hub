import { Component, OnInit, ViewChild } from '@angular/core';

import { ActivatedRoute, Router } from '@angular/router';
import CustomStore from 'devextreme/data/custom_store';
import { DxDropDownBoxComponent } from 'devextreme-angular';
import { MatSnackBar } from '@angular/material/snack-bar';
import { GeneralService } from 'src/app/shared/services/global.service';

@Component({
  selector: 'crud-job-offer',
  templateUrl: './crud-job-offer.component.html',
  styleUrls: ['./crud-job-offer.component.scss']
})
export class CrudJobOfferComponent implements OnInit {
  @ViewChild('application') applicationPopup: DxDropDownBoxComponent;

  selectedApplication: any = [];
  statusEditorOptions: any;

  Id = 0;
  user = null;

  // APPLICATIONS
  dsApplication: any = {};

  // STATUSES
  dsStatuses: any = {};

  dataSource: any = {};

  constructor(private generalService: GeneralService, private route: ActivatedRoute, private snackbar: MatSnackBar, private router: Router) {
    this.Id = Number(this.route.snapshot.params.id);
  }

  ngOnInit() {
    this.dataSource.IsCreatedEmployee = true;
    try {
      this.user = JSON.parse(localStorage.getItem('user'));
    }
    catch {
      this.router.navigate(['/signin']);
      return;
    }

    if (this.Id == 0) {
      this.dataSource = {};
      this.dataSource.StatusId = 300;
    }
    else {
      this.getJobOffer();
    }

    this.applications_createStore();
    this.getStatuses();
  }

  async getJobOffer() {
    this.dataSource = await this.generalService.sendRequest({ Id: this.Id }, '/human/get-job-offer');
    this.selectedApplication = await this.generalService.sendRequest({ Id: this.dataSource.ApplicationId ?? 0 }, '/human/get-application');
  }

  async getStatuses() {
    this.dsStatuses = await this.generalService.sendRequest({ StatusTypeId: 65, UserId: -2 }, '/config/get-statuses');
    this.statusEditorOptions = { dataSource: this.dsStatuses, valueExpr: "Id", displayExpr: "Name" };
  }

  async approve() {
    let messages = {
      ApplicationId: 'Choose any application.',
      Amount: 'Fill in the amount.',
      StatusId: 'Choose any status.',
      OfferDate: 'Choose any offer date.'
    };

    let controls = ['ApplicationId', 'Amount', 'StatusId', 'OfferDate'];

    if (this.selectedApplication[0]) this.dataSource['ApplicationId'] = this.selectedApplication[0].Id;
    this.dataSource['CreatorUserId'] = this.user.Id;

    let resp = this.generalService.validateDataSource(this.dataSource, controls);
    if (resp === true) {
      await this.generalService.sendRequest(this.dataSource, '/human/save-job-offer');
      this.snackbar.open('Operation succeeded.', 'Ok', { duration: 3000 });
      this.router.navigate(['/human-resources/job-offers']);
    }
    else {
      this.snackbar.open('Operation failed. ' + messages[resp], 'Ok', { duration: 3000 });
    }
  }

  gvApplications_rowDblClick() {
    this.applicationPopup.instance.close();
  }

  applications_displayValueFormatter(item) {
    item = item[0];
    return item && item.Code + " - " + item.Department + " - " + item.Position;
  }

  applications_createStore() {
    const _this = this;

    this.dsApplication.store = new CustomStore({
      load: async function (loadOptions: any) {
        let request = { UserId: _this.user.Id };
        if (loadOptions.filter) {
          for (var i = 0; i < loadOptions.filter.length; i += 2) {
            if (typeof loadOptions.filter[i] == "string") {
              request[loadOptions.filter[i]] = loadOptions.filter[2];
              break;
            }
            else {
              request[loadOptions.filter[i][0]] = loadOptions.filter[i][2];
            }
          }
        }

        if (Object.entries(request).length < 2) return null;
        else return _this.generalService.sendRequest(request, '/human/get-applications').then(r => {
          return {
            data: r
          };
        });
      },
      byKey: function () { return _this.dsApplication; }
    });
  }

  async createEmployee() {
    await this.generalService.sendRequest({ JobOfferId: this.Id }, '/human/create-as-employee');
    this.snackbar.open('Created ' + this.dataSource.CandidateFullname + ' as an employee successfully..', 'Ok', { duration: 3000 });
    this.dataSource.IsCreatedEmployee = true;
  }
}
