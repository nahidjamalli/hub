import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageLayoutModule } from '../../../../@hubcore/components/page-layout/page-layout.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BreadcrumbsModule } from '../../../../@hubcore/components/breadcrumbs/breadcrumbs.module';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { CrudJobOfferRoutingModule } from './crud-job-offer-routing.module';
import { DxFormModule, DxDataGridModule, DxValidatorModule, DxDropDownBoxModule, DxTextAreaModule } from 'devextreme-angular';

import { MatSnackBarModule } from '@angular/material/snack-bar';
import { CrudJobOfferComponent } from './crud-job-offer.component';
import { DxiItemModule } from 'devextreme-angular/ui/nested';
import { DxButtonModule } from 'devextreme-angular';
import { MatTabsModule } from '@angular/material/tabs';

@NgModule({
  declarations: [CrudJobOfferComponent],
  imports: [
    MatSnackBarModule,
    CommonModule,
    CrudJobOfferRoutingModule,
    PageLayoutModule,
    FlexLayoutModule,
    BreadcrumbsModule,
    ReactiveFormsModule,
    MatButtonToggleModule,
    DxFormModule,
    DxiItemModule,
    DxDataGridModule,
    DxButtonModule,
    DxValidatorModule,
    DxDropDownBoxModule,
    MatTabsModule,
    DxTextAreaModule
  ],
  providers: []
})
export class CrudJobOfferModule { }
