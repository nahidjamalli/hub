import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { VexRoutes } from '../../../../@hubcore/interfaces/vex-route.interface';
import { CrudJobOfferComponent } from './crud-job-offer.component';

const routes: VexRoutes = [
  {
    path: '',
    component: CrudJobOfferComponent,
    data: {
      toolbarShadowEnabled: true
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CrudJobOfferRoutingModule {
}
