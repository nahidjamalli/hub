import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { GeneralService } from 'src/app/shared/services/global.service';

@Component({
  selector: 'bank-accounts',
  templateUrl: './bank-accounts.component.html'
})
export class BankAccountsComponent implements OnInit {
  dataSource: any = {};
  dsBanks: any = {};

  constructor(private generalService: GeneralService, private router: Router) { }

  ngOnInit() {
    this.getBanks();
    this.getBankAccounts();
  }

  onRowInserting(e) {
    let x = this.generalService.sendRequest(e.data, '/finance/save-bank-account');
    x.then(x => this.dataSource[this.dataSource.length - 1].Id = x.ROWID);
  }

  onRowUpdating(e) {
    let updatedRow = Object.assign(e.oldData, e.newData);
    this.generalService.sendRequest(updatedRow, '/finance/save-bank-account');
  }

  onRowRemoving(e) {
    this.generalService.sendRequest(e.data, '/finance/delete-bank-account');
  }

  async getBankAccounts() {
    this.dataSource = await this.generalService.sendRequest({}, '/finance/get-bank-accounts');
  }

  async getBanks() {
    this.dsBanks = await this.generalService.sendRequest({}, '/finance/get-banks');
  }
}
