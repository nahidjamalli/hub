import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { BanksComponent } from './banks.component';
import { BanksRoutingModule } from './banks-routing.module';

import { TranslateModule } from '@ngx-translate/core';
import { DxDataGridModule } from 'devextreme-angular';
import { PageLayoutModule } from 'src/@hubcore/components/page-layout/page-layout.module';
import { BreadcrumbsModule } from 'src/@hubcore/components/breadcrumbs/breadcrumbs.module';

@NgModule({
  declarations: [BanksComponent],
  imports: [
    CommonModule,
    BanksRoutingModule,
    PageLayoutModule,
    FlexLayoutModule,
    BreadcrumbsModule,
    ReactiveFormsModule,
    MatButtonToggleModule,
    DxDataGridModule,
    TranslateModule
  ],
  providers: []
})
export class BanksModule { }
