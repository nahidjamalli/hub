import { Component, OnInit } from '@angular/core';

import { Router } from '@angular/router';
import { GeneralService } from 'src/app/shared/services/global.service';

@Component({
  selector: 'banks',
  templateUrl: './banks.component.html'
})
export class BanksComponent implements OnInit {
  dataSource: any = {};

  constructor(private generalService: GeneralService, private router: Router) { }

  ngOnInit() {
    this.getBanks();
  }

  onRowInserting(e) {
    let x = this.generalService.sendRequest(e.data, '/finance/save-bank');
    x.then(x => this.dataSource[this.dataSource.length - 1].Id = x.ROWID);
  }

  onRowUpdating(e) {
    let updatedRow = Object.assign(e.oldData, e.newData);
    this.generalService.sendRequest(updatedRow, '/finance/save-bank');
  }

  onRowRemoving(e) {
    this.generalService.sendRequest(e.data, '/finance/delete-bank');
  }

  async getBanks() {
    this.dataSource = await this.generalService.sendRequest({}, '/finance/get-banks');
  }
}
