import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SecurityRoutingModule } from './security-routing.module';

@NgModule({
    imports: [
        CommonModule,
        SecurityRoutingModule
    ],
    providers: []
})
export class ApplicationsModule { }
