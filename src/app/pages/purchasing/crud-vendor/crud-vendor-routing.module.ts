import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { VexRoutes } from 'src/@hubcore/interfaces/vex-route.interface';
import { CrudVendorComponent } from './crud-vendor.component';

const routes: VexRoutes = [
  {
    path: '',
    component: CrudVendorComponent,
    data: {
      toolbarShadowEnabled: true
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CrudVendorRoutingModule { }
