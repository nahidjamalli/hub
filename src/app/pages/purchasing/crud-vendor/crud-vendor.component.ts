import { Component, OnInit } from "@angular/core";
import { FormControl } from "@angular/forms";
import { ActivatedRoute, Router } from "@angular/router";
import { GeneralService } from "src/app/shared/services/global.service";

@Component({
    selector: "crud-vendor",
    templateUrl: './crud-vendor.component.html',
})

export class CrudVendorComponent implements OnInit {
    Id = 0
    dsVendor: any = {};

    async ngOnInit() {
        if (this.Id)
            this.dsVendor = await this.generalService.sendRequest({ Id: this.Id }, '/purchase/get-vendor');
    }

    constructor(private generalService: GeneralService, private route: ActivatedRoute, private router: Router) {
        this.Id = Number(this.route.snapshot.params.id);
    }

    async approve() {
        this.dsVendor.Id = this.Id;
        this.generalService.sendRequest(this.dsVendor, '/purchase/save-vendor');
        await this.router.navigate(['/finance/vendors']);
    }
}
