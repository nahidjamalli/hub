import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FlexLayoutModule } from "@angular/flex-layout";
import { ReactiveFormsModule } from "@angular/forms";
import { MatButtonToggleModule } from "@angular/material/button-toggle";
import { TranslateModule } from "@ngx-translate/core";
import { DxButtonModule, DxFormModule } from "devextreme-angular";
import { BreadcrumbsModule } from "src/@hubcore/components/breadcrumbs/breadcrumbs.module";
import { PageLayoutModule } from "src/@hubcore/components/page-layout/page-layout.module";
import { GeneralService } from "src/app/shared/services/global.service";
import { CrudVendorRoutingModule } from "./crud-vendor-routing.module";
import { CrudVendorComponent } from "./crud-vendor.component";

@NgModule({
    declarations: [CrudVendorComponent],
    imports: [
        CommonModule,
        CrudVendorRoutingModule,
        PageLayoutModule,
        FlexLayoutModule,
        BreadcrumbsModule,
        ReactiveFormsModule,
        MatButtonToggleModule,
        TranslateModule,
        DxFormModule,
        DxButtonModule
    ],
    providers: []
})

export class CrudVendorModule { }