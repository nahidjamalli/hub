import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { GeneralService } from 'src/app/shared/services/global.service';


@Component({
  selector: 'crud-purchase-order',
  templateUrl: './crud-purchase-order.component.html',
  styleUrls: ['./crud-purchase-order.component.scss']
})
export class CrudPurchaseOrderComponent {

  Id = 0;
  dsPurchaseOrder: any = {};
  dsVendors: any = {};
  dsLines: any = [];
  dsItems: any = {};
  dsLocations: any = {};
  dsPurchaseOrders: any = {};

  constructor(private generalService: GeneralService, private route: ActivatedRoute, private router: Router) {
    this.Id = Number(this.route.snapshot.params.id);
  }

  async ngOnInit() {
    this.dsVendors = await this.generalService.sendRequest({}, '/purchase/get-vendors');
    this.dsItems = await this.generalService.sendRequest({}, '/inventory/get-items');
    this.dsLocations = await this.generalService.sendRequest({}, '/admin/get-locations');

    if (this.Id) {
      this.dsPurchaseOrder = await this.generalService.sendRequest({ Id: this.Id }, '/purchase/get-purchase-order');
      this.dsLines = await this.generalService.sendRequest({}, '/purchase/get-purchase-order-lines');
    }
  }

  async approve() {
    this.dsPurchaseOrder.Id = this.Id;

    if (this.Id == 0) {
      let result = await this.generalService.sendRequest(this.dsPurchaseOrder, '/purchase/save-purchase-order');
      this.dsPurchaseOrder.Id = result.ROWID;
    }

    this.dsLines.forEach(element => {
      element.PurchaseOrderId = this.dsPurchaseOrder.Id;
      element.Id = 0;
      this.generalService.sendRequest(element, '/purchase/save-purchase-order-line');
    });

    await this.router.navigate(['/purchasing/purchase-orders']);
  }
}