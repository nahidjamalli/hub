import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { VexRoutes } from 'src/@hubcore/interfaces/vex-route.interface';
import { CrudPurchaseOrderComponent } from './crud-purchase-order.component';

const routes: VexRoutes = [
  {
    path: '',
    component: CrudPurchaseOrderComponent,
    data: {
      toolbarShadowEnabled: true
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CrudPurchaseOrderRoutingModule { }
