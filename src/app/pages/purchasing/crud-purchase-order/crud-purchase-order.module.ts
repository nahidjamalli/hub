import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageLayoutModule } from 'src/@hubcore/components/page-layout/page-layout.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BreadcrumbsModule } from 'src/@hubcore/components/breadcrumbs/breadcrumbs.module';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { CrudPurchaseOrderComponent } from './crud-purchase-order.component';
import { CrudPurchaseOrderRoutingModule } from './crud-purchase-order-routing.module';

import { TranslateModule } from '@ngx-translate/core';
import { DxButtonModule, DxDataGridModule, DxFormModule, DxSelectBoxModule } from 'devextreme-angular';
import { MatTabsModule } from '@angular/material/tabs';
import { DxoLookupModule } from 'devextreme-angular/ui/nested';

@NgModule({
  declarations: [CrudPurchaseOrderComponent],
  imports: [
    CommonModule,
    CrudPurchaseOrderRoutingModule,
    PageLayoutModule,
    FlexLayoutModule,
    BreadcrumbsModule,
    ReactiveFormsModule,
    MatButtonToggleModule,
    TranslateModule,
    DxFormModule,
    DxButtonModule,
    DxSelectBoxModule,
    MatTabsModule,
    DxDataGridModule,
    DxoLookupModule
  ],
  providers: []
})
export class CrudPurchaseOrderModule { }
