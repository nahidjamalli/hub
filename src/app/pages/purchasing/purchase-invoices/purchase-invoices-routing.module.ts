import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { VexRoutes } from 'src/@hubcore/interfaces/vex-route.interface';
import { PurchaseInvoicesComponent } from './purchase-invoices.component';

const routes: VexRoutes = [
  {
    path: '',
    component: PurchaseInvoicesComponent,
    data: {
      toolbarShadowEnabled: true
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PurchaseInvoicesRoutingModule { }
