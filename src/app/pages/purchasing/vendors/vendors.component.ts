import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { GeneralService } from "src/app/shared/services/global.service";

@Component({
    selector: "vendors",
    templateUrl: './vendors.component.html'
})

export class VendorsComponent implements OnInit {
    dataSource: any = {};

    constructor(private router: Router, private generalService: GeneralService) { }

    async ngOnInit() {
        this.dataSource = await this.generalService.sendRequest({}, '/purchase/get-vendors');
    }

    onInitNewRow(e) {
        this.router.navigate(['/finance/crud-vendor/0']);
        e.cancel = false;
    }

    onEditingStart(e) {
        this.router.navigate(['/finance/crud-vendor/' + e.data.Id]);
        e.cancel = false;
    }

    onRowRemoving(e) {
        this.generalService.sendRequest({ Id: e.data.Id }, '/purchase/delete-vendor')
    }
}