import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { PageLayoutModule } from "src/@hubcore/components/page-layout/page-layout.module";
import { FlexLayoutModule } from "@angular/flex-layout";
import { BlanketPurchaseOrderCardComponent } from "./blanket-purchase-order-card.component";
import { BlanketPurchaseOrderCardRoutingModule } from "./blanket-purchase-order-card-routing.module";
import { HubDataGridModule } from "src/@hubcore/components/hubital/hub-data-grid/hub-data-grid.module";
import { CardHeaderModule } from "src/@hubcore/components/card-header/card-header.module";
import { DropDownsModule } from "@progress/kendo-angular-dropdowns";
import { HubSlideToggleModule } from "src/@hubcore/components/hubital/hub-form-elements/hub-slide-toggle/hub-slide-toggle.module";
import { HubInputModule } from "src/@hubcore/components/hubital/hub-form-elements/hub-input/hub-input.module";
import { HubFormFieldModule } from "src/@hubcore/components/hubital/hub-form-elements/hub-form-field/hub-form-field.module";
import { ReactiveFormsModule } from "@angular/forms";
import { HubDatepickerModule } from "src/@hubcore/components/hubital/hub-form-elements/hub-datepicker/hub-datepicker.module";
import { MatButtonModule } from "@angular/material/button";

@NgModule({
  declarations: [BlanketPurchaseOrderCardComponent],
  imports: [
    CommonModule,
    BlanketPurchaseOrderCardRoutingModule,
    ReactiveFormsModule,
    PageLayoutModule,
    FlexLayoutModule,
    HubDataGridModule,
    CardHeaderModule,
    HubFormFieldModule,
    HubInputModule,
    HubSlideToggleModule,
    DropDownsModule,
    HubDatepickerModule,
    MatButtonModule
  ],
  exports: [BlanketPurchaseOrderCardComponent],
  providers: [],
})
export class BlanketPurchaseOrderCardModule {}
