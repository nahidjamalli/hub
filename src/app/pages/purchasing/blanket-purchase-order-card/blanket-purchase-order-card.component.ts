import { Component } from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { ActivatedRoute } from "@angular/router";
import { GeneralService } from "src/app/shared/services/global.service";

@Component({
  selector: "blanket-purchase-order-card",
  templateUrl: "./blanket-purchase-order-card.component.html",
})
export class BlanketPurchaseOrderCardComponent {
  Id = 0;
  dsVendors: any = [];
  dataSource: any = [];
  pageSize = 25;
  pageSizeOptions: number[] = [25, 50, 100];
  columns = [
    {
      dataField: "Item",
      label: "item",
    },
    {
      dataField: "QuantityToReceive",
      label: "quantity-to-receive",
    },
    {
      dataField: "QuantityReceive",
      label: "quantity-receive",
    },
    {
      dataField: "QuantityInvoiced",
      label: "quantity-invoiced",
    },
  ];
  itemForm: FormGroup = this.fb.group({
    Id: this.Id,
    Code: [""],
    VendorId: ["", Validators.required],
    VendorShipmentNumber: ["", Validators.required],
    VendorOrderNumber: ["", Validators.required],
    DocumentDate: ["", Validators.required],
    DueDate: ["", Validators.required],
    OrderDate: ["", Validators.required],
  });
  constructor(
    private generalService: GeneralService,
    private fb: FormBuilder,
    private route: ActivatedRoute
  ) {
    this.Id = Number(this.route.snapshot.params.id);
  }

  ngOnInit() {
    this.getVendors();
    if (this.Id != 0) this.getBlanketPurchaseOrder();
  }

  async getVendors() {
    this.dsVendors = await this.generalService.sendRequest(
      {},
      "/purchase/get-vendors"
    );
  }
  async getBlanketPurchaseOrder() {
    await this.generalService
      .sendRequest({ Id: this.Id }, "/purchase/get-blanket-purchase-order")
      .then((result) => this.updateForm(result));
  }
  
  updateForm(data) {
    this.itemForm.patchValue({
      Id: data.Id,
      Code: data.Code,
      VendorId: data.VendorId,
      VendorShipmentNumber: data.VendorShipmentNumber,
      VendorOrderNumber: data.VendorOrderNumber,
      DocumentDate: data.DocumentDate,
      DueDate: data.DueDate,
      OrderDate: data.OrderDate,
    });
  }

  async approve() {
    console.log(this.Id);
    if (this.Id == 0) {
      await this.generalService.sendRequest(
        this.itemForm.value,
        "/purchase/save-blanket-purchase-order"
      );
    } else {
      await this.generalService.sendRequest(
        this.itemForm.value,
        "/purchase/save-blanket-purchase-order"
      );
    }
    this.itemForm.reset()
  }
}
