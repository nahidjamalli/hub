import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";
import { VexRoutes } from "src/@hubcore/interfaces/vex-route.interface";
import { BlanketPurchaseOrderCardComponent } from "./blanket-purchase-order-card.component";

const routes: VexRoutes = [
  {
    path: "",
    component: BlanketPurchaseOrderCardComponent,
    data: {
      toolbarShadowEnabled: true,
    },
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BlanketPurchaseOrderCardRoutingModule {}
