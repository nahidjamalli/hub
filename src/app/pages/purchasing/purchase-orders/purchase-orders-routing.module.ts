import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { VexRoutes } from 'src/@hubcore/interfaces/vex-route.interface';
import { PurchaseOrdersComponent } from './purchase-orders.component';

const routes: VexRoutes = [
  {
    path: '',
    component: PurchaseOrdersComponent,
    data: {
      toolbarShadowEnabled: true
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PurchaseOrdersRoutingModule { }
