import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageLayoutModule } from 'src/@hubcore/components/page-layout/page-layout.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BreadcrumbsModule } from 'src/@hubcore/components/breadcrumbs/breadcrumbs.module';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { PurchaseOrdersComponent } from './purchase-orders.component';
import { PurchaseOrdersRoutingModule } from './purchase-orders-routing.module';

import { TranslateModule } from '@ngx-translate/core';
import { DxDataGridModule, DxFormModule } from 'devextreme-angular';

@NgModule({
  declarations: [PurchaseOrdersComponent],
  imports: [
    CommonModule,
    PurchaseOrdersRoutingModule,
    PageLayoutModule,
    FlexLayoutModule,
    BreadcrumbsModule,
    ReactiveFormsModule,
    MatButtonToggleModule,
    TranslateModule,
    DxDataGridModule,
    DxFormModule
  ],
  exports: [PurchaseOrdersComponent],
  providers: []
})
export class PurchaseOrdersModule { }
