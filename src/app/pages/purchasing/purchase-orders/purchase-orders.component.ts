import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { GeneralService } from 'src/app/shared/services/global.service';


@Component({
  selector: 'purchase-orders',
  templateUrl: './purchase-orders.component.html'
})
export class PurchaseOrdersComponent {
  dataSource: any = {};

  constructor(private generalService: GeneralService, private router: Router) { }

  async ngOnInit() {
    this.dataSource = await this.generalService.sendRequest({}, '/purchase/get-purchase-orders');
  }

  onInitNewRow(e) {
    this.router.navigate(['/purchasing/crud-purchase-order/0']);
  }

  onEditingStart(e) {
    this.router.navigate(['/purchasing/crud-purchase-order/' + e.data.Id]);
  }

  onRowRemoving(e) {
    this.generalService.sendRequest({ Id: e.data.Id }, '/purchase/delete-purchase-order');
  }
}