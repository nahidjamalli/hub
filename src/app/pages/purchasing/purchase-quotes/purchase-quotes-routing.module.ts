import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { VexRoutes } from 'src/@hubcore/interfaces/vex-route.interface';
import { PurchaseQuotesComponent } from './purchase-quotes.component';

const routes: VexRoutes = [
  {
    path: '',
    component: PurchaseQuotesComponent,
    data: {
      toolbarShadowEnabled: true
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PurchaseQuotesRoutingModule { }
