import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { GeneralService } from "src/app/shared/services/global.service";

@Component({
  selector: "blanket-purchase-orders",
  templateUrl: "./blanket-purchase-orders.component.html",
})
export class BlanketPurchaseOrdersComponent implements OnInit {
  dsBlanketPurchaseOrders: any = [];
  pageSize = 25;
  pageSizeOptions: number[] = [25, 50, 100];
  columns = [
    {
      dataField: "Code",
      label: "code",
    },
    {
      dataField: "DocumentDate",
      label: "document-date",
    },
    {
      dataField: "OrderDate",
      label: "order-date",
    },
    {
      dataField: "DueDate",
      label: "due-date",
    },
    {
      dataField: "VendorShipmentNumber",
      label: "vendor-shipment-number",
    },
    {
      dataField: "VendorOrderNumber",
      label: "vendor-order-number",
    },
    {
      dataField: "VendorCode",
      label: "vendor-code",
    },
    {
      dataField: "VendorName",
      label: "vendor-name",
    },
    {
      dataField: "VendorSearchName",
      label: "vendor-search-name",
    },
  ];

  constructor(private generalService: GeneralService, private router: Router) {}

  ngOnInit(): void {
    this.getBlanketPurchaseOrders();
  }

  insertBlanketPurchaseOrder() {
    this.router.navigate(["/purchasing/blanket-purchase-order-card/0"]);
  }
  editBlanketPurchaseOrder(e) {
    this.router.navigate([
      "/purchasing/blanket-purchase-order-card/" + e.data.Id,
    ]);
  }
  deleteBlanketPurchaseOrder(e) {
    this.generalService
      .sendRequest({ Id: e.data.Id }, "/purchase/delete-blanket-purchase-order")
      .then(() => this.getBlanketPurchaseOrders());
  }

  async getBlanketPurchaseOrders() {
    this.dsBlanketPurchaseOrders = await this.generalService.sendRequest(
      {},
      "/purchase/get-blanket-purchase-orders"
    );
  }
  getDateDisplayFormat(date: string) {
    return date.slice(0, 10);
  }
}
