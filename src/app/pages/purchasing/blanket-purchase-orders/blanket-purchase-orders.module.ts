import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { PageLayoutModule } from "src/@hubcore/components/page-layout/page-layout.module";
import { FlexLayoutModule } from "@angular/flex-layout";
import { BlanketPurchaseOrdersComponent } from "./blanket-purchase-orders.component";
import { BlanketPurchaseOrdersRoutingModule } from "./blanket-purchase-orders-routing.module";
import { HubDataGridModule } from "src/@hubcore/components/hubital/hub-data-grid/hub-data-grid.module";
import { CardHeaderModule } from "src/@hubcore/components/card-header/card-header.module";

@NgModule({
  declarations: [BlanketPurchaseOrdersComponent],
  imports: [
    CommonModule,
    BlanketPurchaseOrdersRoutingModule,
    PageLayoutModule,
    FlexLayoutModule,
    HubDataGridModule,
    CardHeaderModule,
  ],
  exports: [BlanketPurchaseOrdersComponent],
  providers: [],
})
export class BlanketPurchaseOrdersModule {}
