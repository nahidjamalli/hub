import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PageLayoutModule } from 'src/@hubcore/components/page-layout/page-layout.module';
import { FlexLayoutModule } from '@angular/flex-layout';
import { BreadcrumbsModule } from 'src/@hubcore/components/breadcrumbs/breadcrumbs.module';
import { ReactiveFormsModule } from '@angular/forms';
import { MatButtonToggleModule } from '@angular/material/button-toggle';
import { SalesInvoicesComponent } from './sales-invoices.component';
import { SalesInvoicesRoutingModule } from './sales-invoices-routing.module';

import { TranslateModule } from '@ngx-translate/core';
import { DxDataGridModule, DxFormModule } from 'devextreme-angular';

@NgModule({
  declarations: [SalesInvoicesComponent],
  imports: [
    CommonModule,
    SalesInvoicesRoutingModule,
    PageLayoutModule,
    FlexLayoutModule,
    BreadcrumbsModule,
    ReactiveFormsModule,
    MatButtonToggleModule,
    TranslateModule,
    DxDataGridModule,
    DxFormModule
  ],
  exports: [SalesInvoicesComponent],
  providers: []
})
export class SalesInvoicesModule { }
