import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { VexRoutes } from 'src/@hubcore/interfaces/vex-route.interface';
import { SalesInvoicesComponent } from './sales-invoices.component';

const routes: VexRoutes = [
  {
    path: '',
    component: SalesInvoicesComponent,
    data: {
      toolbarShadowEnabled: true
    }
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SalesInvoicesRoutingModule { }
