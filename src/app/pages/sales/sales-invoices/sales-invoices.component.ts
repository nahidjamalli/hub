import { Component } from '@angular/core';
import { GeneralService } from 'src/app/shared/services/global.service';


@Component({
  selector: 'sales-invoices',
  templateUrl: './sales-invoices.component.html'
})
export class SalesInvoicesComponent {
  dataSource: any = {};

  constructor(private generalService: GeneralService) { }

  async ngOnInit() {
    // this.dataSource = await this.generalService.sendRequest({}, '');
  }

  onRowInserting(e) {
    // let x = this.generalService.sendRequest(e.data, '');
    // x.then(x => {
    //   this.dataSource[this.dataSource.length - 1].Id = x.ROWID;
    //   this.dataSource[this.dataSource.length - 1].Code = x.CODE;
    // });
  }

  onRowUpdating(e) {
    // let updatedRow = Object.assign(e.oldData, e.newData);
    // this.generalService.sendRequest(updatedRow, '');
  }

  onRowRemoving(e) {
    // this.generalService.sendRequest({ Id: e.data.Id }, '');
  }
}
