import { Component } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
import { GeneralService } from "src/app/shared/services/global.service";


@Component({
  selector: "crud-customer",
  templateUrl: './crud-customer.component.html',
  styleUrls: ['./crud-customer.component.scss']
})

export class CrudCustomerComponent {
  Id = 0;
  dsCustomer: any = {};

  async ngOnInit() {

    if (this.Id)
      this.dsCustomer = await this.generalService.sendRequest({ Id: this.Id }, '/sales/get-customer');
  }

  constructor(private generalService: GeneralService, private route: ActivatedRoute, private router: Router) {
    this.Id = Number(this.route.snapshot.params.id);
  }

  async approve() {
    this.dsCustomer.Id = this.Id;
    this.generalService.sendRequest(this.dsCustomer, '/sales/save-customer');
    await this.router.navigate(['/finance/customers']);
  }
}