import { CommonModule } from "@angular/common";
import { NgModule } from "@angular/core";
import { FlexLayoutModule } from "@angular/flex-layout";
import { ReactiveFormsModule } from "@angular/forms";
import { MatButtonToggleModule } from "@angular/material/button-toggle";
import { MatSnackBarModule } from "@angular/material/snack-bar";
import { MatTabsModule } from "@angular/material/tabs";
import { TranslateModule } from "@ngx-translate/core";
import { DxButtonModule, DxFormModule, DxSelectBoxModule } from "devextreme-angular";
import { BreadcrumbsModule } from "src/@hubcore/components/breadcrumbs/breadcrumbs.module";
import { PageLayoutModule } from "src/@hubcore/components/page-layout/page-layout.module";
import { GeneralService } from "src/app/shared/services/global.service";
import { CrudCustomerRoutingModule } from "./crud-customer-routing.module";
import { CrudCustomerComponent } from "./crud-customer.component";

@NgModule({
    declarations: [CrudCustomerComponent],
    imports: [
        CommonModule,
        CrudCustomerRoutingModule,
        PageLayoutModule,
        FlexLayoutModule,
        BreadcrumbsModule,
        MatButtonToggleModule,
        ReactiveFormsModule,
        DxFormModule,
        DxButtonModule,
        TranslateModule

    ],
    providers: []
})

export class CrudCustomerModule { }