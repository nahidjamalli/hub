import { Component, OnInit } from "@angular/core";
import { Router } from "@angular/router";
import { GeneralService } from "src/app/shared/services/global.service";

@Component({
    selector: "customers",
    templateUrl: './customers.component.html'
})

export class CustomersComponent implements OnInit {
    dataSource: any = {};

    constructor(private router: Router, private generalService: GeneralService) { }

    async ngOnInit() {
        this.dataSource = await this.generalService.sendRequest({}, '/sales/get-customers');
    }

    onInitNewRow(e) {
        this.router.navigate(['/finance/crud-customer/0']);
        e.cancel = false;
    }

    onEditingStart(e) {
        this.router.navigate(['/finance/crud-customer/' + e.data.Id]);
        e.cancel = false;
    }

    onRowRemoving(e) {
        this.generalService.sendRequest({ Id: e.data.Id }, '/sales/delete-customer')
    }
}