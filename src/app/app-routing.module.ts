import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
import { VexRoutes } from 'src/@hubcore/interfaces/vex-route.interface';
import { CustomLayoutComponent } from './custom-layout/custom-layout.component';

const childrenRoutes: VexRoutes = [
  {
    path: "",
    redirectTo: "/home",
    pathMatch: "full",
  },
  {
    path: "home",
    loadChildren: () =>
      import("./pages/home/home.module").then((m) => m.HomeModule),
  },
  {
    path: "finance",
    children: [
      {
        path: "analysis-sections",
        loadChildren: () =>
          import(
            "./pages/finance/analysis-sections/analysis-sections.module"
          ).then((m) => m.AnalysisSectionsModule),
        // canActivate: [AuthGuard]
      },
      {
        path: "analysis-dimensions",
        loadChildren: () =>
          import(
            "./pages/finance/analysis-dimensions/analysis-dimensions.module"
          ).then((m) => m.AnalysisDimensionsModule),
        // canActivate: [AuthGuard]
      },
      {
        path: "account-categories",
        loadChildren: () =>
          import(
            "./pages/finance/account-categories/account-categories.module"
          ).then((m) => m.AccountCategoriesModule),
        // canActivate: [AuthGuard]
      },
      {
        path: "accounts",
        loadChildren: () =>
          import("./pages/finance/accounts/accounts.module").then(
            (m) => m.AccountsModule
          ),
        // canActivate: [AuthGuard]
      },
      {
        path: "crud-account-card/:id",
        loadChildren: () =>
          import(
            "./pages/finance/crud-account-card/crud-account-card.module"
          ).then((m) => m.CrudAccountCardModule),
        // canActivate: [AuthGuard]
      },
      {
        path: "electronic-invoice-codes",
        loadChildren: () =>
          import(
            "./pages/finance/electronic-invoice-codes/electronic-invoice-codes.module"
          ).then((m) => m.ElectronicInvoiceCodesModule),
        // canActivate: [AuthGuard]
      },
      {
        path: "journal-types",
        loadChildren: () =>
          import("./pages/finance/journal-types/journal-types.module").then(
            (m) => m.JournalTypesModule
          ),
        // canActivate: [AuthGuard]
      },
      {
        path: "journals",
        loadChildren: () =>
          import("./pages/finance/journals/journals.module").then(
            (m) => m.JournalsModule
          ),
        // canActivate: [AuthGuard]
      },
      {
        path: "crud-journal/:id",
        loadChildren: () =>
          import("./pages/finance/crud-journal/crud-journal.module").then(
            (m) => m.CrudJournalCardModule
          ),
        // canActivate: [AuthGuard]
      },

      {
        path: "vendors",
        loadChildren: () =>
          import("./pages/purchasing/vendors/vendors.module").then(
            (m) => m.VendorsModule
          ),
        // canActivate: [AuthGuard]
      },
      {
        path: "crud-vendor/:id",
        loadChildren: () =>
          import("./pages/purchasing/crud-vendor/crud-vendor.module").then(
            (m) => m.CrudVendorModule
          ),
        // canActivate: [AuthGuard]
      },
    ],
  },

  {
    path: "banking",
    children: [
      {
        path: "banks",
        loadChildren: () =>
          import("./pages/cash-management/banking/banks/banks.module").then(
            (m) => m.BanksModule
          ),
        // canActivate: [AuthGuard]
      },
      {
        path: "bank-accounts",
        loadChildren: () =>
          import(
            "./pages/cash-management/banking/bank-accounts/bank-accounts.module"
          ).then((m) => m.BankAccountsModule),
        // canActivate: [AuthGuard]
      },
    ],
  },
  {
    path: "sales",
    children: [
      {
        path: "customers",
        loadChildren: () =>
          import("./pages/sales/customers/customers.module").then(
            (m) => m.CustomersModule
          ),
        // canActivate: [AuthGuard]
      },
      {
        path: "crud-customer/:id",
        loadChildren: () =>
          import("./pages/sales/crud-customer/crud-customer.module").then(
            (m) => m.CrudCustomerModule
          ),
        // canActivate: [AuthGuard]
      },
      {
        path: "sales-orders",
        loadChildren: () =>
          import("./pages/sales/sales-orders/sales-orders.module").then(
            (m) => m.SalesOrdersModule
          ),
        // canActivate: [AuthGuard]
      },
      {
        path: "sales-invoices",
        loadChildren: () =>
          import("./pages/sales/sales-invoices/sales-invoices.module").then(
            (m) => m.SalesInvoicesModule
          ),
        // canActivate: [AuthGuard]
      },
      {
        path: "sales-quotes",
        loadChildren: () =>
          import("./pages/sales/sales-quotes/sales-quotes.module").then(
            (m) => m.SalesQuotesModule
          ),
        // canActivate: [AuthGuard]
      },
    ],
  },
  {
    path: "purchasing",
    children: [
      {
        path: "vendors",
        loadChildren: () =>
          import("./pages/purchasing/vendors/vendors.module").then(
            (m) => m.VendorsModule
          ),
        // canActivate: [AuthGuard]
      },
      {
        path: "crud-vendor/:id",
        loadChildren: () =>
          import("./pages/purchasing/crud-vendor/crud-vendor.module").then(
            (m) => m.CrudVendorModule
          ),
        // canActivate: [AuthGuard]
      },
      {
        path: "purchase-orders",
        loadChildren: () =>
          import(
            "./pages/purchasing/purchase-orders/purchase-orders.module"
          ).then((m) => m.PurchaseOrdersModule),
        // canActivate: [AuthGuard]
      },
      {
        path: "crud-purchase-order/:id",
        loadChildren: () =>
          import(
            "./pages/purchasing/crud-purchase-order/crud-purchase-order.module"
          ).then((m) => m.CrudPurchaseOrderModule),
        // canActivate: [AuthGuard]
      },
      {
        path: "purchase-invoices",
        loadChildren: () =>
          import(
            "./pages/purchasing/purchase-invoices/purchase-invoices.module"
          ).then((m) => m.PurchaseInvoicesModule),
        // canActivate: [AuthGuard]
      },
      {
        path: "purchase-quotes",
        loadChildren: () =>
          import(
            "./pages/purchasing/purchase-quotes/purchase-quotes.module"
          ).then((m) => m.PurchaseQuotesModule),
        // canActivate: [AuthGuard]
      },
      {
        path: "blanket-purchase-orders",
        loadChildren: () =>
          import(
            "./pages/purchasing/blanket-purchase-orders/blanket-purchase-orders.module"
          ).then((m) => m.BlanketPurchaseOrdersModule),
        // canActivate: [AuthGuard]
      },
      {
        path: "blanket-purchase-order-card/:id",
        loadChildren: () =>
          import(
            "./pages/purchasing/blanket-purchase-order-card/blanket-purchase-order-card.module"
          ).then((m) => m.BlanketPurchaseOrderCardModule),
        // canActivate: [AuthGuard]
      },
    ],
  },
  {
    path: "human-resources",
    children: [
      {
        path: "structures",
        loadChildren: () =>
          import("./pages/human-resources/structures/structures.module").then(
            (m) => m.StructuresModule
          ),
        // canActivate: [AuthGuard]
      },
      {
        path: "departments",
        loadChildren: () =>
          import("./pages/human-resources/departments/departments.module").then(
            (m) => m.DepartmentsModule
          ),
        // canActivate: [AuthGuard]
      },
      {
        path: "positions",
        loadChildren: () =>
          import("./pages/human-resources/positions/positions.module").then(
            (m) => m.PositionsModule
          ),
        // canActivate: [AuthGuard]
      },
      {
        path: "crud-employee-demand/:id",
        loadChildren: () =>
          import(
            "./pages/human-resources/crud-employee-demand/crud-employee-demand.module"
          ).then((m) => m.CrudEmployeeDemandModule),
        // canActivate: [AuthGuard]
      },
      {
        path: "employee-demands",
        loadChildren: () =>
          import(
            "./pages/human-resources/employee-demands/employee-demands.module"
          ).then((m) => m.EmployeeDemandsModule),
        // canActivate: [AuthGuard]
      },
      {
        path: "candidates",
        loadChildren: () =>
          import("./pages/human-resources/candidates/candidates.module").then(
            (m) => m.CandidatesModule
          ),
        // canActivate: [AuthGuard]
      },
      {
        path: "crud-candidate/:id",
        loadChildren: () =>
          import(
            "./pages/human-resources/crud-candidate/crud-candidate.module"
          ).then((m) => m.CrudCandidateModule),
        // canActivate: [AuthGuard]
      },
      {
        path: "applications",
        loadChildren: () =>
          import(
            "./pages/human-resources/applications/applications.module"
          ).then((m) => m.ApplicationsModule),
        // canActivate: [AuthGuard]
      },
      {
        path: "crud-application/:id",
        loadChildren: () =>
          import(
            "./pages/human-resources/crud-application/crud-application.module"
          ).then((m) => m.CrudApplicationModule),
        // canActivate: [AuthGuard]
      },
      {
        path: "job-offers",
        loadChildren: () =>
          import("./pages/human-resources/job-offers/job-offers.module").then(
            (m) => m.JobOffersModule
          ),
        // canActivate: [AuthGuard]
      },
      {
        path: "crud-job-offer/:id",
        loadChildren: () =>
          import(
            "./pages/human-resources/crud-job-offer/crud-job-offer.module"
          ).then((m) => m.CrudJobOfferModule),
        // canActivate: [AuthGuard]
      },
      {
        path: "probation-periods",
        loadChildren: () =>
          import(
            "./pages/human-resources/probation-periods/probation-periods.module"
          ).then((m) => m.ProbationPeriodsModule),
        // canActivate: [AuthGuard]
      },
      {
        path: "crud-probation-period/:id",
        loadChildren: () =>
          import(
            "./pages/human-resources/crud-probation-period/crud-probation-period.module"
          ).then((m) => m.CrudProbationPeriodModule),
        // canActivate: [AuthGuard]
      },
      {
        path: "crud-employee/:id",
        loadChildren: () =>
          import(
            "./pages/human-resources/crud-employee/crud-employee.module"
          ).then((m) => m.CrudEmployeeModule),
        // canActivate: [AuthGuard]
      },
      {
        path: "employees",
        loadChildren: () =>
          import("./pages/human-resources/employees/employees.module").then(
            (m) => m.EmployeesModule
          ),
        // canActivate: [AuthGuard]
      },
      {
        path: "education-degrees",
        loadChildren: () =>
          import(
            "./pages/human-resources/education-degrees/education-degrees.module"
          ).then((m) => m.EducationDegreesModule),
        // canActivate: [AuthGuard]
      },
      {
        path: "skill-types",
        loadChildren: () =>
          import("./pages/human-resources/skill-types/skill-types.module").then(
            (m) => m.SkillTypesModule
          ),
        // canActivate: [AuthGuard]
      },
      {
        path: "skills",
        loadChildren: () =>
          import("./pages/human-resources/skills/skills.module").then(
            (m) => m.SkillsModule
          ),
        // canActivate: [AuthGuard]
      },
      {
        path: "reasons",
        loadChildren: () =>
          import("./pages/human-resources/reasons/reasons.module").then(
            (m) => m.ReasonsModule
          ),
        // canActivate: [AuthGuard]
      },
      {
        path: "exams",
        loadChildren: () =>
          import("./pages/human-resources/exams/exams.module").then(
            (m) => m.ExamsModule
          ),
        // canActivate: [AuthGuard]
      },
      {
        path: "questions",
        loadChildren: () =>
          import("./pages/human-resources/questions/questions.module").then(
            (m) => m.QuestionsModule
          ),
        // canActivate: [AuthGuard]
      },
      {
        path: "rules",
        loadChildren: () =>
          import("./pages/human-resources/rules/rules.module").then(
            (m) => m.RulesModule
          ),
        // canActivate: [AuthGuard]
      },
      {
        path: "crud-exam/:id",
        loadChildren: () =>
          import("./pages/human-resources/crud-exam/crud-exam.module").then(
            (m) => m.CrudExamModule
          ),
        // canActivate: [AuthGuard]
      },
      {
        path: "crud-question/:id",
        loadChildren: () =>
          import(
            "./pages/human-resources/crud-question/crud-question.module"
          ).then((m) => m.CrudQuestionModule),
        // canActivate: [AuthGuard]
      },
      {
        path: "exam-candidates",
        loadChildren: () =>
          import(
            "./pages/human-resources/exam-candidates/exam-candidates.module"
          ).then((m) => m.ExamCandidatesModule),
        // canActivate: [AuthGuard]
      },
      {
        path: "crud-exam-candidate/:id",
        loadChildren: () =>
          import(
            "./pages/human-resources/crud-exam-candidate/crud-exam-candidate.module"
          ).then((m) => m.CrudExamCandidateModule),
        // canActivate: [AuthGuard]
      },
    ],
  },
  {
    path: "administration",
    children: [
      {
        path: "crud-user/:id",
        loadChildren: () =>
          import("./pages/administration/crud-user/crud-user.module").then(
            (m) => m.CrudUserModule
          ),
        // canActivate: [AuthGuard]
      },
      {
        path: "users",
        loadChildren: () =>
          import("./pages/administration/users/users.module").then(
            (m) => m.UsersModule
          ),
        // canActivate: [AuthGuard]
      },
      {
        path: "groups",
        loadChildren: () =>
          import("./pages/administration/groups/groups.module").then(
            (m) => m.GroupsModule
          ),
        // canActivate: [AuthGuard]
      },
      {
        path: "permissions",
        loadChildren: () =>
          import("./pages/administration/permissions/permissions.module").then(
            (m) => m.PermissionsModule
          ),
        // canActivate: [AuthGuard]
      },
      {
        path: "roles",
        loadChildren: () =>
          import("./pages/administration/roles/roles.module").then(
            (m) => m.RolesModule
          ),
        // canActivate: [AuthGuard]
      },
      {
        path: "crud-role/:id",
        loadChildren: () =>
          import("./pages/administration/crud-role/crud-role.module").then(
            (m) => m.CrudRoleModule
          ),
        // canActivate: [AuthGuard]
      },
      {
        path: "crud-group/:id",
        loadChildren: () =>
          import("./pages/administration/crud-group/crud-group.module").then(
            (m) => m.CrudGroupModule
          ),
        // canActivate: [AuthGuard]
      },
    ],
  },

  {
    path: "inventory",
    children: [
      {
        path: "item-types",
        loadChildren: () =>
          import("./pages/inventory/item-types/item-types.module").then(
            (m) => m.ItemTypesModule
          ),
        // canActivate: [AuthGuard]
      },
      {
        path: "unit-of-measures",
        loadChildren: () =>
          import(
            "./pages/inventory/unit-of-measures/unit-of-measures.module"
          ).then((m) => m.UnitOfMeasuresModule),
        // canActivate: [AuthGuard]
      },
      {
        path: "item-categories",
        loadChildren: () =>
          import(
            "./pages/inventory/item-categories/item-categories.module"
          ).then((m) => m.ItemCategoriesModule),
        // canActivate: [AuthGuard]
      },
      {
        path: "items",
        loadChildren: () =>
          import("./pages/inventory/items/items.module").then(
            (m) => m.ItemsModule
          ),
        // canActivate: [AuthGuard]
      },
    ],
  },
  {
    path: "warehouse",
    children: [
      {
        path: "bin-types",
        loadChildren: () =>
          import("./pages/warehouse/bin-types/bin-types.module").then(
            (m) => m.BinTypesModule
          ),
        // canActivate: [AuthGuard]
      },
      {
        path: "zones",
        loadChildren: () =>
          import("./pages/warehouse/zones/zones.module").then(
            (m) => m.ZonesModule
          ),
        // canActivate: [AuthGuard]
      },
      {
        path: "bins",
        loadChildren: () =>
          import("./pages/warehouse/bins/bins.module").then(
            (m) => m.BinsModule
          ),
        // canActivate: [AuthGuard]
      },
    ],
  },
  {
    path: "configuration",
    children: [
      {
        path: "companies",
        loadChildren: () =>
          import("./pages/configuration/companies/companies.module").then(
            (m) => m.CompaniesModule
          ),
        // canActivate: [AuthGuard]
      },
      {
        path: "business-units",
        loadChildren: () =>
          import(
            "./pages/configuration/business-units/business-units.module"
          ).then((m) => m.BusinessUnitsModule),
        // canActivate: [AuthGuard]
      },
      {
        path: "locations",
        loadChildren: () =>
          import("./pages/configuration/locations/Locations.module").then(
            (m) => m.LocationsModule
          ),
        // canActivate: [AuthGuard]
      },
      {
        path: "responsibility-center",
        loadChildren: () =>
          import(
            "./pages/configuration/responsibility-center/responsibility-center.module"
          ).then((m) => m.ResponsibilityCenterModule),
        // canActivate: [AuthGuard]
      },
    ],
  },

  {
    path: "**",
    loadChildren: () =>
      import("./pages/errors/error-404/error-404.module").then(
        (m) => m.Error404Module
      ),
    // canActivate: [AuthGuard]
  },
];

const routes: Routes = [
  {
    path: 'signin',
    loadChildren: () => import('./pages/security/auth-n/signin/signin.module').then(m => m.SignInModule)
  },
  {
    path: '',
    component: CustomLayoutComponent,
    children: childrenRoutes
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, {
    preloadingStrategy: PreloadAllModules,
    scrollPositionRestoration: 'enabled',
    relativeLinkResolution: 'corrected',
    anchorScrolling: 'enabled'
  })],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
