import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Injectable()
export class BaseService {
    headers: HttpHeaders;
    protected http: HttpClient;
    public API_EndPoint = 'http://144.91.120.212:5800';

    constructor(https: HttpClient) {
        this.http = https;
        const headers = new HttpHeaders({
            'Content-Type': 'application/json; charset=utf-8',
            'Access-Control-Allow-Origin': '*'
        });
        this.headers = headers;
    }

    async post<T>(url: string, request: any): Promise<any> {
        try {
            let keys = Object.keys(request);
            keys.forEach(element => {
                if (element.toLowerCase().includes('startdate') || element.toLowerCase().includes('enddate')) {
                    let date = new Date(request[element]).toJSON();
                    if (date != null)
                        request[element] = date;
                }
            });

            const data = await this.http.post<T>(this.API_EndPoint + url, request, { headers: this.headers }).toPromise();
            return data;
        } catch (error) { console.log(error); return null; }
    }
}
