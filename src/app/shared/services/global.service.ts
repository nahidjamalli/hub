import { Injectable } from '@angular/core';
import { BaseService } from './base.service';

@Injectable({
    providedIn: 'root'
})

export class GeneralService extends BaseService {
    public async sendRequest<REQ_TYPE, RES_TYPE>(request: REQ_TYPE, url: string) {
        return await this.post<RES_TYPE>(url, request);
    }

    public validateDataSource(ds, controls) {
        for (let i = 0; i < controls.length; i++) {
            if (ds[controls[i]] == undefined || !Boolean(ds[controls[i]].toString()) || !ds[controls[i]])
                return controls[i];
        }
        return true;
    }
}
