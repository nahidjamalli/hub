import { Component, Inject, LOCALE_ID, Renderer2 } from "@angular/core";
import { ConfigService } from "../@hubcore/services/config.service";
import { Settings } from "luxon";
import { DOCUMENT } from "@angular/common";
import { Platform } from "@angular/cdk/platform";
import { NavigationService } from "../@hubcore/services/navigation.service";
import icLayers from "@iconify/icons-ic/twotone-layers";
import { LayoutService } from "../@hubcore/services/layout.service";
import { ActivatedRoute } from "@angular/router";
import { filter, map } from "rxjs/operators";
import { coerceBooleanProperty } from "@angular/cdk/coercion";
import { SplashScreenService } from "../@hubcore/services/splash-screen.service";
import { Style, StyleService } from "../@hubcore/services/style.service";
import { ConfigName } from "../@hubcore/interfaces/config-name.model";
import { TranslateService } from "@ngx-translate/core";

// Icons
import icbook from "@iconify/icons-ic/twotone-book";

@Component({
  selector: "vex-root",
  templateUrl: "./app.component.html",
  styleUrls: ["./app.component.scss"],
})
export class AppComponent {
  title = "vex";

  constructor(
    private configService: ConfigService,
    private styleService: StyleService,
    private renderer: Renderer2,
    private platform: Platform,
    @Inject(DOCUMENT) private document: Document,
    @Inject(LOCALE_ID) private localeId: string,
    private layoutService: LayoutService,
    private route: ActivatedRoute,
    private navigationService: NavigationService,
    private splashScreenService: SplashScreenService,
    private translate: TranslateService
  ) {
    Settings.defaultLocale = this.localeId;

    if (this.platform.BLINK) {
      this.renderer.addClass(this.document.body, "is-blink");
    }

    this.translate.setDefaultLang("en");
    this.translate.use(localStorage.getItem("lang") || "en");

    this.navigationService.items = [
      {
        type: "subheading",
        label: "finance",
        children: [
          {
            type: "dropdown",
            label: "dimensions",
            icon: icbook,
            children: [
              {
                type: "link",
                label: "analysis-sections",
                route: "/finance/analysis-sections",
              },
              {
                type: "link",
                label: "analysis-dimensions",
                route: "/finance/analysis-dimensions",
              },
            ],
          },
          {
            type: "link",
            label: "accounts",
            route: "/finance/accounts",
            icon: icbook,
          },
          {
            type: "link",
            label: "journals",
            route: "/finance/journals",
            icon: icbook,
          },
          {
            type: "link",
            label: "account-categories",
            route: "/finance/account-categories",
            icon: icbook,
          },
          {
            type: "link",
            label: "journal-types",
            route: "/finance/journal-types",
            icon: icbook,
          },
          {
            type: "link",
            label: "electronic-invoice-codes",
            route: "/finance/electronic-invoice-codes",
            icon: icbook,
          },
        ],
      },
      {
        type: "subheading",
        label: "Cash Management",
        children: [
          {
            type: "dropdown",
            label: "Banking",
            icon: icbook,
            children: [
              {
                type: "link",
                label: "banks",
                route: "/banking/banks",
              },
              {
                type: "link",
                label: "bank-accounts",
                route: "/banking/bank-accounts",
              },
            ],
          },
        ],
      },

      {
        type: "subheading",
        label: "Sales",
        children: [
          {
            type: "link",
            label: "customers",
            route: "/sales/customers",
            icon: icbook,
          },
          {
            type: "link",
            label: "sales-orders",
            route: "/sales/sales-orders",
            icon: icbook,
          },
          {
            type: "link",
            label: "sales-invoices",
            route: "/sales/sales-invoices",
            icon: icbook,
          },
          {
            type: "link",
            label: "sales-quotes",
            route: "/sales/sales-quotes",
            icon: icbook,
          },
        ],
      },
      {
        type: "subheading",
        label: "Purchasing",
        children: [
          {
            type: "link",
            label: "vendors",
            route: "/purchasing/vendors",
            icon: icbook,
          },
          {
            type: "link",
            label: "blanket-purchase-orders",
            route: "/purchasing/blanket-purchase-orders",
            icon: icbook,
          },
          {
            type: "link",
            label: "purchase-orders",
            route: "/purchasing/purchase-orders",
            icon: icbook,
          },
          {
            type: "link",
            label: "purchase-invoices",
            route: "/purchasing/purchase-invoices",
            icon: icbook,
          },
          {
            type: "link",
            label: "purchase-quotes",
            route: "/purchasing/purchase-quotes",
            icon: icbook,
          },
        ],
      },
      {
        type: "subheading",
        label: "Items & Prod Dev.",
        children: [
          {
            type: "link",
            label: "items",
            route: "/inventory/items",
            icon: icbook,
          },

          {
            type: "link",
            label: "item-types",
            route: "/inventory/item-types",
            icon: icbook,
          },
          {
            type: "link",
            label: "item-categories",
            route: "/inventory/item-categories",
            icon: icbook,
          },
          {
            type: "link",
            label: "unit-of-measures",
            route: "/inventory/unit-of-measures",
            icon: icbook,
          },
        ],
      },
      {
        type: "subheading",
        label: "warehouse",
        children: [
          {
            type: "link",
            label: "bin-types",
            route: "/warehouse/bin-types",
            icon: icbook,
          },
          {
            type: "link",
            label: "zones",
            route: "/warehouse/zones",
            icon: icbook,
          },
          {
            type: "link",
            label: "bins",
            route: "/warehouse/bins",
            icon: icbook,
          },
        ],
      },
      {
        type: "subheading",
        label: "Human Resources",
        children: [
          {
            type: "link",
            label: "Employee Demands",
            route: "/human-resources/employee-demands",
            icon: icbook,
          },
          {
            type: "link",
            label: "Candidates",
            route: "/human-resources/candidates",
            icon: icbook,
          },
          {
            type: "link",
            label: "Applications",
            route: "/human-resources/applications",
            icon: icbook,
          },
          {
            type: "link",
            label: "Job Offers",
            route: "/human-resources/job-offers",
            icon: icbook,
          },
          {
            type: "link",
            label: "Probation Periods",
            route: "/human-resources/probation-periods",
            icon: icbook,
          },
          {
            type: "link",
            label: "Employees",
            route: "/human-resources/employees",
            icon: icbook,
          },
          {
            type: "dropdown",
            label: "Examination",
            icon: icbook,
            children: [
              {
                type: "link",
                label: "Exam Candidates",
                route: "/human-resources/exam-candidates",
              },
              {
                type: "link",
                label: "Exams",
                route: "/human-resources/exams",
              },
              {
                type: "link",
                label: "Questions",
                route: "/human-resources/questions",
              },
              {
                type: "link",
                label: "Rules",
                route: "/human-resources/rules",
              },
            ],
          },
          {
            type: "dropdown",
            label: "Core HR",
            icon: icbook,
            children: [
              {
                type: "link",
                label: "Structures",
                route: "/human-resources/structures",
              },
              {
                type: "link",
                label: "Departments",
                route: "/human-resources/departments",
              },
              {
                type: "link",
                label: "Positions",
                route: "/human-resources/positions",
              },
              {
                type: "link",
                label: "Education Degrees",
                route: "/human-resources/education-degrees",
              },
              {
                type: "link",
                label: "Skill Types",
                route: "/human-resources/skill-types",
              },
              {
                type: "link",
                label: "Skills",
                route: "/human-resources/skills",
              },
              {
                type: "link",
                label: "Reasons",
                route: "/human-resources/reasons",
              },
            ],
          },
        ],
      },
      {
        type: "subheading",
        label: "Administration",
        children: [
          {
            type: "dropdown",
            label: "User Management",
            icon: icbook,
            children: [
              {
                type: "link",
                label: "Users",
                route: "/administration/users",
              },
              {
                type: "link",
                label: "Groups",
                route: "/administration/groups",
              },
              {
                type: "link",
                label: "Roles",
                route: "/administration/roles",
              },
              {
                type: "link",
                label: "Permissions",
                route: "/administration/permissions",
              },
            ],
          },
        ],
      },
      {
        type: "subheading",
        label: "configuration",
        children: [
          {
            type: "link",
            label: "companies",
            route: "/configuration/companies",
            icon: icbook,
          },
          {
            type: "link",
            label: "business-units",
            route: "/configuration/business-units",
            icon: icbook,
          },
          {
            type: "link",
            label: "locations",
            route: "/configuration/locations",
            icon: icbook,
          },
          {
            type: "link",
            label: "responsibility-center",
            route: "/configuration/responsibility-center",
            icon: icbook,
          },
        ],
      },
    ];
  }
}
