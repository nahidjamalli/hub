import { Component, EventEmitter, Output } from '@angular/core';
import { MatDialog, MatDialogConfig } from '@angular/material/dialog';
import icSearch from '@iconify/icons-ic/twotone-search';
import { SearchDialogComonent } from './search-content-dialog/search-dialog.component';

@Component({
  selector: 'vex-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']

})
export class SearchComponent {
  constructor(public dialog: MatDialog) { }
  icSearch = icSearch;

  openDialog() {
    const dialogConfig = new MatDialogConfig();
    dialogConfig.position = {
      top: '0',
    }
    this.dialog.open(SearchDialogComonent, dialogConfig);
  }
}
