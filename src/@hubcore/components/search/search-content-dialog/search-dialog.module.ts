import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MatDialogModule } from '@angular/material/dialog';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { IconModule } from '@visurel/iconify-angular';
import { MatInputModule } from '@angular/material/input';
import { SearchDialogComonent } from './search-dialog.component';
import { FormsModule } from '@angular/forms';

@NgModule({
    declarations: [SearchDialogComonent],
    imports: [
        CommonModule,
        MatDialogModule,
        MatIconModule,
        MatButtonModule,
        IconModule,
        MatInputModule,
        FormsModule
    ],
})

export class SearchDialogModule { }
