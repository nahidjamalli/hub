import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { HubLookupComponent } from "./hub-lookup.component";

@NgModule({
  declarations: [HubLookupComponent],
  imports: [CommonModule],
  exports: [HubLookupComponent],
  providers: [],
})
export class HubLookupModule {}
