import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { HubLabelComponent } from "./hub-label/hub-label.component";
import { HubFormFieldComponent } from "./hub-form-field.component";

@NgModule({
  declarations: [HubFormFieldComponent, HubLabelComponent],
  imports: [CommonModule],
  exports: [HubFormFieldComponent, HubLabelComponent],
})
export class HubFormFieldModule {}
