import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { HubDatepickerComponent } from "./hub-datepicker.component";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MatFormFieldModule } from "@angular/material/form-field";
import { MatDatepickerModule } from "@angular/material/datepicker";
import { MatNativeDateModule } from "@angular/material/core";
import { MatInputModule } from "@angular/material/input";

@NgModule({
  declarations: [HubDatepickerComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatFormFieldModule,
    MatInputModule,
    MatDatepickerModule,
    MatNativeDateModule,
  ],
  exports: [HubDatepickerComponent],
  providers: [MatDatepickerModule],
})
export class HubDatepickerModule {}
