import {
  Component,
  ElementRef,
  forwardRef,
  Input,
  OnChanges,
  ViewChild,
} from "@angular/core";
import {
  ControlValueAccessor,
  NgModel,
  NG_VALUE_ACCESSOR,
} from "@angular/forms";

@Component({
  selector: "hub-datepicker",
  templateUrl: "./hub-datepicker.component.html",
  styleUrls: ["./hub-datepicker.component.scss"],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => HubDatepickerComponent),
      multi: true,
    },
  ],
})
export class HubDatepickerComponent implements OnChanges, ControlValueAccessor {
  @ViewChild(NgModel) hubDatePickerControl: NgModel;
  @ViewChild("hubDatePickerControl", { read: ElementRef })
  inputControl: ElementRef;

  @Input() label = "DD/MM/YYYY";
  @Input() required? = false;
  @Input() disabled? = false;
  @Input() autofocus? = false;
  @Input() allowFutureDates? = true;

  maxDate;

  innerValue;

  onChange: any = () => {};
  onTouched: any = () => {};

  constructor() {}

  focus() {
    this.inputControl.nativeElement.focus();
  }

  ngOnChanges() {
    this.maxDate = this.allowFutureDates ? null : new Date();
  }

  writeValue(value: any) {
    this.innerValue = value;
  }

  onDateChange(event) {
    this.onChange(event.value);
  }

  registerOnChange(fn: (value: any) => void) {
    this.onChange = fn;
  }

  registerOnTouched(fn: any) {
    this.onTouched = fn;
  }

  setDisabledState(isDisabled: boolean) {
    this.disabled = isDisabled;
    if (isDisabled) {
      this.hubDatePickerControl.control.disable();
    } else {
      this.hubDatePickerControl.control.enable();
    }
  }

  onBlur($event) {
    console.log($event);
    if (
      $event.target &&
      $event.target.value &&
      $event.target.value.length === 8 &&
      !isNaN($event.target.value)
    ) {
      const val: String = $event.target.value;
      const month = val.slice(0, 2);
      const day = val.slice(2, 4);
      const year = val.slice(4);
      this.innerValue = new Date(`${month}/${day}/${year}`);
      this.hubDatePickerControl.control.setValue(this.innerValue);
      this.hubDatePickerControl.control.updateValueAndValidity();
      this.onChange(this.innerValue);
    }
    if (this.hubDatePickerControl.hasError("matDatepickerParse")) {
      this.hubDatePickerControl.control.setValue(null);
      this.hubDatePickerControl.control.updateValueAndValidity();
    }

    this.onTouched();
  }
}
