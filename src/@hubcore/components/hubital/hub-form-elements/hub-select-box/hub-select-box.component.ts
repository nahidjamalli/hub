import {
  Component,
  forwardRef,
  EventEmitter,
  Output,
  Input,
  ContentChild,
  TemplateRef,
} from "@angular/core";
import {
  ControlValueAccessor,
  FormControl,
  NG_VALUE_ACCESSOR,
} from "@angular/forms";

import { MatSelectChange } from "@angular/material/select";

@Component({
  selector: "hub-select-box",
  templateUrl: "./hub-select-box.component.html",
  styleUrls: ["./hub-select-box.component.scss"],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => HubSelectBoxComponent),
      multi: true,
    },
  ],
})
export class HubSelectBoxComponent implements ControlValueAccessor {
  @ContentChild(TemplateRef) templateVariable: TemplateRef<any>;
  @Input() value;
  @Input() required: boolean = false;
  @Input() defaultText: string;
  @Input() dataSource: any[];
  @Output() selectionChange = new EventEmitter<MatSelectChange>();

  selectListControl = new FormControl("");

  // model --> view
  writeValue(value: string): void {
    if (value) {
      this.selectListControl.setValue(value, { emitEvent: false });
    } else {
      this.selectListControl.reset("");
    }
  }

  // view --> model
  registerOnChange(fn: (value: string) => void) {
    this.selectListControl.valueChanges.subscribe(fn);
  }

  registerOnTouched(fn: () => void) {
    this.onTouched = fn;
  }

  onTouched: () => void = () => {};
}
