import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { HubSelectBoxComponent } from "./hub-select-box.component";
import { MatSelectModule } from "@angular/material/select";
import { ReactiveFormsModule } from "@angular/forms";

@NgModule({
  declarations: [HubSelectBoxComponent],
  imports: [CommonModule, ReactiveFormsModule, MatSelectModule],
  exports: [HubSelectBoxComponent],
})
export class HubSelectBoxModule {}
