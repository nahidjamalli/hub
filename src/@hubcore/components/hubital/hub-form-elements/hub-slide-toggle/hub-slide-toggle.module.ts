import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { HubSlideToggleComponent } from "./hub-slide-toggle.component";

@NgModule({
  declarations: [HubSlideToggleComponent],
  imports: [CommonModule],
  exports: [HubSlideToggleComponent],
})
export class HubSlideToggleModule {}
