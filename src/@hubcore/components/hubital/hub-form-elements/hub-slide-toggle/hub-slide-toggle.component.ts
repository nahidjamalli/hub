import { Component, forwardRef, Input } from "@angular/core";
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from "@angular/forms";

@Component({
  selector: "hub-slide-toggle",
  templateUrl: "./hub-slide-toggle.component.html",
  styleUrls: ["./hub-slide-toggle.component.scss"],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => HubSlideToggleComponent),
      multi: true,
    },
  ],
})

export class HubSlideToggleComponent implements ControlValueAccessor {
  @Input() checked: boolean = false;
  @Input() disabled: boolean = false;

  private innerValue: boolean;

  private onTouchedCallback: () => void;
  private onChangeCallback: (_: any) => void;

  constructor() {}

  onInputChange(event) {
    const newValue: boolean = event.target.checked;
    if (newValue !== this.innerValue) {
      this.innerValue = newValue;
      this.onChangeCallback(newValue);
    }
  }

  onBlur() {
    this.onTouchedCallback();
  }

  writeValue(value: any) {
    if (value !== this.innerValue) {
      this.innerValue = value;
    }
  }

  registerOnChange(fn: any) {
    this.onChangeCallback = fn;
  }

  registerOnTouched(fn: any) {
    this.onTouchedCallback = fn;
  }
}
