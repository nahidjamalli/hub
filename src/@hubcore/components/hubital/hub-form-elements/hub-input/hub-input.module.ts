import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule } from "@angular/forms";
import { HubInputComponent } from "./hub-input.component";

@NgModule({
  declarations: [HubInputComponent],
  imports: [CommonModule, FormsModule],
  exports: [HubInputComponent],
})
export class HubInputModule {}
