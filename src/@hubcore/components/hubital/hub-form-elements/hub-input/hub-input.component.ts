import { Component, forwardRef, Input, OnInit } from "@angular/core";
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from "@angular/forms";

@Component({
  selector: "hub-input",
  templateUrl: "./hub-input.component.html",
  styleUrls: ["./hub-input.component.scss"],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => HubInputComponent),
      multi: true,
    },
  ],
})
export class HubInputComponent implements ControlValueAccessor {
  @Input() label: string = "";

  @Input() type: string = "text";
  @Input() name: string = null;
  @Input() placeholder: string = "";

  @Input() id: string = null;
  @Input() class: string = null;

  @Input() disabled: boolean = false;
  @Input() required: boolean = false;
  @Input() readonly: boolean = false;

  constructor() {}

  public value: string;
  public changed: (value: string) => void;
  public touched: () => void;
  public isDisabled: boolean;

  writeValue(value: string): void {
    this.value = value;
  }
  public onChange() {
    const value: string = (<HTMLInputElement>event.target).value;
    this.changed(value);
  }
  registerOnChange(fn: any): void {
    this.changed = fn;
  }
  registerOnTouched(fn: any): void {
    this.touched = fn;
  }
  setDisabledState?(isDisabled: boolean): void {
    this.isDisabled = isDisabled;
  }
}
