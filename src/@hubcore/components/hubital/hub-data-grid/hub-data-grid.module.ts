import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { HubDataGridComponent } from "./hub-data-grid.component";
import { MatPaginatorModule } from "@angular/material/paginator";
import { MatTableModule } from "@angular/material/table";
import { MatSortModule } from "@angular/material/sort";
import { MatIconModule } from "@angular/material/icon";
import { IconModule } from "@visurel/iconify-angular";
import { MatButtonModule } from "@angular/material/button";
import { MatMultiSortModule } from "ngx-mat-multi-sort";
import { MatMenuModule } from "@angular/material/menu";
import { MatCheckboxModule } from "@angular/material/checkbox";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { FlexLayoutModule } from "@angular/flex-layout";
import { TranslateModule } from "@ngx-translate/core";

@NgModule({
  declarations: [HubDataGridComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FormsModule,
    TranslateModule,
    FlexLayoutModule,
    MatPaginatorModule,
    MatTableModule,
    MatSortModule,
    MatIconModule,
    IconModule,
    MatButtonModule,
    MatMultiSortModule,
    MatMenuModule,
    MatCheckboxModule,
  ],
  exports: [HubDataGridComponent],
})
export class HubDataGridModule {}
