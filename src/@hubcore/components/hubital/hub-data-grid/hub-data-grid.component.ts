import { SelectionModel } from "@angular/cdk/collections";
import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  ContentChild,
  EventEmitter,
  Input,
  OnInit,
  Output,
  TemplateRef,
  ViewChild,
} from "@angular/core";
import { MatPaginator } from "@angular/material/paginator";
import { MatTableDataSource } from "@angular/material/table";
import { MatMultiSort } from "ngx-mat-multi-sort";
import { FormControl } from "@angular/forms";
import { UntilDestroy, untilDestroyed } from "@ngneat/until-destroy";
import { MatDialog } from "@angular/material/dialog";
import { HubConfirmComponent } from "../hub-confirm/hub-confirm.component";

@UntilDestroy()
@Component({
  selector: "hub-data-grid",
  templateUrl: "./hub-data-grid.component.html",
  styleUrls: ["./hub-data-grid.component.scss"],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HubDataGridComponent implements OnInit, AfterViewInit {
  searchCtrl = new FormControl();
  selection = new SelectionModel<any>(true, []);
  displayedColumns: string[] = [];

  @ViewChild(MatMultiSort) sort: MatMultiSort;
  @ViewChild(MatPaginator) paginator: MatPaginator;
  @ContentChild(TemplateRef) templateVariable: TemplateRef<any>;

  @Input() dataSource: MatTableDataSource<any> | null;
  @Input() pageSize: number;
  @Input() pageSizeOptions: number[] = [];
  @Input() paginatorText: string;
  @Input() emptyDataText: string;
  @Input() name: string = "item";
  @Input() columns: any[] = [];
  @Input() actions: boolean = false;
  @Input() insert: boolean = false;
  @Input() selectRow: boolean = false;
  @Input() search: boolean = false;
  @Input() paginatorShow: boolean = false;

  @Output() onRemove = new EventEmitter();
  @Output() onEdit = new EventEmitter();
  @Output() onInsert = new EventEmitter();
  @Output() onSelectedDelete = new EventEmitter();
  @Output() onDblClick = new EventEmitter();

  constructor(public dialog: MatDialog) {}

  ngOnInit() {
    this.searchCtrl.valueChanges
      .pipe(untilDestroyed(this))
      .subscribe((value) => this.onSearchChange(value));
  }

  ngAfterViewInit() {
    let lang = localStorage.getItem("lang");
    this.isActionsAndisSelected();
    this.dataSource.sort = this.sort;
    setTimeout(() => (this.dataSource.paginator = this.paginator));
    if (this.paginatorText) {
      lang == "az"
        ? (this.paginator._intl.itemsPerPageLabel = this.paginatorText)
        : "Items per page:";
    }
    if (this.emptyDataText) {
      lang == "az" ? null : (this.emptyDataText = "No Data");
    }
  }

  onRowAdd() {
    this.onInsert.emit();
  }

  onRowEdit(data, $event) {
    this.onEdit.emit({ data, e: $event });
  }

  onRowRemove(data, $event) {
    const dialogRef = this.dialog.open(HubConfirmComponent, {
      data: {
        title: this.name,
      },
    });
    dialogRef.afterClosed().subscribe((result) => {
      if (result == "OK") {
        this.onRemove.emit({ data, e: $event });
        this.selection.deselect(data);
      }
    });
  }

  onRowsSelected(data, $event) {
    this.onSelectedDelete.emit({ data, e: $event });
  }

  onRowDblClick(row, e) {
    if (this.selectRow) this.selection.toggle(row);
    this.onDblClick.emit({ data: row, e });
  }

  isActionsAndisSelected() {
    let allLabels = this.columns.map((item) => item.dataField);
    if (this.actions && this.selectRow) {
      this.displayedColumns.push("Checkbox", ...allLabels, "Actions");
    } else if (this.selectRow) {
      this.displayedColumns.push("Checkbox", ...allLabels);
    } else if (this.actions) {
      this.displayedColumns.push(...allLabels, "Actions");
    } else {
      this.displayedColumns.push(...allLabels);
    }
  }

  onSearchChange(value: string) {
    if (!this.dataSource) {
      return;
    }
    value = value.trim();
    value = value.toLowerCase();
    this.dataSource.filter = value;

    if (this.dataSource.paginator) {
      this.dataSource.paginator.firstPage();
    }
  }

  isAllSelected() {
    const numSelected = this.selection.selected.length;
    const numRows = this.dataSource.data.length;
    return numSelected === numRows;
  }

  masterToggle() {
    this.isAllSelected()
      ? this.selection.clear()
      : this.dataSource.data.forEach((row) => this.selection.select(row));
  }
}
