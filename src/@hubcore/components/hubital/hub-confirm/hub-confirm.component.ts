import { Component, Inject } from "@angular/core";
import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";

@Component({
  selector: "hub-confirm",
  templateUrl: "./hub-confirm.component.html",
  styleUrls: ["./hub-confirm.component.scss"],
})
export class HubConfirmComponent {
  constructor(
    @Inject(MAT_DIALOG_DATA) public data,
    public dialogRef: MatDialogRef<HubConfirmComponent>
  ) {}

  close(): void {
    this.dialogRef.close();
  }
  success(): void {
    this.dialogRef.close("OK");
  }
}
