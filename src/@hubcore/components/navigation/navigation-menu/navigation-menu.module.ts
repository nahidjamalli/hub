import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NavigationMenuComponent } from './navigation-menu.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatRippleModule } from '@angular/material/core';
import { IconModule } from '@visurel/iconify-angular';
import { NavigationModule } from 'src/@hubcore/layout/application-bar/application-bar.module';
import { NavigationItemModule } from '../navigation-item/navigation-item.module';
import { ContainerModule } from 'src/@hubcore/directives/container/container.module';

@NgModule({
  declarations: [NavigationMenuComponent],
  imports: [
    CommonModule,
    FlexLayoutModule,
    MatButtonModule,
    MatIconModule,
    MatMenuModule,
    MatRippleModule,
    IconModule,
    NavigationModule,
    NavigationItemModule,
    ContainerModule
  ],
  exports: [NavigationMenuComponent]
})

export class NavigationMenuModule { }
