import { Component, HostBinding, Input } from '@angular/core';
import { LayoutService } from '../../../services/layout.service';
import icMenu from '@iconify/icons-ic/twotone-menu';
import { ConfigService } from '../../../services/config.service';
import { map } from 'rxjs/operators';
import { NavigationService } from '../../../services/navigation.service';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'vex-navigation-menu',
  templateUrl: './navigation-menu.component.html',
  styleUrls: ['./navigation-menu.component.scss']
})
export class NavigationMenuComponent {
  layoutCtrl = new FormControl('fullwidth');
  @Input() mobileQuery: boolean;

  @Input()
  @HostBinding('class.shadow-b')
  hasShadow: boolean;

  navigationItems = this.navigationService.items;

  isDesktop$ = this.layoutService.isDesktop$;
  isHorizontalLayout$ = this.configService.config$.pipe(map(config => config.layout === 'horizontal'));
  isVerticalLayout$ = this.configService.config$.pipe(map(config => config.layout === 'vertical'));
  isNavbarInToolbar$ = this.configService.config$.pipe(map(config => config.navbar.position === 'in-toolbar'));
  isNavbarBelowToolbar$ = this.configService.config$.pipe(map(config => config.navbar.position === 'below-toolbar'));

  icMenu = icMenu;

  constructor(private layoutService: LayoutService,
    private configService: ConfigService,
    private navigationService: NavigationService) { }

  ngOnInit() {
  }

  openSidenav() {
    this.layoutService.openSidenav();
  }
}
