import { Component, HostBinding, Input } from '@angular/core';
import { LayoutService } from '../../../services/layout.service';
import { ConfigService } from '../../../services/config.service';
import { map } from 'rxjs/operators';
import { NavigationService } from '../../../services/navigation.service';

@Component({
  selector: 'vex-navigation-bar',
  templateUrl: './navigation-bar.component.html',
  styleUrls: ['./navigation-bar.component.scss']
})
export class NavigationBarComponent {

  @Input() navigationBarItems: [];
  @HostBinding('class.shadow-b')
  hasShadow: boolean;


  isDesktop$ = this.layoutService.isDesktop$;
  isHorizontalLayout$ = this.configService.config$.pipe(map(config => config.layout === 'horizontal'));
  isVerticalLayout$ = this.configService.config$.pipe(map(config => config.layout === 'vertical'));
  isNavbarInToolbar$ = this.configService.config$.pipe(map(config => config.navbar.position === 'in-toolbar'));
  isNavbarBelowToolbar$ = this.configService.config$.pipe(map(config => config.navbar.position === 'below-toolbar'));

  constructor(private layoutService: LayoutService,
    private configService: ConfigService,
    private navigationService: NavigationService) { }
}
