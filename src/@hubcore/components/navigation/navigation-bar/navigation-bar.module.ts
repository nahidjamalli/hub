import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatRippleModule } from '@angular/material/core';
import { IconModule } from '@visurel/iconify-angular';
import { NavigationModule } from 'src/@hubcore/layout/application-bar/application-bar.module';
import { NavigationItemModule } from '../navigation-item/navigation-item.module';
import { ContainerModule } from 'src/@hubcore/directives/container/container.module';
import { NavigationBarComponent } from './navigation-bar.component';


@NgModule({
  declarations: [NavigationBarComponent],
  imports: [
    CommonModule,
    FlexLayoutModule,
    MatButtonModule,
    MatIconModule,
    MatMenuModule,
    MatRippleModule,
    IconModule,
    NavigationModule,
    NavigationItemModule,
    ContainerModule,
  ],
  exports: [NavigationBarComponent]
})

export class NavigationBarModule { }
