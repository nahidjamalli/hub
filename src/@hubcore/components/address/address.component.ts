import { Component, EventEmitter, Output } from '@angular/core';
import { Address } from './address.model';

@Component({
  selector: 'vex-address',
  templateUrl: './address.component.html',
  styleUrls: ['./address.component.scss']
})
export class AddressComponent {
  popupVisible = false;

  addressList = [];
  tempDataSourceAddr: Address = new Address();
  addressType: any;
  @Output() address = new EventEmitter<object>();

  constructor() { }

  initAddress() {
    this.tempDataSourceAddr = new Address();
    this.popupVisible = !this.popupVisible;
  }

  addNewAddress() {
    this.tempDataSourceAddr.Id = Math.ceil(Math.random() * 10);
    this.addressList.push(this.tempDataSourceAddr);
    this.popupVisible = !this.popupVisible;


    this.tempDataSourceAddr = new Address();
  }

  editAddress(selectedItem) {
    this.tempDataSourceAddr = selectedItem;
    this.popupVisible = !this.popupVisible;
  }

  deleteAddress(id) {
    let result = this.addressList.find(i => i.Id == id);
    this.addressList.splice(result, 1);
  }
}
