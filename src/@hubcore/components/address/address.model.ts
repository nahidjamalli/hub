export class Address {
    Id: number;
    Address: string;
    Country: string;
    City: string;
    ZipCode: string;
}