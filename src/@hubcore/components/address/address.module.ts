import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddressComponent } from './address.component';
import { DxButtonModule, DxFormModule, DxLookupModule, DxPopupModule } from 'devextreme-angular';


@NgModule({
  declarations: [AddressComponent],
  imports: [
    CommonModule,
    DxPopupModule,
    DxButtonModule,
    DxFormModule,
    DxLookupModule
  ],
  exports: [AddressComponent]
})

export class AddressModule { }
