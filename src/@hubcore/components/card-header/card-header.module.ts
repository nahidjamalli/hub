import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { CardHeaderComponent } from './card-header.component';
import { BreadcrumbsModule } from '../breadcrumbs/breadcrumbs.module';
import { MatButtonModule } from '@angular/material/button';
import { IconModule } from '@visurel/iconify-angular';
import { MatIconModule } from '@angular/material/icon';
import { FlexLayoutModule } from '@angular/flex-layout';

@NgModule({
  declarations: [CardHeaderComponent],
  imports: [
    CommonModule,
    BreadcrumbsModule,
    FlexLayoutModule,
    MatButtonModule,
    MatIconModule,
    IconModule,
  ],
  exports: [CardHeaderComponent]
})

export class CardHeaderModule { }
