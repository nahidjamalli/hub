import { Component, Input } from '@angular/core';

@Component({
  selector: 'vex-card-header',
  templateUrl: './card-header.component.html',
  styleUrls: ['./card-header.component.scss']
})
export class CardHeaderComponent {
  @Input() crumbs: string[] = [];
  // @Input() add: Function;

  constructor() { }
}
