import { Component, Input, OnInit } from '@angular/core';
import icHome from '@iconify/icons-ic/twotone-home';
import { trackByValue } from '../../utils/track-by';

@Component({
  selector: 'vex-breadcrumbs',
  styles: ['.dot:last-of-type {display:none; }'],
  template: `
    <div class="flex items-center">
      <ng-container *ngFor="let crumb of crumbs; trackBy: trackByValue">
        <vex-breadcrumb>
          <a [routerLink]="[]">{{ crumb | translate }}</a>
        </vex-breadcrumb>
        <div class="dot w-1 h-1 bg-gray rounded-full ltr:mr-2 rtl:ml-2"></div>
      </ng-container>
    </div>
  `
})
export class BreadcrumbsComponent implements OnInit {

  @Input() crumbs: string[] = [];

  trackByValue = trackByValue;
  icHome = icHome;

  constructor() {
  }

  ngOnInit() {
  }
}
