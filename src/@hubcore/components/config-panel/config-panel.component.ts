import { Component, Inject, OnInit } from '@angular/core';
import { ConfigService } from '../../services/config.service';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { first, map } from 'rxjs/operators';
import icSettings from '@iconify/icons-ic/twotone-settings';
import { LayoutService } from '../../services/layout.service';
import icCheck from '@iconify/icons-ic/twotone-check';
import { MatRadioChange } from '@angular/material/radio';
import { ActivatedRoute } from '@angular/router';
import { coerceBooleanProperty } from '@angular/cdk/coercion';
import { Style, StyleService } from '../../services/style.service';
import { ConfigName } from '../../interfaces/config-name.model';
import { ColorVariable, colorVariables } from './color-variables';
import { DOCUMENT } from '@angular/common';
import icClose from '@iconify/icons-ic/twotone-close';
import DxThemes from 'devextreme/ui/themes';
import { currentTheme } from 'devextreme/viz/themes';

@Component({
  selector: 'vex-config-panel',
  templateUrl: './config-panel.component.html',
  styleUrls: ['./config-panel.component.scss']
})
export class ConfigPanelComponent implements OnInit {

  configs = this.configService.configs;
  colorVariables = colorVariables;

  config$ = this.configService.config$;
  activeConfig$ = this.configService.config$.pipe(
    map(config => Object.keys(this.configService.configs).find(key => this.configService.configs[key] === config))
  );

  isRTL$ = (localStorage.getItem('RightToLeft') == 'true' ? true : false);

  selectedStyle$ = this.styleService.style$;

  icSettings = icSettings;
  icCheck = icCheck;
  icClose = icClose;
  ConfigName = ConfigName;
  Style = Style;
  selectedColor = colorVariables.cerulean;


  constructor(private configService: ConfigService,
    private styleService: StyleService,
    private layoutService: LayoutService,
    @Inject(DOCUMENT) private document: Document,
    private route: ActivatedRoute) { }

  ngOnInit() {
    localStorage.getItem('dark-mode') == 'vex-style-dark' ? this.enableDarkMode() : this.disableDarkMode();
    this.styleService.setStyle((localStorage.getItem('dark-mode') || 'vex-style-default') as Style);
    this.selectColor({
      key: localStorage.getItem('colorName'),
      value: colorVariables[localStorage.getItem('colorName')]
    });

    let toolbarPosition = localStorage.getItem('toolbarPosition') || 'static';
    this.toolbarPositionChange({ value: toolbarPosition } as MatRadioChange);
  }

  setConfig(layout: ConfigName, style: Style) {
    this.styleService.setStyle(style);
  }

  selectColor(color) {
    if (!color || !color.value) return;
    localStorage.setItem('colorName', color.key);
    this.selectedColor = color.value;

    if (this.document) {
      this.document.documentElement.style.setProperty('--color-primary', this.selectedColor.default.replace('rgb(', '').replace(')', ''));
      this.document.documentElement.style.setProperty('--color-primary-contrast', this.selectedColor.contrast.replace('rgb(', '').replace(')', ''));
    }
  }

  isSelectedColor(color: ColorVariable) {
    return color === this.selectedColor;
  }
  enableDarkMode() {
    localStorage.setItem('dark-mode', Style.dark);
    this.styleService.setStyle(Style.dark);
    currentTheme("generic.dark");
    DxThemes.current("generic.dark");
    this.document.documentElement.style.setProperty('--color-primary-menu-text', '0,0,0');
    this.document.documentElement.style.setProperty('--color-company-name', '0,0,0');
    this.document.documentElement.style.setProperty('--color-bar', '255,255,255');
    this.document.documentElement.style.setProperty('--color-border-line', '255,255,255');
    this.document.documentElement.style.setProperty('--color-mode', '245, 245, 248');
    this.document.documentElement.style.setProperty('--color-remode', '35, 43, 62');
  }

  disableDarkMode() {
    localStorage.setItem('dark-mode', Style.default);
    this.styleService.setStyle(Style.default);
    currentTheme("generic.light");
    DxThemes.current("generic.light");
    this.document.documentElement.style.setProperty('--color-primary-menu-text', '127,125,127');
    this.document.documentElement.style.setProperty('--color-company-name', '0,0,0');
    this.document.documentElement.style.setProperty('--color-bar', '0,0,0');
    this.document.documentElement.style.setProperty('--color-border-line', '136,141,150');
    this.document.documentElement.style.setProperty('--color-mode', '35, 43, 62');
    this.document.documentElement.style.setProperty('--color-remode', '245, 245, 248');
  }

  sidenavOpenChange(change: MatSlideToggleChange) {
    change.checked ? this.layoutService.openSidenav() : this.layoutService.closeSidenav();
  }

  toolbarPositionChange(change: MatRadioChange) {
    localStorage.setItem('toolbarPosition', change.value);
    this.configService.updateConfig({
      toolbar: {
        fixed: change.value === 'fixed'
      }
    });
  }
}
