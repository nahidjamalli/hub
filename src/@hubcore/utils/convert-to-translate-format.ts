export function convertTitleToTranslateFormat(title) {
  let result = title.replace(/([A-Z])/g, "-$1");
  let finalResult = result.slice(1).toLowerCase();
  return finalResult;
}
