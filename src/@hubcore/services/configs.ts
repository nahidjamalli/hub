import { mergeDeep } from '../utils/merge-deep';
import { ConfigName } from '../interfaces/config-name.model';
import { Config } from '../interfaces/config.model';

const defaultConfig: Config = {
  id: ConfigName.ikaros,
  name: 'Ikaros',
  imgSrc: '//vex-landing.visurel.com/assets/img/layouts/apollo.png',
  layout: 'vertical',
  boxed: true,
  sidenav: {
    title: 'HUBITAL Business Central',
    imageUrl: 'assets/img/demo/logo.png',
    showCollapsePin: true,
    state: 'expanded'
  },
  toolbar: {
    fixed: false
  },
  navbar: {
    position: 'in-toolbar'
  }
};

export const configs: Config[] = [
  defaultConfig,
  mergeDeep({ ...defaultConfig }, {})
];
