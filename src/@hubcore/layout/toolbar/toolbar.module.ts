import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ToolbarComponent } from './toolbar.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatMenuModule } from '@angular/material/menu';
import { MatRippleModule } from '@angular/material/core';
import { ToolbarNotificationsModule } from './toolbar-notifications/toolbar-notifications.module';
import { ToolbarUserModule } from './toolbar-user/toolbar-user.module';
import { IconModule } from '@visurel/iconify-angular';
import { NavigationModule } from '../application-bar/application-bar.module';
import { RouterModule } from '@angular/router';
import { NavigationItemModule } from '../../components/navigation/navigation-item/navigation-item.module';
import { ContainerModule } from '../../directives/container/container.module';
import { SearchModule } from 'src/@hubcore/components/search/search.module';


@NgModule({
  declarations: [ToolbarComponent],
  imports: [
    CommonModule,
    FlexLayoutModule,
    MatButtonModule,
    MatIconModule,
    MatMenuModule,
    MatRippleModule,
    ToolbarNotificationsModule,
    ToolbarUserModule,
    IconModule,
    NavigationModule,
    RouterModule,
    NavigationItemModule,
    ContainerModule,
    SearchModule
  ],
  exports: [ToolbarComponent]
})
export class ToolbarModule {
}
